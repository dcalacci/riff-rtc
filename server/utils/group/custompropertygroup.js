/* ******************************************************************************
 * custompropertygroup.js                                                       *
 * *************************************************************************/ /**
 *
 * @fileoverview Find the Riff group name for a student using the custom property API
 *
 * The CustomPropertyGroupApi handles the case when a custom LTI property
 * contains the group name for the user launching the LTI request.
 *
 * Created on       November 14, 2018
 * @author          Michael Jay Lippert
 *
 * @copyright (c) 2018 Riff Learning,
 *            MIT License (see https://opensource.org/licenses/MIT)
 *
 * ******************************************************************************/

const { AppError } = require('../errortypes');

const SEC_PER_MIN = 60;
const SEC_PER_HR = 60 * SEC_PER_MIN;
const SEC_PER_DAY = 24 * SEC_PER_HR; // eslint-disable-line no-unused-vars

// TODO: Consider getting this value from the configuration
const USERGROUP_CACHE_EXPIRE_SECS = 0; // Do not cache


/* ******************************************************************************
 * CustomPropertyGroupApi                                                  */ /**
 *
 * Provides the standard GroupApi for retrieving the group for a student
 * doing an LTI launch of Riff with the group name contained in a custom property.
 *
 ********************************************************************************/
class CustomPropertyGroupApi
{
  /* **************************************************************************
   * constructor                                                         */ /**
   *
   * CustomPropertyGroupApi class constructor.
   *
   * @param {CustomPropertyGroupApi.Config} config
   *      The settings to configure this CustomPropertyGroupApi
   */
  constructor({ groupApiConfig, req, logger } = {})
  {
    this.logger = (logger || req.app.get('routerLogger')).child({ 'class': 'CustomPropertyGroupApi' });

    // instance properties
    this.groupApiConfig = groupApiConfig;
    this.contextId = req.body.context_id;
    this.groupName = req.body[this.groupApiConfig.group_property_name];
    this.groupsCacheKey = CustomPropertyGroupApi.getGroupsCacheKey({ body: req.body });
  }

  /* **************************************************************************
   * userGroupCacheExpire property                                       */ /**
   *
   * The number of seconds to keep the user's group cached. Zero seconds
   * means do not cache.
   *
   * @param {number} seconds
   *      The number of seconds to keep the user's group cached.
   *
   * @returns {number} the number of seconds to keep the user's group
   *      cached, 0 if the user's group should not be cached.
   */
  get userGroupCacheExpire()
  {
    return USERGROUP_CACHE_EXPIRE_SECS;
  }
  set userGroupCacheExpire(seconds)
  {
    seconds;
    throw new Error('The userGroupCacheExpire value many not be changed.');
  }

  /* **************************************************************************
   * getGroup                                                            */ /**
   *
   * Get the name of the group the requestor belongs to. An error is thrown
   * if the requestor doesn't belong to any group.
   *
   * Note that for edX the group name was sent as a custom property in the
   * request and stored by the contructor so we just have to return it.
   *
   * @param {RequestorId} requestorId
   *      The ID of the user whose riff group is to be found and returned.
   *
   * @returns {string}
   */
  async getGroup(requestorId)
  {
    if (this.groupName)
    {
      return this.groupName;
    }

    // No group was found for the user, throw an error
    const errorContext =
      {
        user:
        {
          requestor_id: requestorId,
        },
        course:
        {
          context_id: this.contextId,
        },
        group_property_name: this.groupApiConfig.group_property_name,
      };

    throw new AppError('No group found for custom property LTI user', errorContext);
  }

  /* **************************************************************************
   * getRequestorId (static)                                             */ /**
   *
   * Get the value from the request which identifies the user to the
   * `getGroup` method.
   *
   * @param {ExpressRequest} req
   *      The request object
   *
   * @returns {RequestorId}
   */
  static getRequestorId(req)
  {
    return req.body.user_id;
  }

  /* **************************************************************************
   * getGroupsCacheKey (static)                                          */ /**
   *
   * Get the groups cache key. The key used to cache the groups info for a
   * particular course. The EdxGroupsApi doesn't retrieve ALL groups and
   * therefore it doesn't cache anything, but this method should still
   * return a cache key as though it did.
   *
   * @param {Object} body
   *      The request body (contains properties from the POST)
   *
   * @param {Object} query
   *      The request query properties
   *
   * @param {Object} params
   *      The request parameters (extracted from the route)
   *
   * @returns {string}
   */
  static getGroupsCacheKey({ body = {}, query = {}, params = {} } = {})
  {
    // because we don't have a domain we use to acquire the all the groups
    // we're ok w/ it being an empty string
    const apiDomain = '';
    const contextId = body.context_id || params.contextId || query.contextId;

    if (apiDomain === undefined || contextId === undefined)
    {
      const context =
        {
          body,
          query,
          params,
          properties:
          {
            apiDomain: [ 'params.domain', 'query.domain' ],
            contextId: [ 'body.context_id', 'params.contextId', 'query.contextId' ],
          },
        };
      throw new AppError('missing properties to define groups key', context);
    }

    const groupsCacheKey = `riff-rtc:customprop:${apiDomain}:${contextId}`;

    return groupsCacheKey;
  }
}

/* ******************************************************************************
 * CustomPropertyGroupApi.Config                                           */ /**
 *
 * The CustomPropertyGroupApi.Config defines the object passed to the constructor that
 * contains options/values used to initialize an instance of a CustomPropertyGroupApi.
 *
 * @typedef {!Object} CustomPropertyGroupApi.Config
 *
 * @property {GroupApiConfig} groupApiConfig
 *      The group api configuration for an Emeritus canvas instance containing the
 *      information needed to query the LMS for group information.
 *
 * @property {ExpressRequest} req
 *      The express request containing the values for the course that contains
 *      the groups to be examined.
 *
 * @property {Logger | undefined} logger
 */


// ES6 import compatible export
//        either: import CustomPropertyGroupApi from './custompropertygroup';
//            or: import { CustomPropertyGroupApi } from './custompropertygroup';
//   or CommonJS: const { CustomPropertyGroupApi } = require('./custompropertygroup');
module.exports =
{
  'default': CustomPropertyGroupApi,
  CustomPropertyGroupApi,
};
