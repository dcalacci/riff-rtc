# Change Log for riff-rtc

## [2.0.0](https://github.com/rifflearning/riff-rtc/tree/2.0.0) (2019-11-06)
[Full Changelog](https://github.com/rifflearning/riff-rtc/compare/1.1.0...2.0.0)

The major change for version 2.0.0 were extensive.
- Accessiblity was addressed everywhere.
- The dashboard and video chat were refactored for maintainability and future enhancement.
- A common style was defined using eslint and huge progess was made in updating sources to
  conform to that style (although there are still 518 issues listed by eslint).
- Video chat was enhanced to show display names and to help identify which node represents which
  participant in the meeting mediator.
- Video chat reliability was improved by adjusting the bandwidth based on the number of participants.
- The dashboard metrics were improved by using a common color throughout all charts for a particular
  participant.
- The dashboard metrics were improved so that they all scale reasonably when the browser window is
  resized.
- The dashboard was improved so that the various metrics draw as soon as the data they rely on is
 ready.
- The dashboard was improved by adding a scrollbar for the list of meetings if the list goes over
  one screenful.
- The overall look of the site was improved. The home page has more appropriate text no longer
  referring to a "beta".
- The top navigation was improved.
- The video chat lobby page for joining a chat was improved.

### Merged pull requests
- [\#54](https://github.com/rifflearning/riff-rtc/pull/54) switch to using a specific commit from the rifflearning SimpleWebRTC cloned repository

- [\#64](https://github.com/rifflearning/riff-rtc/pull/64) update npm packages
- [\#65](https://github.com/rifflearning/riff-rtc/pull/65) lint files in src directory

- [\#63](https://github.com/rifflearning/riff-rtc/pull/63) Validated email in forgot password form on front-end using regex
- [\#56](https://github.com/rifflearning/riff-rtc/pull/56) Use props to pass info to &lt;Helmet />, instead of passing JSX children
- [\#67](https://github.com/rifflearning/riff-rtc/pull/67) remove dotenv package
- [\#58](https://github.com/rifflearning/riff-rtc/pull/58) Images need alt attributes
- [\#59](https://github.com/rifflearning/riff-rtc/pull/59) Fix a11y on login page
- [\#66](https://github.com/rifflearning/riff-rtc/pull/66) Remove maximum-scale=1 from meta tag in index.html
- [\#62](https://github.com/rifflearning/riff-rtc/pull/62) Removed package 'material-icons-react', used @material-ui

- [\#55](https://github.com/rifflearning/riff-rtc/pull/55) address many a11y review issues found on chat page
- [\#57](https://github.com/rifflearning/riff-rtc/pull/57) Fix a11y issues on the my riffs page
- [\#69](https://github.com/rifflearning/riff-rtc/pull/69) copy utility function improvements that were implemented in mm-webapp

- [\#70](https://github.com/rifflearning/riff-rtc/pull/70) Dashboard cleanup pt1
- [\#80](https://github.com/rifflearning/riff-rtc/pull/80) Update issue templates
- [\#82](https://github.com/rifflearning/riff-rtc/pull/82) dashboard cleanup pt2
- [\#68](https://github.com/rifflearning/riff-rtc/pull/68) shift screenreader focus to top of page on route change
- [\#100](https://github.com/rifflearning/riff-rtc/pull/100) dashboard cleanup pt3

- [\#101](https://github.com/rifflearning/riff-rtc/pull/101) dashboard cleanup pt4
- [\#103](https://github.com/rifflearning/riff-rtc/pull/103) dashboard refactor pt1
- [\#104](https://github.com/rifflearning/riff-rtc/pull/104) dashboard refactor pt2
- [\#107](https://github.com/rifflearning/riff-rtc/pull/107) fix calculation to get the recent utterances in processInfluence
- [\#108](https://github.com/rifflearning/riff-rtc/pull/108) update firebase to v6
- [\#106](https://github.com/rifflearning/riff-rtc/pull/106) add a11y data tables to mediator

- [\#105](https://github.com/rifflearning/riff-rtc/pull/105) First pass at chat refactor

- [\#109](https://github.com/rifflearning/riff-rtc/pull/109) dashboard refactor pt3 (dashboard action creators)
- [\#114](https://github.com/rifflearning/riff-rtc/pull/114) add textchat back to MeetingRoom

- [\#121](https://github.com/rifflearning/riff-rtc/pull/121) dashboard refactor pt4

- [\#126](https://github.com/rifflearning/riff-rtc/pull/126) fix gantt x-axis ticks from dashboard refactor
- [\#120](https://github.com/rifflearning/riff-rtc/pull/120) label nodes on the meeting mediator w/ participant first initial and peer videos w/ the peer's display name

- [\#128](https://github.com/rifflearning/riff-rtc/pull/128) fix dashboard being scrolled to the top of the timeline chart
- [\#127](https://github.com/rifflearning/riff-rtc/pull/127) dashboard refactor pt5
- [\#123](https://github.com/rifflearning/riff-rtc/pull/123) add turns taken count to mediator
- [\#124](https://github.com/rifflearning/riff-rtc/pull/124) wrap the mediator in an info box

- [\#116](https://github.com/rifflearning/riff-rtc/pull/116) Start scss refactoring
- [\#130](https://github.com/rifflearning/riff-rtc/pull/130) Fix dashboard meeting list cursor

- [\#129](https://github.com/rifflearning/riff-rtc/pull/129) Refactor Chat pt2

- [\#134](https://github.com/rifflearning/riff-rtc/pull/134) Remove unnecessary propTypes from LeaveRoomButton
- [\#136](https://github.com/rifflearning/riff-rtc/pull/136) Remove the start and end time from the list of X axis tickmarks
- [\#133](https://github.com/rifflearning/riff-rtc/pull/133) Fix meeting mediator text
- [\#119](https://github.com/rifflearning/riff-rtc/pull/119) profile refactor
- [\#135](https://github.com/rifflearning/riff-rtc/pull/135) dashboard styling

- [\#137](https://github.com/rifflearning/riff-rtc/pull/137) fix primary button colors accidentally changed to turquoise

- [\#138](https://github.com/rifflearning/riff-rtc/pull/138) Video chat performance tweaks - bitrate limitation and resolution/fps changes

- [\#139](https://github.com/rifflearning/riff-rtc/pull/139) update influence chart to match style of turn chart

- [\#140](https://github.com/rifflearning/riff-rtc/pull/140) Fix the meeting list scrollbar displaying on chrome

- [\#142](https://github.com/rifflearning/riff-rtc/pull/142) Site wide changes
- [\#145](https://github.com/rifflearning/riff-rtc/pull/145) update node and packages

- [\#141](https://github.com/rifflearning/riff-rtc/pull/141) Lobby visual updates
- [\#144](https://github.com/rifflearning/riff-rtc/pull/144) Home page updates

- [\#146](https://github.com/rifflearning/riff-rtc/pull/146) Add meeting type input to lobby, and handle in redux
- [\#143](https://github.com/rifflearning/riff-rtc/pull/143) Fix metrics text overflow bug


## [1.1.0](https://github.com/rifflearning/riff-rtc/tree/1.1.0) (2019-03-11)
[Full Changelog](https://github.com/rifflearning/riff-rtc/compare/1.0.0...1.1.0)

The major change for version 1.1.0 is the addition of user signup and profile support.
Verification emails are now sent to the user with a link to validate the email address
for that user. The user's profile now has a Full Name field, and the user is able to update
their name and password. Their email remains the identifier for the user and may not be
changed at this time.

### Merged pull requests
- [\#46](https://github.com/rifflearning/riff-rtc/pull/46) Fixed duplicate peer colors hopefully
- [\#48](https://github.com/rifflearning/riff-rtc/pull/48) Profile & sign up overhaul
- [\#50](https://github.com/rifflearning/riff-rtc/pull/50) Add ToS and Privacy Policy to signup page
- [\#51](https://github.com/rifflearning/riff-rtc/pull/51) Update the redisUrl in the dev & prod config to match service name changes in riff-docker
- [\#52](https://github.com/rifflearning/riff-rtc/pull/52) One more time updating the redisUrl in the dev & prod config
- [\#53](https://github.com/rifflearning/riff-rtc/pull/53) make sure firebase name is used if display name is not changed

### Added

- New Profile page allowing updating of the logged in user's information
- User's information now includes a name used as their display name
- Terms of Service and Privacy Policy text and links added to signup page

### Changed

- Chat display name is now pre-populated with a logged in user's name
- The scripts (docker-compose and docker-stack files) in riff-docker have been
  changed giving the services new names. The development and production config
  files were updated to reflect those new names.

### Fixed

- The list of peer colors was being cached w/ the redux persistent state, it no
  longer is, this was an issue when we increased the number of peer colors from 4
  to 7 ([PR#32](https://github.com/rifflearning/riff-rtc/pull/32))


## [1.0.0](https://github.com/rifflearning/riff-rtc/tree/1.0.0) (2019-03-05)
[Full Changelog](https://github.com/rifflearning/riff-rtc/compare/0.3.0...1.0.0)

riff-rtc has 2 parts client and server. 1.0.0 has greatly enhanced both parts.

The client is now a full-fledged Single Page Application, enabling user creation to
preserve access to Riff Metrics, meeting creation, and Metric display.

The server supports much more robust LTI provider capabilities, both for the Emeritus
canvas implementation as well as for edX.

### Merged pull requests

- [\#3](https://github.com/rifflearning/riff-rtc/pull/3) Fix/lti profile signup
- [\#4](https://github.com/rifflearning/riff-rtc/pull/4) Updated actions on keypress and button press for makeMeetingCard
- [\#5](https://github.com/rifflearning/riff-rtc/pull/5) User notification and persistent chat state!
- [\#6](https://github.com/rifflearning/riff-rtc/pull/6) LTI users can no longer launch arbitrary chats from homepage
- [\#7](https://github.com/rifflearning/riff-rtc/pull/7) Chat state persists if you re-join your most recent room
- [\#8](https://github.com/rifflearning/riff-rtc/pull/8) makemeeting reducer responds to CHAT_CHANGE_ROOM_NAME action also
- [\#9](https://github.com/rifflearning/riff-rtc/pull/9) Reset auth state if user is LTI, and only check for LTI auth
- [\#10](https://github.com/rifflearning/riff-rtc/pull/10) update styles pkg to new major version
- [\#11](https://github.com/rifflearning/riff-rtc/pull/11) Fix lti login
- [\#12](https://github.com/rifflearning/riff-rtc/pull/12) removing textchat from the persist blacklist bug
- [\#13](https://github.com/rifflearning/riff-rtc/pull/13) No ugly room names for LTI users in dashboard
- [\#14](https://github.com/rifflearning/riff-rtc/pull/14) text chat persists, but will be cleared on a new meeting
- [\#15](https://github.com/rifflearning/riff-rtc/pull/15) Fix/misc fixes
- [\#16](https://github.com/rifflearning/riff-rtc/pull/16) Feature edX Appsembler LTI support (and update packages)
- [\#17](https://github.com/rifflearning/riff-rtc/pull/17) Feature/influence bar graphs
- [\#18](https://github.com/rifflearning/riff-rtc/pull/18) Feature/influence bar graphs
- [\#19](https://github.com/rifflearning/riff-rtc/pull/19) Sign up users who were not logged in anonymously
- [\#20](https://github.com/rifflearning/riff-rtc/pull/20) Fix emeritus group api change
- [\#21](https://github.com/rifflearning/riff-rtc/pull/21) Users no longer redirected to '/home' on login
- [\#22](https://github.com/rifflearning/riff-rtc/pull/22) Fix stitching and video limits
- [\#23](https://github.com/rifflearning/riff-rtc/pull/23) Fix/login and chat
- [\#24](https://github.com/rifflearning/riff-rtc/pull/24) Fix/influence chart and timeline
- [\#25](https://github.com/rifflearning/riff-rtc/pull/25) Screen sharing
- [\#26](https://github.com/rifflearning/riff-rtc/pull/26) Feature/screen sharing v2
- [\#27](https://github.com/rifflearning/riff-rtc/pull/27) Accessible data tables for dashboard visualizations
- [\#28](https://github.com/rifflearning/riff-rtc/pull/28) Fix/screen sharing
- [\#29](https://github.com/rifflearning/riff-rtc/pull/29) Fix/screen sharing audio
- [\#30](https://github.com/rifflearning/riff-rtc/pull/30) accessible timeline gantt chart
- [\#31](https://github.com/rifflearning/riff-rtc/pull/31) Fix/screen sharing tweaks
- [\#32](https://github.com/rifflearning/riff-rtc/pull/32) Expanded color palette to accommodate 7 people
- [\#33](https://github.com/rifflearning/riff-rtc/pull/33) Fix color strip on bottom of PeerVideos
- [\#34](https://github.com/rifflearning/riff-rtc/pull/34) Fixed the long strings not wrapping in &lt;TextChat />
- [\#35](https://github.com/rifflearning/riff-rtc/pull/35) only add audio from peers who aren't sharing screen
- [\#36](https://github.com/rifflearning/riff-rtc/pull/36) Added alt attribute to main riff logo
- [\#37](https://github.com/rifflearning/riff-rtc/pull/37) Remove role from navbar
- [\#38](https://github.com/rifflearning/riff-rtc/pull/38) Enable user zooming and scaling for a11y improvement
- [\#39](https://github.com/rifflearning/riff-rtc/pull/39) Increased darkness on color of links
- [\#40](https://github.com/rifflearning/riff-rtc/pull/40) Added skip content link - a11y improvement
- [\#41](https://github.com/rifflearning/riff-rtc/pull/41) Added React Helmet to all routes
- [\#42](https://github.com/rifflearning/riff-rtc/pull/42) Added alternative text for loading spinner
- [\#43](https://github.com/rifflearning/riff-rtc/pull/43) Made hamburger menu icon into button instead of div
- [\#44](https://github.com/rifflearning/riff-rtc/pull/44) Unmute audio by default when user hits /room
- [\#45](https://github.com/rifflearning/riff-rtc/pull/45) Linked the correct default messages to both influence type graphs
- [\#47](https://github.com/rifflearning/riff-rtc/pull/47) add some error logging to catch when peers get assigned duplicate colors
- [\#49](https://github.com/rifflearning/riff-rtc/pull/49) multiuser tiling fix

### Added

Change to a Single Page Application was too extensive to enumerate additions.

### Changed

Change to a Single Page Application was too extensive to enumerate changes.

### Fixed

Change to a Single Page Application was too extensive to enumerate fixes.


## [0.3.0](https://github.com/rifflearning/riff-rtc/tree/0.3.0) (2018-06-10)
[Full Changelog](https://github.com/rifflearning/riff-rtc/compare/0.3.0-dev.0...0.3.0)

### Merged pull requests

Note: The github rifflearning/riff-rtc repository was updated as a straight clone
instead of a fork, and older PRs were lost, the links below may no longer be valid.

- [\#3](https://github.com/rifflearning/riff-rtc/pull/3) update packages
  (except build related packages)
- [\#5](https://github.com/rifflearning/riff-rtc/pull/5) Allow configuration
  to be set at runtime instead of statically built into the webpack artifacts
- [mlippert/rhythm-rtc\#2](https://github.com/mlippert/rhythm-rtc/pull/2) update
  feathers packages to match those used by rhythm-server
- [mlippert/rhythm-rtc\#4](https://github.com/mlippert/rhythm-rtc/pull/4) Merge
  updates from jordanreedie's master branch to develop
- [mlippert/rhythm-rtc\#6](https://github.com/mlippert/rhythm-rtc/pull/6) Fix syntax error


### Added

- dataserver path configuration setting. This allows multiple websocket endpoints
  on the same domain, letting the reverse proxy forward them as required)

### Changed

- Configuration settings are supplied to browser at runtime, rather than being
  hardcoded into the artifacts
- Build script supports building development or production artifacts based on
  NODE_ENV value
- Update dependency packages to the latest versions (except build related packages,
  e.g. webpack)
  (will require connecting to a riff-server w/ updated packages (`^0.4.0-dev.2`)
- forked repositories changed _rhythm-_ prefix to _riff-_

### Fixed

- Fix syntax error in browser console on /chat [mlippert/rhythm-rtc\#5](https://github.com/mlippert/rhythm-rtc/issues/5)


## [0.2.0](https://github.com/rifflearning/riff-rtc/tree/0.2.0) (2017-05-17 and prior)
(from `https://github.com/jordanreedie/rhythm-rtc#master` which was
 from `https://github.com/HumanDynamics/rhythm-rtc`)

### Added

- All functionality of rhythm-rtc up until this time. (I am not aware of any distinct
  versions or releases prior to this time. -mjl)
