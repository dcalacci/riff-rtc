import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import {
  attemptUserSignIn,
  clearAuthError,
  changePasswordState,
  changeEmailState,
  forgotPassword,
  cancelForgotPassword,
  resetPassword,
  dismissResetPassword
} from 'redux/actions/auth';

import LogInView from './LogInView';


const mapStateToProps = state => ({
  error: state.auth.error,
  email: state.auth.input.email,
  password: state.auth.input.password,
  isInvalid: state.auth.input.email === '',
  isForgotPassword: state.auth.isForgotPassword,
  resetLinkSent: state.auth.resetLinkSent
});

const mapDispatchToProps = dispatch => ({
  handleLogIn: event => {
    event.preventDefault();
    const {email, password} = event.target.elements;
    dispatch(attemptUserSignIn(email.value, password.value));
  },

  handlePassword: pass => {
    dispatch(changePasswordState(pass));
  },

  handleEmail: email => {
    console.log("handling email...")
    dispatch(changeEmailState(email));
  },

  handleForgotPassword: event => {
    event.preventDefault();
    dispatch(forgotPassword());
  },

  handleCancelForgotClick: event => {
    event.preventDefault();
    dispatch(cancelForgotPassword());
  },

  handleForgotSubmit: event => {
    event.preventDefault();
    let { email } = event.target.elements;
    dispatch(resetPassword(email.value));
  },

  handleDismissResetPassword: event => {
    event.preventDefault();
    dispatch(dismissResetPassword());
  },

  clearError: event => {
    event.preventDefault();
    console.log("event", event, event.target);
    dispatch(clearAuthError());
  }
})

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(LogInView))
