import React from "react";
import { FixedCard } from './styled';
import SubmitButton from './SubmitButton';


const CancelButton = ({handleCancelForgotClick}) => (
  <div className="field" style={{display:'inline-block', marginRight:'10px'}}>
    <div className="control">
      <button
        className="button"
        aria-label="Cancel"
        type="button"
        onClick={handleCancelForgotClick}
      >
        Cancel
      </button>
    </div>
  </div>
);

const ForgotPasswordForm = (props) => (
  <FixedCard>
    <p className="title">
      Forgot Password
    </p>
    <p>A password reset link will be sent to your email address</p>
    <form onSubmit={props.handleForgotSubmit}>
      {props.error && (
        <div id="forgot-pass-error-notification" className="notification is-warning">
          <button
            className="delete"
            onClick={props.clearError}
            aria-label="Close form error message"
          />
          {props.error.message}
        </div>
      )}
      <div className="field">
        <label className="label">Email</label>
        <div className="control" style={{paddingBottom:'10px'}}>
          <input
            className="input"
            type="text"
            name="email"
            value={props.email}
            onChange={event => props.handleEmail(event.target.value)}
          />
        </div>
        <div>
          <CancelButton handleCancelForgotClick={props.handleCancelForgotClick} />
          <SubmitButton isInvalid={props.isInvalid} />
        </div>
        {props.resetLinkSent && (
          <div className="notification is-success">
            <button
              className="delete"
              onClick={props.handleDismissResetPassword}
            />
            A link to reset your password has been sent!
          </div>
        )}
      </div>
    </form>
  </FixedCard>
);

export default ForgotPasswordForm;
