import React, { Component } from "react";
import {Helmet} from "react-helmet";
import { withRouter } from 'react-router-dom';
import { LogInContent, GradientCol } from './styled';
import LogInForm from './LogInForm';
import ForgotPasswordForm from './ForgotPasswordForm';

const LogInView = (props) => (
  <div className="section">
    <Helmet title='Login - Riff' />
    <div className="columns">
      <div className="column">
        <LogInContent>
          <h1> Log in to save your meeting metrics and access your meeting history!</h1>
        </LogInContent>
      </div>
      <GradientCol>
        {props.isForgotPassword ? (
          <ForgotPasswordForm {...props} />
        ) : (
          <LogInForm {...props} />
        )}
      </GradientCol>
    </div>
  </div>
);

export default LogInView;
