import React from 'react';
import { FixedCard } from './styled';
import SubmitButton from './SubmitButton';


const LogInForm = (props) => (
  <FixedCard>
    <p className="title">
      Log In
    </p>
    <p> Don't have an account? <a href="/signup">Create one!</a></p>
    <form onSubmit={props.handleLogIn}>
      {props.error && (
        <div className="notification is-warning">
          <button className="delete" onClick={props.clearError} aria-label="Close form error message"/>
          {props.error.message}
        </div>
      )}
      <div className="field">
        <label className="label">Email (required)</label>
        <div className="control">
          <input
            className="input"
            type="text"
            name="email"
            value={props.email}
            onChange={event => props.handleEmail(event.target.value)}
            required={true}
          />
        </div>
      </div>
      <div className="field">
        <label className="label">Password (required)</label>
        <div className="control">
          <input
            className="input"
            type="password"
            name="password"
            onChange={event => props.handlePassword(event.target.value)}
            required={true}
          />
        </div>
      </div>
      <div style={{marginBottom:'10px'}}>
        <p><a href="#" onClick={props.handleForgotPassword}>Forgot Password?</a></p>
        <SubmitButton isInvalid={props.isInvalid} />
      </div>
    </form>
  </FixedCard>
);

export default LogInForm;
