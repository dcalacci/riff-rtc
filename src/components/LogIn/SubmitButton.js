import React from "react";


const SubmitButton = ({isInvalid}) => (
  <div className="field" style={{display:'inline-block'}}>
    <div className="control">
      <button className="button" type="submit" disabled={isInvalid}>
        Submit
      </button>
    </div>
  </div>
);

export default SubmitButton;
