/* ******************************************************************************
 * styled.js                                                                    *
 * *************************************************************************/ /**
 *
 * @fileoverview React components to apply styling to login components
 *
 * [More detail about the file's contents]
 *
 * Created on       March 5, 2019
 * @author          Jordan Reedie
 *
 * @copyright (c) 2019-present Riff Learning Inc.,
 *            MIT License (see https://opensource.org/licenses/MIT)
 *
 * ******************************************************************************/

import styled from 'styled-components';

const FixedCard = styled.div.attrs({
  className: 'card-content'
})`
  border-radius: 5px;
  background: linear-gradient(30deg, rgba(138,106,148,1) 12%, rgba(171,69,171,1) 87%);
  @media (min-width: 625px) {
    position: fixed;
    width: 40%;
  }
  p {
    color: #fff;
    margin-bottom: 0.5em !important;
  }
  a {
    color: #fff;
    text-decoration: underline;
  }
  a:hover {
    color: #afafaf;
  }
  label {
    color: #fff;
  }
`;

const GradientCol = styled.div.attrs({
  className: 'column'
})`
  height: 100%;
`;

const LogInContent = styled.div.attrs({
  className: 'content'
})`
  max-height: 100%;
`;

/* **************************************************************************** *
 * Module exports                                                               *
 * **************************************************************************** */
export {
  FixedCard,
  GradientCol,
  LogInContent,
};
