/* ******************************************************************************
 * styled.js                                                                    *
 * *************************************************************************/ /**
 *
 * @fileoverview React components to apply styling to NavBar components
 *
 * [More detail about the file's contents]
 *
 * Created on       Oct 07, 2019
 * @author          Brec Hanson
 *
 * @copyright (c) 2019-present Riff Learning Inc.,
 *            MIT License (see https://opensource.org/licenses/MIT)
 *
 * ******************************************************************************/

import styled from 'styled-components';

import { Colors, rgbaColor } from 'libs/utils';

const Brandimg = styled.img.attrs({
  alt: 'Riff homepage'
})`
  height: 75px;
`;

const NavBar = styled.nav.attrs({
  className: 'navbar is-transparent'
})`
  background-color: ${rgbaColor(Colors.white, 0)};
  max-height: unset;
  .navbar-item img {
    max-height: unset;
  }
  .navbar-burger {
    background: none;
    border: none;
  }
`;

/* **************************************************************************** *
 * Module exports                                                               *
 * **************************************************************************** */
export {
  Brandimg,
  NavBar,
};
