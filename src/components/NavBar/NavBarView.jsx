/* ******************************************************************************
 * NavBarView.jsx                                                               *
 * *************************************************************************/ /**
 *
 * @fileoverview React component to render the site-wide nav bar
 *
 * [More detail about the file's contents]
 *
 * Created on       August 1, 2018
 * @author          Dan Calacci
 * @author          Michael Jay Lippert
 * @author          Jordan Reedie
 * @author          Brec Hanson
 *
 * @copyright (c) 2018-present Riff Learning Inc.,
 *            MIT License (see https://opensource.org/licenses/MIT)
 *
 * ******************************************************************************/

import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import riffLogo from '../../../assets/rifflogo.png';

import { Brandimg, NavBar } from './styled';

const ProfileLinks = ({ isLti, loggedIn, handleLogOut }) => {
  if (isLti) {
    return (<div className='navbar-end'/>);
  }

  if (!loggedIn) {
    return (
      <div className='navbar-end'>
        <a className='navbar-item' href='mailto:beta@rifflearning.com'>{'Send Feedback'}</a>
        <Link className='navbar-item' to='/signup'>{'Sign Up'}</Link>
        <Link className='navbar-item' to='/login'>{'Log In'}</Link>
      </div>);
  }

  return (
    <div className='navbar-end'>
      <a className='navbar-item' href='mailto:beta@rifflearning.com'>{'Send Feedback'}</a>
      <Link className='navbar-item' to='/profile'>{'Profile'}</Link>
      <Link className='navbar-item' to='/home' onClick={handleLogOut}>{'Log Out'}</Link>
    </div>
  );
};

ProfileLinks.propTypes = {
  /** did the user authenticate via LTI launch from an LMS or just a regular log in (or anonymous) */
  isLti: PropTypes.bool.isRequired,

  /** is the user logged in */
  loggedIn: PropTypes.bool.isRequired,

  /** handles logging out the user */
  handleLogOut: PropTypes.func.isRequired,
};

class NavBarView extends React.Component {
  static propTypes = {

    /** did the user authenticate via LTI launch from an LMS or just a regular log in (or anonymous) */
    isLti: PropTypes.bool.isRequired,

    /** is the user logged in */
    loggedIn: PropTypes.bool.isRequired,

    /** is the mobile hamburger menu open */
    menuOpen: PropTypes.bool.isRequired,

    /** handles logging out the user */
    handleLogOut: PropTypes.func.isRequired,

    /** handles the hamburger menu icon being clicked */
    handleBurgerClick: PropTypes.func.isRequired,
  };

  render() {
    const menuOpenClass = this.props.menuOpen ? 'is-active' : '';

    return (
      <NavBar aria-label='main navigation'>
        <div className='navbar-brand'>
          <Link className='navbar-item' to='/home'>
            <Brandimg src={riffLogo}/>
          </Link>
          <button
            role='button'
            aria-expanded={this.props.menuOpen}
            className={`navbar-burger burger ${menuOpenClass}`}
            onClick={event => this.props.handleBurgerClick(event)}
          >
            <span/>
            <span/>
            <span/>
          </button>
        </div>
        <div className={`navbar-menu ${menuOpenClass}`}>
          <div className='navbar-start'>
            <Link className='navbar-item' to='/room'>{'Riff Video'}</Link>
            <Link className='navbar-item' to='/riffs'>{'Riff Metrics'}</Link>
            <a
              className='navbar-item'
              href='https://www.riffanalytics.ai'
              target='_blank'
              rel='noreferrer noopener'
            >
              {'Learn More'}
            </a>
          </div>
          <ProfileLinks
            isLti={this.props.isLti}
            loggedIn={this.props.loggedIn}
            handleLogOut={this.props.handleLogOut}
          />
        </div>
      </NavBar>
    );
  }
}

/* **************************************************************************** *
 * Module exports                                                               *
 * **************************************************************************** */
export {
  NavBarView,
};
