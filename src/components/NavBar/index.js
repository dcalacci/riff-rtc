/* ******************************************************************************
 * index.js                                                                     *
 * *************************************************************************/ /**
 *
 * @fileoverview [summary of file contents]
 * @fileoverview Hook up the Dashboard to redux state and actions
 *
 * [More detail about the file's contents]
 *
 * Created on       August 1, 2018
 * @author          Dan Calacci
 *
 * @copyright (c) 2018-present Riff Learning, Inc.,
 *            MIT License (see https://opensource.org/licenses/MIT)
 *
 * ******************************************************************************/

import { connect } from 'react-redux';

import { CLOSE_NAV_MENU, OPEN_NAV_MENU } from 'redux/constants/ActionTypes';
import { logOutUser } from 'redux/actions/auth';
import { logoutLTIUser } from 'redux/actions/lti';

import { logger } from 'libs/utils';

import { NavBarView } from './NavBarView';

const mapStateToProps = state => ({
  loggedIn: state.auth.loggedIn && !state.auth.anonymous,
  menuOpen: state.menu.menuOpen,
  isLti: state.lti.loggedIn,
});

const mapDispatchToProps = dispatch => ({
  handleLogOut: (/* event */) => {
    dispatch(logOutUser());
    dispatch(logoutLTIUser());
  },
  openMenu: () => {
    dispatch({ type: OPEN_NAV_MENU });
  },
  closeMenu: () => {
    dispatch({ type: CLOSE_NAV_MENU });
  },
});

const mapMergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps,
  withRef: true,
  handleBurgerClick: (/* event */) => {
    logger.debug('NavBar:handleBurgerClick: clicked burger!', stateProps);
    if (!stateProps.menuOpen) {
      dispatchProps.openMenu();
    }
    else {
      dispatchProps.closeMenu();
    }
  },
});

const ConnectedNavBar = connect(
  mapStateToProps,
  mapDispatchToProps,
  mapMergeProps,
)(NavBarView);

/* **************************************************************************** *
 * Module exports                                                               *
 * **************************************************************************** */
export {
  ConnectedNavBar as NavBar,
};
