/* ******************************************************************************
 * App.jsx                                                                      *
 * *************************************************************************/ /**
 *
 * @fileoverview Top level react component of the Riff Platform SPA
 *
 * Note: This has been the main page of the "app" from when this was rythm-rtc
 * and was not a single page app.
 *
 * Created on       August 7, 2017
 * @author          Jordan Reedie
 * @author          Dan Calacci
 *
 * @copyright (c) 2017-present Riff Learning Inc.,
 *            MIT License (see https://opensource.org/licenses/MIT)
 *
 * ******************************************************************************/

import React from 'react';
import PropTypes from 'prop-types';
import { Route, Router } from 'react-router';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { replace } from 'connected-react-router';
import { ScaleLoader } from 'react-spinners';
import styled from 'styled-components';

import addAuthListener from 'redux/listeners/auth';
import { attemptRiffAuthenticate } from 'redux/actions/riff';
import { loginLTIUser } from 'redux/actions/lti';
import { Colors, isCSSAnimationsSupported, logger } from 'libs/utils';

import { browserHistory } from '../history';

import { Home } from './Home';
import SignUp from './SignUp';
import LogIn from './LogIn';
import { Profile } from './Profile';
import { Chat } from './Chat';
import { NavBar } from './NavBar';
import { Dashboard } from './Dashboard';

const SkipContent = styled.div.attrs({
  className: 'skip-content'
})`
  padding: 6px;
  position: absolute;
  top: -40px;
  left: 0px;
  border-right: 1px solid #4a4a4a;
  border-bottom: 1px solid #4a4a4a;
  border-bottom-right-radius: 3px;
  background:  #f6f0fb;
  -webkit-transition: top .5s ease-out;
  transition: top .5s ease-out;
  z-index: 100;

  a {
    padding: 3px;
  }

  &:focus-within {
    top: 0px;
  }
`;

const LoadingTextAlt = styled.footer.attrs({
  className: 'loading-text-alt'
})`
  color: ${Colors.lightRoyal};
  font-size: 20px;
`;

const LoadingView = () => {
  const loadingIndication = isCSSAnimationsSupported()
    ? <ScaleLoader color={Colors.lightRoyal}/>
    : <LoadingTextAlt>{'Loading...'}</LoadingTextAlt>;

  return (
    <div className='columns has-text-centered is-centered is-vcentered' style={{ minHeight: '100vh' }}>
      <div className='column is-vcentered has-text-centered' aria-label='loading'>
        {loadingIndication}
      </div>
    </div>
  );
};

const isLoaded = (state) => {
  // Wait until we've authenticated with the riff data-server before allowing
  // any other operations.
  return Boolean(state.riff.authToken);

  // NOTE: We were attempting to show the loading view until there was an
  // authenticated firebase user signed in. However, we only need a firebase
  // user for chat (and to populate the dashboard, but it seems reasonable
  // to show no meetings if no user is signed in, because the best we'd do is
  // create a brand new anonymous user who wouldn't have any meetings.
  //
  // if (window.lti_data.lti_user && window.lti_data.is_valid) {
  //   return state.lti.loggedIn && state.riff.authToken;
  // } else {
  //   return ((state.auth.user.uid || state.auth.uid) && state.riff.authToken);
  // }
};

const mapStateToProps = state => ({
  auth: state.auth,
  riff: state.riff,
  isLoaded: isLoaded(state),
});

const mapDispatchToProps = dispatch => ({
  authenticateRiff: () => {
    logger.debug('App: attempt data-server auth');
    dispatch(attemptRiffAuthenticate());
  },
  loginLTIUser: (data) => {
    return dispatch(loginLTIUser(data));
  },
  changeRoute: (path) => {
    dispatch(replace(path));
  },
  dispatch: dispatch
});

// App component
class App extends React.Component {
  static propTypes = {

    /** true when the connection to the riffdata server has been established */
    isLoaded: PropTypes.bool.isRequired,

    /**  */
    auth: PropTypes.object.isRequired,

    /**  */
    riff: PropTypes.object.isRequired,

    /**  */
    authenticateRiff: PropTypes.func.isRequired,

    /**  */
    loginLTIUser: PropTypes.func.isRequired,

    /**  */
    changeRoute: PropTypes.func.isRequired,

    // match, location and history are props added by withRouter() from 'react-router-dom'
    /** contains information about how a <Route path> matched the URL */
    match: PropTypes.shape({
      params: PropTypes.object,
      isExact: PropTypes.bool,
      path: PropTypes.string,
      url: PropTypes.string,
    }),

    /** represents where the app is now, where you want it to go, or even where it was */
    location: PropTypes.shape({
      key: PropTypes.string,
      pathname: PropTypes.string,
      search: PropTypes.string,
      hash: PropTypes.string,
      state: PropTypes.object,
    }).isRequired,

    /** allows you to manage and handle the browser history */
    history: PropTypes.shape({
      length: PropTypes.number,
      action: PropTypes.string,
      location: PropTypes.object,
      push: PropTypes.func,
      replace: PropTypes.func,
      go: PropTypes.func,
      goBack: PropTypes.func,
      goForward: PropTypes.func,
      block: PropTypes.func,
    }),

    /** redux action dispatch function */
    dispatch: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);
    logger.debug('App: Start the AuthListener');
    addAuthListener(this.props.dispatch,
                    this.props.auth);
  }

  // TODO: We should do this in some other way and not use an UNSAFE method. -mjl 2019-10-21
  UNSAFE_componentWillMount() { // eslint-disable-line camelcase
    if (!this.props.riff.token) {
      this.props.authenticateRiff();
    }

    // if window was loaded with authenticated LTI data
    if (window.lti_data.lti_user && window.lti_data.is_valid) {
      logger.debug('App.WillMount: found LTI user, logging them in', { lti_data: window.lti_data, props: this.props });
      this.props.loginLTIUser(window.lti_data)
        .then(() => {
          const ltiLaunchPathPrefix = '/lti/launch';
          let launchPath = this.props.location.pathname;
          if (launchPath.startsWith(ltiLaunchPathPrefix)) {
            launchPath = launchPath.slice(ltiLaunchPathPrefix.length);
            launchPath = launchPath || '/';
            this.props.changeRoute(launchPath);
          }

          // We're done at this point, return undefined to indicate that.
          return undefined;
        })
        .catch((error) => {
          logger.error('App.WillMount: Failed to log in an LTI user', { error, lti_data: window.lti_data });
        });
    }
    else if (!this.props.auth.user.uid && !this.props.auth.uid) {
      logger.debug('App.WillMount: No user (named or anonymous) detected');
    }
  }

  componentDidMount() {
    logger.debug('App: component loaded.');
  }

  componentDidUpdate(prevProps) {
    if (prevProps.location.pathname !== this.props.location.pathname) {
      if (document.activeElement) {
        document.activeElement.blur();
        document.body.focus();
      }
    }
  }

  render() {
    return (
      <div>
        <title>{'Riff'}</title>
        <Router history={browserHistory}>
          <div style={{ maxHeight: '100vh' }}>
            <SkipContent>
              <a href={`${this.props.location.pathname}#main-content-container`}>{'skip to main content'}</a>
            </SkipContent>
            <NavBar/>
            {!this.props.isLoaded ? (
              <LoadingView/>
            ) : (
              <div id='main-content-container'>
                <Route path='(/home|.|/)' component={Home}/>
                <Route exact={true} path='/room' component={Chat}/>
                <Route exact={true} path='/signup' component={SignUp}/>
                <Route exact={true} path='/login' component={LogIn}/>
                <Route exact={true} path='/profile' component={Profile}/>
                <Route exact={true} path='/riffs' component={Dashboard}/>
              </div>
            )}
          </div>
        </Router>
      </div>
    );
  }
}

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(App));
