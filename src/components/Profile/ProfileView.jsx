/* ******************************************************************************
 * ProfileView.jsx                                                              *
 * *************************************************************************/ /**
 *
 * @fileoverview React component for the Profile
 *
 * The User Profile page presents information about the currently logged in
 * user, along with the ability to change some of that information.
 *
 * Created on       Aug 2, 2018
 * @author          Dan Calacci
 * @author          Jordan Reedie
 * @author          Mike Lippert
 * @author          Brec Hanson
 *
 * @copyright (c) 2018-present Riff Learning Inc.,
 *            MIT License (see https://opensource.org/licenses/MIT)
 *
 * ******************************************************************************/

import React from 'react';
import PropTypes from 'prop-types';

import { VerifiedProfile } from './VerifiedProfile/VerifiedProfile';
import { UnverifiedProfile } from './UnverifiedProfile/UnverifiedProfile';


class ProfileView extends React.Component {
  static propTypes = {

    /** the logged in user object, this page should not be rendered for unlogged in users */
    user: PropTypes.shape({
      verified: PropTypes.boolean,
    }).isRequired,
  };

  render() {
    let profile;
    if (this.props.user.verified) {
      profile = <VerifiedProfile {...this.props}/>;
    }
    else {
      profile = <UnverifiedProfile {...this.props}/>;
    }

    return (
      <div className='section'>
        {profile}
      </div>
    );
  }
}

/* **************************************************************************** *
 * Module exports                                                               *
 * **************************************************************************** */
export {
  ProfileView,
};
