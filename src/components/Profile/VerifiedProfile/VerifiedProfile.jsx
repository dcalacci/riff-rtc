/* ******************************************************************************
 * VerifiedProfile.jsx                                                          *
 * *************************************************************************/ /**
 *
 * @fileoverview React component for the Profile
 *
 * [More detail about the file's contents]
 *
 * Created on       Feb 28, 2019
 * @author          Jordan Reedie
 *
 * @copyright (c) 2019-present Riff Learning Inc.,
 *            MIT License (see https://opensource.org/licenses/MIT)
 *
 * ******************************************************************************/

import React from 'react';
import PropTypes from 'prop-types';

import { Notification } from 'components/Notification';

import { BasicProfileElement } from './BasicProfileElement';
import { EditableProfileElement } from './EditableProfileElement';

class VerifiedProfile extends React.Component {
  static propTypes = {

    /** A user object containing data from auth.user in the store */
    user: PropTypes.object,

    /** A function to be invoked when the user requests to reset their password */
    handleResetPassword: PropTypes.func.isRequired,

    /** A string which can be considered the user's display name in this context */
    displayNameInput: PropTypes.string.isRequired,

    /** A function to be invoked when the user edits the text in the display name text input */
    handleDisplayNameInput: PropTypes.func.isRequired,

    /** A function to be invoked when the user submits a request to update their display name */
    handleDisplayNameSubmit: PropTypes.func.isRequired,

    /** A string denoting the status of a request to update the user's display name */
    displayNameStatus: PropTypes.string.isRequired,

    /** A function to be invoked when the user requests the notification containing
     *  the status of a display name update be dismissed
     */
    clearDisplayNameError: PropTypes.func.isRequired,

    /** When a request to update the user's display name has errored,
     *  this string contains the error message
     */
    displayNameMessage: PropTypes.string.isRequired,

    /** true when the user has requested to update their password */
    resetPassRequested: PropTypes.bool.isRequired,

    /** A function to be invoked when the user requests that the notification confirming
     *  that a password reset email has been sent be dismissed
     */
    dismissResetPassword: PropTypes.func.isRequired,
  };

  render() {
    const containerClassNames = [ 'column',
                                  'is-full-mobile',
                                  'is-half-tablet',
                                  'is-one-third-desktop',
                                  'is-one-quarter-widescreen',
                                  'is-offset-1',
                                  'has-text-left',
    ];
    return (
      <div className='columns'>
        <div className={containerClassNames.join(' ')}>
          <div style={{ marginBottom: '20px' }}>
            <h1 className='is-size-3'>{'Profile'}</h1>
            <p className='is-size-6'>
              {'Update your name or change your password.'}
            </p>
          </div>
          <BasicProfileElement fieldName='Email'>
            <p>{this.props.user.email}</p>
          </BasicProfileElement>
          <BasicProfileElement fieldName='Password'>
            <p>
              {'Want to change your password?'}&nbsp;
              <a href='#' onClick={this.props.handleResetPassword}>{'Reset it'}</a> {'here.'}
            </p>
          </BasicProfileElement>
          <EditableProfileElement
            fieldName='Full Name'
            fieldInput={this.props.displayNameInput}
            handleInput={this.props.handleDisplayNameInput}
          />
          <div className='control' style={{ marginBottom: '15px' }}>
            <button
              className='button is-primary'
              onClick={() => this.props.handleDisplayNameSubmit(this.props.displayNameInput)}
            >
              {'Update'}
            </button>
          </div>

          <Notification
            show={this.props.displayNameStatus === 'success'}
            type='is-success'
            onClose={this.props.clearDisplayNameError}
          >
            {'Profile updated!'}
          </Notification>

          <Notification
            show={this.props.displayNameStatus === 'error'}
            type='is-warning'
            onClose={this.props.clearDisplayNameError}
          >
            {this.props.displayNameMessage}
          </Notification>

          <Notification
            show={this.props.resetPassRequested}
            type='is-success'
            onClose={this.props.dismissResetPassword}
            style={{ marginTop: '10px' }}
          >
            {'A password reset link has been sent to your email address!'}
          </Notification>
        </div>
      </div>
    );
  }
}

/* **************************************************************************** *
 * Module exports                                                               *
 * **************************************************************************** */
export {
  VerifiedProfile,
};
