/* ******************************************************************************
 * UnverifiedProfile.jsx                                                        *
 * *************************************************************************/ /**
 *
 * @fileoverview React component for the Profile
 *
 * [More detail about the file's contents]
 *
 * Created on       February 28, 2019
 * @author          Jordan Reedie
 *
 * @copyright (c) 2019-present Riff Learning Inc.,
 *            MIT License (see https://opensource.org/licenses/MIT)
 *
 * ******************************************************************************/

import React from 'react';
import PropTypes from 'prop-types';

import { Notification } from 'components/Notification';

import { SentEmailVerification } from './SentEmailVerification';


class UnverifiedProfile extends React.Component {
  static propTypes = {

    /** A user object containing the email of the user that is currently logged in */
    user: PropTypes.shape({
      email: PropTypes.string,
    }).isRequired,

    /** Function to be invoked when the user requests that the email verification be resent */
    resendVerification: PropTypes.func.isRequired,

    /** true when the user requested that the email verification be resent */
    resendVerificationRequested: PropTypes.bool,

    /** Function to be invoked when the user requests that the notification
     *  confirming that a email verification has been sent be dismissed
     */
    dismissResendVerification: PropTypes.func.isRequired,
  };

  render() {
    return (
      <div className='columns' style={{ paddingTop: '5vh' }}>
        <div className='column is-half is-offset-one-quarter is-centered has-text-centered'>
          <SentEmailVerification
            email={this.props.user.email}
            resendVerification={this.props.resendVerification}
          />
          <Notification
            show={this.props.resendVerificationRequested}
            type='is-success'
            onClose={this.props.dismissResendVerification}
            style={{ marginTop: '10px' }}
          >
            {'A new verification link has been sent to your email address!'}
          </Notification>
        </div>
      </div>
    );
  }
}

/* **************************************************************************** *
 * Module exports                                                               *
 * **************************************************************************** */
export {
  UnverifiedProfile,
};
