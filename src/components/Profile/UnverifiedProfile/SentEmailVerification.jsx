/* ******************************************************************************
 * SentEmailVerification.jsx                                                    *
 * *************************************************************************/ /**
 *
 * @fileoverview React component for the UnverifiedProfile
 *
 * [More detail about the file's contents]
 *
 * Created on       May 28, 2019
 * @author          Brec Hanson
 *
 * @copyright (c) 2019-present Riff Learning Inc.,
 *            MIT License (see https://opensource.org/licenses/MIT)
 *
 * ******************************************************************************/

import React from 'react';
import PropTypes from 'prop-types';

class SentEmailVerification extends React.Component {
  static propTypes = {

    /** The email of the user that is currently logged in */
    email: PropTypes.string,

    /** Function to be invoked when the user requests that the email notification be resent */
    resendVerification: PropTypes.func.isRequired,
  };

  render() {
    return (
      <div>
        <h1 className='is-size-1'>{'Thanks for signing up!'}</h1>
        <p className='is-size-5'>
          {`We've sent an email to ${this.props.email}, with a link to verify your account.`}
        </p>
        <p>
          {'Didn\'t get an email?'}&nbsp;
          <a href='#' onClick={this.props.resendVerification}>{'Click here'}</a>{' to resend it.'}
        </p>
      </div>
    );
  }
}

/* **************************************************************************** *
 * Module exports                                                               *
 * **************************************************************************** */
export {
  SentEmailVerification,
};
