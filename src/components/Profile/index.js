/* ******************************************************************************
 * index.js                                                                     *
 * *************************************************************************/ /**
 *
 * @fileoverview React Profile component attached to the router and redux store
 *
 * [More detail about the file's contents]
 *
 * Created on       Aug 2, 2018
 * @author          Jordan Reedie
 * @author          Dan Calacci
 * @author          Mike Lippert
 *
 * @copyright (c) 2018-present Riff Learning Inc.,
 *            MIT License (see https://opensource.org/licenses/MIT)
 *
 * ******************************************************************************/

import { connect } from 'react-redux';

import {
  clearDisplayNameError,
  dismissResendVerification,
  dismissResetPassword,
  handleChangeDisplayName,
  handleDisplayNameInput,
  handleResetPassword,
  resendVerification
} from 'redux/actions/profile';

import { ProfileView } from './ProfileView';


const processMessage = (msg, field) => {
  if (msg !== null && msg.indexOf('This operation') > -1) {
    return `Oops! For your security, we need you to log in again before you can change your ${field}.`;
  }
  return msg;
};

const mapStateToProps = state => ({
  user: state.auth.user,
  displayNameMessage: processMessage(state.profile.changeDisplayNameMessage, 'name'),
  displayNameStatus: state.profile.changeDisplayNameStatus,
  displayNameInput: state.profile.displayNameInput === null
    ? state.auth.user.displayName || ''
    : state.profile.displayNameInput,
  resetPassRequested: state.profile.resetPassRequested,
  resendVerificationRequested: state.profile.resendVerificationRequested
});

const mapDispatchToProps = dispatch => ({
  clearDisplayNameError: () => {
    dispatch(clearDisplayNameError());
  },
  handleDisplayNameSubmit: (displayName) => {
    dispatch(handleChangeDisplayName(displayName));
  },
  handleDisplayNameInput: (e) => {
    e.preventDefault();
    dispatch(handleDisplayNameInput(e.target.value));
  },
  handleResetPassword: (e) => {
    e.preventDefault();
    dispatch(handleResetPassword());
  },
  dismissResetPassword: () => {
    dispatch(dismissResetPassword());
  },
  resendVerification: (e) => {
    e.preventDefault();
    dispatch(resendVerification());
  },
  dismissResendVerification: (e) => {
    e.preventDefault();
    dispatch(dismissResendVerification());
  }

});

const ConnectedProfile = connect(mapStateToProps,
                                 mapDispatchToProps)(ProfileView);

/* **************************************************************************** *
 * Module exports                                                               *
 * **************************************************************************** */
export {
  ConnectedProfile as Profile,
};
