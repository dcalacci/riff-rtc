/* ******************************************************************************
 * MeetingList.jsx                                                              *
 * *************************************************************************/ /**
 *
 * @fileoverview React Riff Meeting List component
 *
 * Presents a list of Riff meetings allowing one to be selected, calling a
 * supplied onSelectionChanged function.
 *
 * Created on       May 29, 2019
 * @author          Michael Jay Lippert
 *
 * @copyright (c) 2019-present Riff Learning Inc.,
 *            MIT License (see https://opensource.org/licenses/MIT)
 *
 * ******************************************************************************/

// I haven't figured out for sure if we(Riff) want to use the underscore prefix to
// signify private properties/methods or not, and I've used that convention in here
// for now, so I'm disabling no-underscore-dangle. -mjl
/* eslint
    no-underscore-dangle: off,
 */

import React from 'react';
import PropTypes from 'prop-types';

import { logger } from 'libs/utils';

import { MeetingTabs } from './styled';
import { MeetingListElement } from './MeetingListElement';

/* ******************************************************************************
 * MeetingList                                                             */ /**
 *
 * React component to present a list of meetings.
 *
 ********************************************************************************/
class MeetingList extends React.Component {
    static propTypes = {

        /** The list of meeting objects to be presented */
        meetings: PropTypes.array.isRequired,

        /** The meeting object that is currently selected or null if none is selected
         *  Note: PropTypes has no native way to distinguish between null and undefined
         *        allowing null is why this is "optional"
         */
        selectedMeeting: PropTypes.object,

        /** function to invoke when a meeting is clicked on (will select the meeting) */
        onSelectionChanged: PropTypes.func.isRequired,
    };


    /* **************************************************************************
     * render                                                              */ /**
     *
     * Required method of a React component.
     * @see {@link https://reactjs.org/docs/react-component.html#render|React.Component.render}
     */
    render() {
        logger.debug('MeetingList.render: rendering meetings in list');
        return (
            <MeetingTabs>
                <div className='timeline-header' style={{ minHeight: '2em' }}>
                    <span className='tag is-medium is-inverted is-primary'>{'Today'}</span>
                </div>
                {this._getMeetingTiles()}
            </MeetingTabs>
        );
    }

    /* **************************************************************************
     * _getMeetingTiles (private)                                          */ /**
     *
     * Returns an array of MeetingListElements for all of the meetings that
     * should be displayed in this MeetingList.
     *
     * (currently *ALL* meetings in the meeting list should be displayed)
     *
     * @returns {Array<MeetingListElement>}
     */
    _getMeetingTiles() {
        return this.props.meetings
            .map((meeting) => {
                const selected = this.props.selectedMeeting !== null &&
                                 meeting._id === this.props.selectedMeeting._id;
                return (
                    <MeetingListElement
                        key={meeting._id}
                        meeting={meeting}
                        selected={selected}
                        handleClick={this.props.onSelectionChanged}
                    />
                );
            });
    }
}

/* **************************************************************************** *
 * Module exports                                                               *
 * **************************************************************************** */
export {
    MeetingList,
};
