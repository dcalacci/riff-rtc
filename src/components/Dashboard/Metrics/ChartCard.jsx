/* ******************************************************************************
 * ChartCard.jsx                                                                *
 * *************************************************************************/ /**
 *
 * @fileoverview React component for charts displayed on the dashboard
 *
 * [More detail about the file's contents]
 *
 * Created on       October 12, 2018
 * @author          Dan Calacci
 *
 * @copyright (c) 2018-present Riff Learning Inc.,
 *            MIT License (see https://opensource.org/licenses/MIT)
 *
 * ******************************************************************************/

// I haven't figured out for sure if we(Riff) want to use the underscore prefix to
// signify private properties/methods or not, and I've used that convention in here
// for now, so I'm disabling no-underscore-dangle. -mjl
/* eslint
    no-underscore-dangle: off,
 */

import React from 'react';
import PropTypes from 'prop-types';
import InfoIcon from '@material-ui/icons/Info';

import { logger } from 'libs/utils';

import {
    StyledCard,
    StyledCardTitle,
    StyledChart,
} from './styled';
import { ChartCardInfo } from './ChartCardInfo';


/* ******************************************************************************
 * ChartCard                                                               */ /**
 *
 * React component to present a Chart on the dashboard.
 *
 ********************************************************************************/
class ChartCard extends React.Component {
    static propTypes = {

        /** Unique string usable as an element ID for the ChartCard */
        chartCardId: PropTypes.string.isRequired,

        /** Title of the card */
        title: PropTypes.string.isRequired,

        /** Description of the presented chart */
        chartInfo: PropTypes.string.isRequired,

        /** React renderable element that is the focus of this chart card, ie the
         *  actual chart, and perhaps also an a11y table representation of
         *  that chart if desired.
         */
        children: PropTypes.node.isRequired,

        /** flag for an especially long description that requires smaller text. */
        longDescription: PropTypes.bool,
    };

    constructor(props) {
        super(props);

        this.state = {
            isInfoOpen: false,
        };

        this._infoBtn = React.createRef();

        // We need to know when the info was closed after being open
        // so we can set the focus to the info button to open it again
        // but this property isn't state because we don't want to re-render
        // when it changes.
        this._infoJustClosed = false;
    }

    componentDidUpdate() {
        // We want to have the focus on the info button when the chart card is
        // rendered immediately after the displayed info was closed by the user
        if (this._infoJustClosed) {
            this._infoBtn.current.focus();
            this._infoJustClosed = false;
        }
    }

    /* **************************************************************************
     * render                                                              */ /**
     *
     * Required method of a React component.
     * @see {@link https://reactjs.org/docs/react-component.html#render|React.Component.render}
     */
    render() {
        logger.debug('ChartCard.render: children', this.props.children);
        const infoId = `chart-info-${this.props.chartCardId}`;

        return (
            <StyledCard>
                {/* Use a column flexbox to allow the chart to adjust to the space not
                    occupied by the card title, remember only the direct children of the
                    flex container will be adjusted. */}
                <div style={{ height: '100%', display: 'flex', flexDirection: 'column' }}>
                    <div>
                        <StyledCardTitle>
                            {this.props.title}
                            <span className="has-text-right chart-card-icon" style={{ 'float': 'right' }}>
                                <button
                                    onClick={() => this.setState({ isInfoOpen: true })}
                                    aria-describedby={infoId}
                                    tabIndex='-1'
                                    ref={this._infoBtn}
                                >
                                    <InfoIcon/>
                                </button>
                            </span>
                        </StyledCardTitle>
                    </div>
                    <div style={{ height: '100%', minHeight: 0, flexGrow: 1, flexShrink: 1 }}>
                        <StyledChart>
                            {this.props.children}
                        </StyledChart>
                    </div>
                </div>
                {this.state.isInfoOpen &&
                    <ChartCardInfo
                        id={infoId}
                        close={() => {
                            this.setState({ isInfoOpen: false });
                            this._infoJustClosed = true;
                        }}
                        chartInfo={this.props.chartInfo}
                        longDescription={this.props.longDescription}
                    />
                }
            </StyledCard>
        );
    }
}

/* **************************************************************************** *
 * Module exports                                                               *
 * **************************************************************************** */
export {
    ChartCard,
};
