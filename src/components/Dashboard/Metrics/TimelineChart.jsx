/* ******************************************************************************
 * TimelineChart.jsx                                                            *
 * *************************************************************************/ /**
 *
 * @fileoverview React component chart of the meeting speaking timeline
 *
 * [More detail about the file's contents]
 *
 * DevNote: I referred to https://www.smashingmagazine.com/2018/02/react-d3-ecosystem/
 *    when integrating the d3 gantt chart w/ this react component.
 *    I did the 1st simple integration, but also see the lifecycle wrapping
 *    section which will require a little more work on the d3 class.
 *
 * Created on       October 5, 2018
 * @author          Dan Calacci
 * @author          Michael Jay Lippert
 *
 * @copyright (c) 2018-present Riff Learning Inc.,
 *            MIT License (see https://opensource.org/licenses/MIT)
 *
 * ******************************************************************************/

// I haven't figured out for sure if we(Riff) want to use the underscore prefix to
// signify private properties/methods or not, and I've used that convention in here
// for now, so I'm disabling no-underscore-dangle. -mjl
/* eslint
    no-underscore-dangle: off,
 */

import React from 'react';
import PropTypes from 'prop-types';
import { ScaleLoader } from 'react-spinners';

import { Colors, PeerColors, getColorMap, logger } from 'libs/utils';

import { Gantt } from './gantt';
import { ChartCard } from './ChartCard';
import { WavyBackground } from './WavyBackground';


/**
 *  Chart configuration properties
 */
const chartConfig = {
    cardTitle: 'Timeline',
    info: 'This chart shows a timeline of when people spoke during your meeting.',
};

/* ******************************************************************************
 * TimelineChart                                                           */ /**
 *
 * React component to visualize the timeline of a Riff meeting.
 *
 * TODO:
 *  - the size of the chart should be specified in a prop somehow, not sure
 *    if the size should be for the whole chart, chart card and all or just the
 *    chartDiv. -mjl 2019-05-31
 ********************************************************************************/
class TimelineChart extends React.PureComponent {
    static propTypes = {

        /** meeting whose timeline data will be in timelineData */
        meeting: PropTypes.shape({
            _id: PropTypes.string.isRequired,
            participants: PropTypes.instanceOf(Set).isRequired,
        }),

        /** ID of the logged in user so their data can be distinguished */
        participantId: PropTypes.string.isRequired,

        /** status of the timelineData */
        timelineStatus: PropTypes.oneOf([ 'loading', 'loaded', 'error' ]).isRequired,

        /** data about who spoke when in the meeting */
        timelineData: PropTypes.shape({
            sortedParticipants: PropTypes.array,
            utts: PropTypes.array,
            startTime: PropTypes.string,
            endTime: PropTypes.string,
        }),
    };

    /* **************************************************************************
     * constructor                                                         */ /**
     *
     * TimelineChart class constructor.
     */
    constructor(props) {
        super(props);

        this.state = {
            chartWidth: this._getChartWidth(),
        };

        /** The parent node in this component that the d3 gantt chart will draw in */
        this._ganttNode = React.createRef();

        /** The timeline chart instance */
        this._gantt = new Gantt()
            .setTitle('Timeline Chart');

        // bind this for methods to be passed to others
        this._onResize = this._onResize.bind(this);
    }

    /* **************************************************************************
     * componentDidMount                                                   */ /**
     *
     * Lifecycle method of a React component.
     * This is invoked immediately after a component is mounted (inserted into the
     * tree). Initialization that requires DOM nodes should go here.
     *
     * Draw the d3 gantt chart w/ the _ganttNode as its parent node.
     *
     * @see {@link https://reactjs.org/docs/react-component.html#componentdidmount|React.Component.componentDidMount}
     */
    componentDidMount() {
        // we need to update the gantt chart when the window changes size
        window.addEventListener('resize', this._onResize);

        const meetingId = this.props.meeting ? this.props.meeting._id : 'no-selected-meeting';
        logger.debug(`TimelineChart: drawing gantt chart for meeting ${meetingId}.`, this.props);
        this._updateGantt();
    }

    /* **************************************************************************
     * componentWillUnmount                                                */ /**
     *
     * Lifecycle method of a React component.
     */
    componentWillUnmount() {
        window.removeEventListener('resize', this._onResize);
    }

    /* **************************************************************************
     * componentDidUpdate                                                  */ /**
     *
     * componentDidUpdate() is invoked immediately after updating occurs. This
     * method is not called for the initial render.
     *
     * Use this as an opportunity to operate on the DOM when the component has
     * been updated.
     */
    componentDidUpdate() {
        logger.debug('TimelineChart: component updating...', this.props);
        this._updateGantt();
    }

    /* **************************************************************************
     * render                                                              */ /**
     *
     * Required method of a React component.
     * @see {@link https://reactjs.org/docs/react-component.html#render|React.Component.render}
     */
    render() {
        if (this.props.timelineStatus !== 'loaded') {
            const loadingDisplay = (
                <div>
                    <ScaleLoader color={Colors.lightRoyal}/>
                </div>
            );

            return this._decorateContent(loadingDisplay);
        }

        const ganttDiv = <div ref={this._ganttNode}/>;

        return this._decorateContent(ganttDiv);
    }

    /* **************************************************************************
     * _decorateContent                                                    */ /**
     *
     * Decorate the given content for display.
     *
     * @param {React.node} content
     *
     * @returns {React.element}
     */
    _decorateContent(content) {
        const meetingId = this.props.meeting ? this.props.meeting._id : 'no-selected-meeting';
        const chartCardId = `cc-${meetingId}-${chartConfig.cardTitle.replace(' ', '-')}`;

        return (
            <WavyBackground>
                {/* set the width for the chart card to use and center it on the background */}
                <div style={{ margin: 'auto', width: '90%' }}>
                    <ChartCard
                        title={chartConfig.cardTitle}
                        chartInfo={chartConfig.info}
                        chartCardId={chartCardId}
                    >
                        {content}
                    </ChartCard>
                </div>
            </WavyBackground>
        );
    }

    /* **************************************************************************
     * _onResize                                                           */ /**
     *
     * Handle a resize event of the window, which we want to resize the gantt
     * chart.
     */
    _onResize(e) {
        logger.debug('TimelineChart._onResize:', e);
        this.setState({ chartWidth: this._getChartWidth() });
    }

    /* **************************************************************************
     * _getChartWidth                                                      */ /**
     *
     * Get the width to use for the gantt chart. Currently this is based on the
     * current width of the document body (resizing the browser will change
     * the body width).
     *
     * @returns {number}
     */
    _getChartWidth() {
        return document.body.clientWidth / 2;
    }

    /* **************************************************************************
     * _updateGantt                                                        */ /**
     *
     * Update the d3 gantt chart if needed.
     */
    _updateGantt() {
        // No timelineData available means don't display the gantt chart
        if (!this.props.timelineData) {
            if (this._gantt.isDrawn()) {
                this._gantt.erase();
            }
            return;
        }

        if (this._ganttNode.current === null) {
            // I'm not 100% sure that erasing AFTER the parent node is no longer in the DOM will work -mjl
            // but I'm also not sure that this condition will ever arise.
            if (this._gantt.isDrawn()) {
                this._gantt.erase();
            }
            return;
        }

        const { timelineData, participantId } = this.props;
        const { utts, sortedParticipants } = timelineData;

        // create map of id: name
        // local user will always be first.
        logger.debug('TimelineChart: sorted participants:', sortedParticipants);
        const participantNames = sortedParticipants.map(p => p.name);
        const participantColor = getColorMap(this.props.meeting.participants, participantId);

        // create the participant map of id to name and color, filter out and
        // set the current user first, then add the other participants
        const participantMap = {};
        sortedParticipants
            .filter((p) => {
                if (p.id !== participantId) {
                    return true;
                }
                participantMap[p.id] = { name: p.name, color: PeerColors[0] };
                return false;
            })
            .reduce((pmap, p) => {
                pmap[p.id] = {
                    name: p.name,
                    color: participantColor.get(p.id),
                };
                return pmap;
            }, participantMap);

        // create extra key 'taskName' detailing name of speaker
        const utts2 = utts.map(u => ({
            ...u,
            taskName: participantMap[u.participant].name,
            color: participantMap[u.participant].color,
        }));

        this._gantt
            .setWidth(this.state.chartWidth)
            .setTaskTypes(participantNames)
            .setTasks(utts2);

        if (!this._gantt.isDrawn()) {
            this._gantt.draw(this._ganttNode.current);
        }
        else {
            this._gantt.redraw();
        }
    }
}

/* **************************************************************************** *
 * Module exports                                                               *
 * **************************************************************************** */
export {
    TimelineChart,
};
