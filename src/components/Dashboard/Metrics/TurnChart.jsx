/* ******************************************************************************
 * TurnChart.js                                                                 *
 * *************************************************************************/ /**
 *
 * @fileoverview React component chart of proportion of meeting for each participant's turns
 *
 * [More detail about the file's contents]
 *
 * Created on               October 1, 2018
 * @author                  Dan Calacci
 *
 * @copyright (c) 2018-present Riff Learning Inc.,
 *                MIT License (see https://opensource.org/licenses/MIT)
 *
 * ******************************************************************************/

// I haven't figured out for sure if we(Riff) want to use the underscore prefix to
// signify private properties/methods or not, and I've used that convention in here
// for now, so I'm disabling no-underscore-dangle. -mjl
/* eslint
    no-underscore-dangle: off,
 */

import React from 'react';
import PropTypes from 'prop-types';
import { ScaleLoader } from 'react-spinners';
import ReactChartkick, { PieChart } from 'react-chartkick';
import Chart from 'chart.js';

import {
    Colors,
    PeerColors,
    cmpObjectProp,
    getColorMap,
    logger,
} from 'libs/utils';
import { ChartTable } from 'components/A11y/ChartTable';

import { ChartCard } from './ChartCard';

// Tell the react chartkick package to use the chart package for implementing its charts
ReactChartkick.addAdapter(Chart);

/**
 *  Chart configuration properties
 */
const chartConfig = {
    cardTitle: 'Speaking Time',
    info: 'This shows a breakdown of how long each member of your meeting spoke for. More ' +
          'equal speaking time across all members is associated with higher creativity, ' +
          'more trust between group members, and better brainstorming.',
    options: {
        tooltips: {
            callbacks: {
                label(tooltipItem, data) {
                    const label = data.labels[tooltipItem.index] || '';
                    let seconds = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index] || -1;
                    const minutes = Math.trunc(seconds / 60);
                    seconds = Math.round(seconds % 60);

                    const tooltip = `${label}${label ? ': ' : ''}${minutes}m ${seconds}s`;
                    return tooltip;
                }
            },
        },
    },
};

const getTimeString =
    seconds => `${Math.trunc(seconds / 60)} minutes ${Math.round(seconds % 60)} seconds`;

const getPercentageString =
    (seconds, totalTime) => `${Math.round((seconds / totalTime) * 100)}%`;


/* ******************************************************************************
 * TurnChart                                                               */ /**
 *
 * React component to visualize the speaking time of all the participants in
 * a Riff meeting.
 *
 * TODO:
 *  - the size of the chart should be specified in a prop somehow, not sure
 *    if the size should be for the whole chart, chart card and all or just the
 *    chartDiv. -mjl 2019-05-31
 ********************************************************************************/
class TurnChart extends React.PureComponent {
    static propTypes = {

        /** meeting whose stats will be in meetingStats */
        meeting: PropTypes.shape({
            _id: PropTypes.string.isRequired,
            participants: PropTypes.instanceOf(Set).isRequired,
        }),

        /** ID of the logged in user so their data can be distinguished */
        participantId: PropTypes.string.isRequired,

        /** status of the meetingStats */
        statsStatus: PropTypes.oneOf([ 'loading', 'loaded', 'error' ]).isRequired,

        /** General stats for all participants in the meeting, null unless status is 'loaded' */
        meetingStats: PropTypes.array,
    };

    /* **************************************************************************
     * render                                                              */ /**
     *
     * Required method of a React component.
     * @see {@link https://reactjs.org/docs/react-component.html#render|React.Component.render}
     */
    render() {
        if (this.props.statsStatus !== 'loaded') {
            const loadingDisplay = (
                <div style={{ height: '100%', width: '100%' }}>
                    <ScaleLoader color={Colors.lightRoyal}/>
                </div>
            );

            return this._decorateContent(loadingDisplay);
        }

        const r = this._formatChartData();
        logger.debug('TurnChart: data for chart:', r.data);

        const chartDiv = (
            <PieChart
                donut={true}
                library={chartConfig.options}
                data={r.data}
                colors={r.colors}
                height='100%'
                width='100%'
            />
        );

        const totalTime = r.data.reduce((prev, curr) => {
            return prev + curr[1];
        }, 0);

        // Create the a11y table containing the data visualized by the chart
        const chartTable = (
            <ChartTable
                cols={[ 'Participant', 'Percent Speaking Time', 'Total Speaking Time' ]}
                rows={r.data.map(participant => [
                    participant[0],
                    getPercentageString(participant[1], totalTime),
                    getTimeString(participant[1])
                ])}
            />
        );

        // Content is a react fragment w/ the a11y table followed by the visualization
        return this._decorateContent(<>{chartTable}{chartDiv}</>);
    }

    /* **************************************************************************
     * _formatChartData                                                    */ /**
     *
     * Format the meeting stats for display by the PieChart while also
     *  - identify the current user, overriding their display name, put their data first
     *  - assign colors to all participants
     *
     * @returns {string}
     */
    _formatChartData() {
        logger.debug('TurnChart: formatting:', this.props.meetingStats);

        const selfParticipantId = this.props.participantId;
        const participantColor = getColorMap(this.props.meeting.participants, selfParticipantId);
        const data = [];

        const othersTurns = this.props.meetingStats
            .filter((partUtt) => {
                if (partUtt.participantId === selfParticipantId) {
                    data.push([ 'You', partUtt.lengthUtterances, partUtt.participantId ]);
                    return false;
                }
                return true;
            })
            .sort(cmpObjectProp('participantId'))
            .map((partUtt, i) => [ partUtt.name || `User ${i + 1}`, partUtt.lengthUtterances, partUtt.participantId ]);

        data.push(...othersTurns);
        if (othersTurns.length > PeerColors.length - 1) {
            logger.warn(`Not enough distinct colors (${PeerColors.length - 1}) for all peers (${othersTurns.length})`);
        }
        return { data, colors: data.map(d => participantColor.get(d[2])) };
    }

    /* **************************************************************************
     * _decorateContent                                                    */ /**
     *
     * Decorate the given content for display.
     *
     * @param {React.node} content
     *
     * @returns {React.element}
     */
    _decorateContent(content) {
        const meetingId = this.props.meeting ? this.props.meeting._id : 'no-selected-meeting';
        const chartCardId = `cc-${meetingId}-${chartConfig.cardTitle.replace(' ', '-')}`;

        return (
            <ChartCard
                title={chartConfig.cardTitle}
                chartInfo={chartConfig.info}
                chartCardId={chartCardId}
            >
                {content}
            </ChartCard>
        );
    }
}

/* **************************************************************************** *
 * Module exports                                                               *
 * **************************************************************************** */
export {
    TurnChart,
};
