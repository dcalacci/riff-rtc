/* ******************************************************************************
 * InfluenceChart.js                                                            *
 * *************************************************************************/ /**
 *
 * @fileoverview React component chart to display influence in a meeting
 *
 * [More detail about the file's contents]
 *
 * Created on       October 1, 2018
 * @author          Dan Calacci
 * @author          Michael Jay Lippert
 *
 * @copyright (c) 2018-present Riff Learning Inc.,
 *            MIT License (see https://opensource.org/licenses/MIT)
 *
 * ******************************************************************************/

// I haven't figured out for sure if we(Riff) want to use the underscore prefix to
// signify private properties/methods or not, and I've used that convention in here
// for now, so I'm disabling no-underscore-dangle. -mjl
/* eslint
    no-underscore-dangle: off,
 */

import React from 'react';
import PropTypes from 'prop-types';
import { ScaleLoader } from 'react-spinners';
import ReactChartkick, { ColumnChart } from 'react-chartkick';
import Chart from 'chart.js';

import {
    Colors,
    cmpObjectProp,
    getColorMap,
    logger,
    reverseCmp,
    rgbaColor,
} from 'libs/utils';
import { ChartTable } from 'components/A11y/ChartTable';

import { EmptyGraphText } from './styled';

import { ChartCard } from './ChartCard';

// Tell the react chartkick package to use the chart package for implementing its charts
ReactChartkick.addAdapter(Chart);

/**
 * There are only 2 influence types: 'mine' and 'theirs'.
 * This config object contains settings objects for the 2 types.
 * These settings encapsulate the differences between the 2 types
 * of influence chart.
 *
 *  title    - The title of the chart
 *  info     - The text displayed by the info button of the chart card containing the chart
 *  empty    - The text displayed in place of a chart when there is no data to create a chart
 *  filter   - returns a function which takes an influenceData object and compares one of
 *             its properties to a given uid for equality, the property compared depends on
 *             the influence type.
 *  getLabel - returns the value of the appropriate property of the given influenceData object
 *             depending on the influence type.
 */
const influenceTypeConfig = {
    mine: {
        title: 'Who You Influenced',
        info: 'This graph shows how many times each person spoke first after you finished speaking. ' +
              'Frequent first-responses indicate that a person is engaged by what you have to say.',
        empty: "It doesn't look like anyone responded to you quickly in this meeting.",
        filter(uid) { return d => d.target === uid; },
        getLabel(d) { return d.sourceName; },
        getPartId(d) { return d.source; },
    },

    theirs: {
        title: 'Who Influenced You',
        info: 'This graph shows how many times you spoke first after another person finished speaking. ' +
              'Frequent first-responses indicate that you are engaged by what another person is saying.',
        empty: "It doesn't look like you responded quickly to anyone in this meeting.",
        filter(uid) { return d => d.source === uid; },
        getLabel(d) { return d.targetName; },
        getPartId(d) { return d.target; },
    },
};

/* ******************************************************************************
 * InfluenceChart                                                          */ /**
 *
 * React component to visualize the influence of others on a participant or
 * of the participant on other participants in a Riff meeting.
 *
 ********************************************************************************/
class InfluenceChart extends React.PureComponent {
    static propTypes = {

        /** meeting whose influence data will be in influenceData */
        meeting: PropTypes.shape({
            _id: PropTypes.string.isRequired,
            participants: PropTypes.instanceOf(Set).isRequired,
        }),

        /** ID of the logged in user so their data can be distinguished */
        participantId: PropTypes.string.isRequired,

        /** status of the influenceData */
        influenceStatus: PropTypes.oneOf([ 'loading', 'loaded', 'error' ]).isRequired,

        /** data about who influenced whom in the meeting */
        influenceData: PropTypes.arrayOf(PropTypes.shape({
            id: PropTypes.string,
            size: PropTypes.number,
            source: PropTypes.string,
            sourceName: PropTypes.string,
            target: PropTypes.string,
            targetName: PropTypes.string,
        })),

        /** Which influence to show, the user on others, or others on the user */
        influenceType: PropTypes.oneOf([ 'mine', 'theirs' ]).isRequired,
    };

    /* **************************************************************************
     * render                                                              */ /**
     *
     * Required method of a React component.
     * @see {@link https://reactjs.org/docs/react-component.html#render|React.Component.render}
     */
    render() {
        // get the config for the influence type (mine or theirs)
        const config = influenceTypeConfig[this.props.influenceType];
        const meetingId = this.props.meeting ? this.props.meeting._id : 'no-selected-meeting';
        const chartCardId = `cc-${meetingId}-${config.title.replace(' ', '-')}`;

        if (this.props.influenceStatus !== 'loaded') {
            const loadingDisplay = (
                <div>
                    <ScaleLoader color={Colors.lightRoyal}/>
                </div>
            );

            // TODO:
            //  - the size of the chart should be specified in a prop somehow, not sure
            //    if the size should be for the whole chart, chart card and all or just the
            //    chartDiv. -mjl 2019-05-31
            return (
                <ChartCard
                    title={config.title}
                    chartInfo={config.info}
                    chartCardId={chartCardId}
                >
                    {loadingDisplay}
                </ChartCard>
            );
        }

        const labelsAndData = this._getLabelsAndData();

        // the X axis is 'who', but we don't want it labeled so use a space (' ') as the x axis key
        const colData = labelsAndData.data.map((v, i) => ({ name: labelsAndData.labels[i], data: { ' ': v } }));

        // DevNote: If you don't set the opacity, the ColumnChart will make the bar fill transparent.
        // Use solid bar colors to match the TurnChart
        const colColors = labelsAndData.colors.map(c => rgbaColor(c, 1.0));
        logger.debug('InfluenceChart: labels and data,',
                     { influenceType: this.props.influenceType, labelsAndData, colColors });

        // Display either the chart, or some explanatory text if there isn't any data for the chart
        let chartDiv;
        if (labelsAndData.labels.length === 0) {
            chartDiv = <EmptyGraphText>{config.empty}</EmptyGraphText>;
        }
        else {
            chartDiv = (
                <ColumnChart
                    data={colData}
                    colors={colColors}
                    height='100%'
                    width='100%'
                />
            );
        }

        // The a11y table representation of the data in the chart
        const chartTable = (
            <ChartTable
                cols={[ 'Participant', 'Responses' ]}
                rows={labelsAndData.labels.map((label, i) => [
                    label,
                    labelsAndData.data[i]
                ])}
            />
        );

        return (
            <ChartCard
                title={config.title}
                chartInfo={config.info}
                chartCardId={chartCardId}
            >
                {chartTable}
                {chartDiv}
            </ChartCard>
        );
    }

    /* **************************************************************************
     * _getLabelsAndData                                                   */ /**
     *
     * Get the raw labels and data for this influence chart.
     *
     * Appropriate for the type of chart (mine or theirs).
     *
     * @returns {{ labels: Array, data: Array }}
     */
    _getLabelsAndData() {
        const config = influenceTypeConfig[this.props.influenceType];
        const typeFilterFn = config.filter(this.props.participantId);
        const participantColor = getColorMap(this.props.meeting.participants, this.props.participantId);

        // only keep influence data for this type of influence chart
        const influenceTypeData = this.props.influenceData
            .filter(typeFilterFn)
            .sort(reverseCmp(cmpObjectProp('size')));

        logger.debug('InfluenceChart.getLabelsAndData: influenceData for ' +
                     `${this.props.participantId}(${this.props.influenceType})`,
                     { influenceTypeData, meeting: this.props.meeting });

        return {
            labels: influenceTypeData.map(config.getLabel),
            data: influenceTypeData.map(d => d.size),
            colors: influenceTypeData.map(d => participantColor.get(config.getPartId(d))),
        };
    }
}

/* **************************************************************************** *
 * Module exports                                                               *
 * **************************************************************************** */
export {
    InfluenceChart,
};
