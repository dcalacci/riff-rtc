/* ******************************************************************************
 * gantt.js                                                                     *
 * *************************************************************************/ /**
 *
 * @fileoverview Draw a gantt chart
 *
 * [More detail about the file's contents]
 *
 * Created on       October 5, 2018
 * @author          Dan Calacci
 * @author          Michael Jay Lippert
 *
 * @copyright (c) 2018-present Riff Learning, Inc.,
 *            MIT License (see https://opensource.org/licenses/MIT)
 *
 * ******************************************************************************/

// I haven't figured out for sure if we(Riff) want to use the underscore prefix to
// signify private properties/methods or not, and I've used that convention in here
// for now, so I'm disabling no-underscore-dangle. -mjl
/* eslint
    no-underscore-dangle: off,
 */

import { d3 } from 'libs/d3';
import { logger } from 'libs/utils';


const MS_PER_SECOND = 1000;
const MS_PER_MINUTE = 60 * MS_PER_SECOND;

/**
 *  TimeDomainMode values
 *  @enum {string}
 */
const TimeDomainMode = {
    FIT: 'fit',
    FIXED: 'fixed',
};

/* ******************************************************************************
 * Gantt                                                                   */ /**
 *
 * The Gantt class uses d3 to draw a gantt chart of tasks organized by task type.
 *
 * After setting all required properties use the draw, redraw and erase methods
 * to render, update and remove the gantt chart.
 *
 ********************************************************************************/
class Gantt {

    /* **************************************************************************
     * constructor                                                         */ /**
     *
     * Gantt class constructor.
     */
    constructor() {
        /** backing store for getter/setter properties, and other private properties
         *  sets initial values for those properties.
         */
        this._privateProp = {
            parentSelector: null,
            svg: null,
            width: document.body.clientWidth / 2,
            height: 280,    // alternatively '100%'
            margin: { top: 20, right: 50, bottom: 40, left: 80 },
            yLabelWidth: 50,
            timeDomainStart: d3.timeDay.offset(new Date(), -3),
            timeDomainEnd: d3.timeHour.offset(new Date(), +3),
            tickFormat: '%H:%M',
            timeDomainMode: TimeDomainMode.FIT,
            title: 'Gantt Chart',
            tasks: [],
            taskTypes: [],
            taskStatus: [],
        };

        // Initialize this.{x,y,xAxis,yAxis} from current values of width, height, margin, timeDomain...
        this._initAxis();

        this._keyFunction = (d) => {
            return d.startDate + d.taskName + d.endDate;
        };

        this._rectTransform = (d) => {
            return `translate(${this.x(d.startDate)}, ${this.y(d.taskName)})`;
        };
    }

    /*
     * Gantt property getters/setters
     */
    get parentSelector() {
        return this._privateProp.parentSelector;
    }

    get width() {
        return this._privateProp.width;
    }
    set width(w) {
        let newWidth = w;
        if (!newWidth) {
            newWidth = document.body.clientWidth / 2;
            logger.info(`Gantt set width: width set to default (1/2 client width): ${newWidth}`);
        }

        this._privateProp.width = Number(newWidth);
        this._initAxis();
    }

    get height() {
        return this._privateProp.height;
    }
    set height(h) {
        this._privateProp.height = Number(h);
    }

    get margin() {
        return this._privateProp.margin;
    }
    set margin(m) {
        this._privateProp.margin = m;
    }

    get yLabelWidth() {
        return this._privateProp.yLabelWidth;
    }

    get timeDomain() {
        return [ this._privateProp.timeDomainStart, this._privateProp.timeDomainEnd ];
    }
    set timeDomain(td) {
        this._privateProp.timeDomainStart = Number(td[0]);
        this._privateProp.timeDomainEnd = Number(td[1]);
    }

    get tickFormat() {
        return this._privateProp.tickFormat;
    }
    set tickFormat(fmt) {
        this._privateProp.tickFormat = fmt;
    }

    /**
     * @param {TimeDomainMode} tdMode
     *      - 'fit' - the domain fits the data
     *      - 'fixed' - fixed domain
     * @returns {TimeDomainMode} current time domain mode
     */
    get timeDomainMode() {
        return this._privateProp.timeDomainMode;
    }
    set timeDomainMode(tdMode) {
        this._privateProp.timeDomainMode = tdMode;
    }

    get title() {
        return this._privateProp.title;
    }
    set title(title) {
        this._privateProp.title = title;
    }

    get tasks() {
        return this._privateProp.tasks;
    }
    set tasks(value) {
        logger.debug('Gantt: setting tasks to:', value);
        this._privateProp.tasks = value;
        this.initTimeDomain();
        this._initAxis();

        // TODO: if drawn, we should redraw
    }

    get taskTypes() {
        return this._privateProp.taskTypes;
    }
    set taskTypes(value) {
        logger.debug('Gantt: setting task types to:', value);
        this._privateProp.taskTypes = value;
    }

    get taskStatus() {
        return this._privateProp.taskStatus;
    }
    set taskStatus(status) {
        this._privateProp.taskStatus = status;
    }

    /*
     * chaining property setting functions
     */
    setWidth(w) {
        this.width = w;
        return this;
    }

    setTitle(title) {
        this.title = title;
        return this;
    }

    setTasks(tasks) {
        this.tasks = tasks;
        return this;
    }

    setTaskTypes(types) {
        this.taskTypes = types;
        return this;
    }

    setTaskStatus(status) {
        this.taskStatus = status;
        return this;
    }

    /* **************************************************************************
     * draw                                                                */ /**
     *
     * Draw the gantt chart (as defined by the currently set properties) as
     * child elements of the element found using the given parentSelector.
     *
     * @param {string} parentSelector
     */
    draw(parentSelector) {
        this._privateProp.parentSelector = parentSelector;
        logger.debug('Gantt: drawing tasks:', this.tasks);

        const ganttParent = d3.select(this.parentSelector);
        logger.debug('Gantt: parent element', ganttParent);

        const svg = ganttParent
            .append('svg')
            .attr('class', 'chart')
            .attr('width', this.width)
            .attr('height', this.height)
            .style('overflow', 'visible');

        // store the d3 svg selection object to make redraw and erase easier
        this._privateProp.svg = svg;

        const chartGroup = svg.append('g')
            .attr('class', 'gantt-chart')
            .attr('role', 'figure')
            .attr('aria-labelledby', 'gantt-label')
            .attr('width', this.width - this.margin.left - this.margin.right)
            .attr('height', this.height - this.margin.top - this.margin.bottom)
            .attr('transform', `translate(${this.margin.left}, ${this.margin.top})`);

        chartGroup.append('title')
            .attr('id', 'gantt-label')
            .text(this.title);

        chartGroup.selectAll('.chart')
            .data(this.tasks, this._keyFunction)
            .enter()
            .append('g')
            .attr('role', 'presentation')
            .attr('aria-label', (d) => {
                const startTime = d3.timeFormat(this.tickFormat)(d.startDate);
                const utteranceLength = Math.round((d.endDate - d.startDate) / 1000);
                return `${startTime}, ${d.taskName} spoke for ${utteranceLength} seconds`;
            })
            .append('rect')
            .attr('rx', 5)
            .attr('ry', 5)
            .attr('class', (d) => { return this.taskStatus[d.status] || 'bar'; })
            .attr('y', 0)
            .attr('transform', this._rectTransform)
            .attr('height', this.y.bandwidth())
            .attr('width', (d) => {
                return Math.max(1, this.x(d.endDate) - this.x(d.startDate));
            })
            .attr('fill', (d) => {
                return d.color;
            });

        chartGroup.append('g')
            .classed('x axis', true)
            .classed('axisGray', true)
            .attr('aria-hidden', 'true')
            .attr('transform', `translate(0, ${this.height - this.margin.top - this.margin.bottom})`)
            .call(this.xAxis);

        const yAxis = chartGroup.append('g')
            .classed('y axis', true)
            .classed('axisGray', true)
            .attr('aria-hidden', 'true')
            .call(this.yAxis);

        yAxis.selectAll('.tick text')
            .call(wrapYLabels, this.yLabelWidth);

        return this;
    }

    /* **************************************************************************
     * redraw                                                              */ /**
     *
     * Update this drawn Gantt chart using the current property values, which
     * may have changed since the chart was last drawn.
     */
    redraw() {
        // Can't redraw something that hasn't been drawn
        if (!this.isDrawn()) {
            return;
        }

        // Poor man's redraw for now (erase and draw again)
        const parentSelector = this.parentSelector;
        this.erase();
        this.draw(parentSelector);
    }

    /* **************************************************************************
     * erase                                                               */ /**
     *
     * Remove this gantt chart from the DOM, and forget the selector used to
     * find the parent to draw in.
     */
    erase() {
        if (!this.isDrawn()) {
            return;
        }

        this._privateProp.svg.remove();
        this._privateProp.parentSelector = null;
        this._privateProp.svg = null;
    }

    /* **************************************************************************
     * isDrawn                                                             */ /**
     *
     * Determine if this Gantt chart is currently drawn somewhere.
     *
     * @returns {bool} true if this Gantt chart is drawn somewhere.
     */
    isDrawn() {
        return this._privateProp.svg !== null;
    }

    /**
     * set the time domain start and end values based on the current tasks
     * and time domain mode.
     */
    initTimeDomain() {
        logger.debug('Gantt: initializing time domain with tasks:', this.tasks);
        let timeDomainStart;
        let timeDomainEnd;
        if (this.timeDomainMode === TimeDomainMode.FIT) {
            if (this.tasks === undefined || this.tasks.length < 1) {
                timeDomainStart = [ d3.timeDay.offset(new Date(), -3), d3.timeHour.offset(new Date(), +3) ];
                timeDomainEnd = [ d3.timeDay.offset(new Date(), -3), d3.timeHour.offset(new Date(), +3) ];
            }
            else {
                // TODO: need the earliest startDate and the latest endDate, depending on how the tasks
                // overlap that may not be the start of the first and the end of the last. Check!
                timeDomainStart = this.tasks[0].startDate;
                timeDomainEnd = this.tasks[this.tasks.length - 1].endDate;
            }
            this.timeDomain = [ timeDomainStart, timeDomainEnd ];
            logger.debug('Gantt: time domain:', { timeDomainStart, timeDomainEnd });
        }
    }

    /* **************************************************************************
     * _initAxis                                                            */ /**
     *
     * set the x, y, xAxis and yAxis d3 functions based on the current values
     * of time domain, width, height, margins...
     */
    _initAxis() {
        this.x = d3
            .scaleTime()
            .domain(this.timeDomain)
            .range([ 0, this.width - this.margin.left - this.margin.right ])
            .clamp(true);

        this.y = d3
            .scaleBand()
            .domain(this.taskTypes)
            .range([ 0, this.height - this.margin.top - this.margin.bottom ], 0.1);

        this.xAxis = d3
            .axisBottom(this.x)
            .tickFormat(d3.timeFormat(this.tickFormat))
            .tickSize(8)
            .tickPadding(8)
            .ticks(this._getShowXTick());

        this.yAxis = d3
            .axisLeft(this.y)
            .tickSize(0);
    }

    /* **************************************************************************
     * _getShowXTick                                                       */ /**
     *
     * Get a d3 time interval which specifies what tick marks to display on the
     * X Axis.
     *
     * @returns {d3.interval}
     */
    _getShowXTick() {
        const meetingMinutes = (this.timeDomain[1] - this.timeDomain[0]) / MS_PER_MINUTE;
        let tickMinutes;
        if (meetingMinutes < 8) {
            tickMinutes = 1;
        }
        else if (meetingMinutes < 15) {
            tickMinutes = 2;
        }
        else if (meetingMinutes < 40) {
            tickMinutes = 5;
        }
        else if (meetingMinutes < 80) {
            tickMinutes = 10;
        }
        else {
            tickMinutes = 15;
        }

        logger.debug(`Gantt: _getShowTick: meeting minutes: ${meetingMinutes} tick minutes: ${tickMinutes}`);

        const showXTick = d3.timeMinute.filter((dt) => {
            const show = dt.getMinutes() % tickMinutes === 0;
            // logger.debug(`Gantt: xaxis tick filter show: ${show} ms: ${dt.getTime()}`, dt);
            return show;
        });

        return showXTick;
    }
}

/**
 * Wrap long d3 axis labels
 * based on code from: https://bl.ocks.org/mbostock/7555321
 *
 * @param {d3.Selection} labels - Selection of text elements for labels of an axis
 * @param {number} width - The max width of a label before wrapping words to the next line
 */
function wrapYLabels(yLabels, width) {
    const wrapYLabel = function () {
        const textEl = d3.select(this);
        const lineHeight = 1.1; // ems
        const words = textEl.text().split(/\s+/).reverse();
        const x = textEl.attr('x');
        let line = [];
        let firstDy = parseFloat(textEl.attr('dy'));
        firstDy = isNaN(firstDy) ? 0 : firstDy;
        const tspanFirstEl = textEl.text(null).append('tspan')
            .attr('x', x);
        let tspanEl = tspanFirstEl;

        // See if the next word from the label fits on the current line,
        // otherwise put it on the next line and make that the current line
        let numLines = 1;
        let word;
        while (word = words.pop()) {    // eslint-disable-line no-cond-assign
            line.push(word);
            tspanEl.text(line.join(' '));
            if (tspanEl.node().getComputedTextLength() > width) {
                line.pop();
                tspanEl.text(line.join(' '));
                line = [ word ];
                tspanEl = textEl.append('tspan')
                    .attr('x', x)
                    .attr('dy', `${lineHeight}em`)
                    .text(word);
                ++numLines;
            }
        }

        // centering the label lines (vertically)
        // Without this a single line would have its baseline at the center, so
        // this will move that single line up 1/2 of the height of any extra lines
        // in order to center all of the lines.
        firstDy -= (numLines - 1) * lineHeight / 2;
        tspanFirstEl
            .attr('dy', `${firstDy.toFixed(2)}em`);
    };

    yLabels.each(wrapYLabel);
}

/* **************************************************************************** *
 * Module exports                                                               *
 * **************************************************************************** */
export {
    Gantt,
};
