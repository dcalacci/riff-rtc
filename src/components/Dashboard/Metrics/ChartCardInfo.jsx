/* ******************************************************************************
 * ChartCardInfo.jsx                                                            *
 * *************************************************************************/ /**
 *
 * @fileoverview React component for the chart info on a chart card
 *
 * [More detail about the file's contents]
 *
 * Created on       June 5, 2019
 * @author          Michael Jay Lippert
 *
 * @copyright (c) 2019-present Riff Learning Inc.,
 *            MIT License (see https://opensource.org/licenses/MIT)
 *
 * ******************************************************************************/

// I haven't figured out for sure if we(Riff) want to use the underscore prefix to
// signify private properties/methods or not, and I've used that convention in here
// for now, so I'm disabling no-underscore-dangle. -mjl
/* eslint
    no-underscore-dangle: off,
 */

import React from 'react';
import PropTypes from 'prop-types';
import CloseIcon from '@material-ui/icons/Close';

import { StyledInfo, StyledInfoSmall } from './styled';


/* ******************************************************************************
 * ChartCardInfo                                                           */ /**
 *
 * React component used by ChartCard to present the chart description.
 *
 ********************************************************************************/
class ChartCardInfo extends React.Component {
    static propTypes = {

        /** Value to use for the id of the top level element of this component */
        id: PropTypes.string.isRequired,

        /** Description of the presented chart */
        chartInfo: PropTypes.string.isRequired,

        /** function to close (actually remove) this info component */
        close: PropTypes.func.isRequired,

        /** flag for an especially long description that requires smaller text. */
        longDescription: PropTypes.bool,
    };

    /* **************************************************************************
     * constructor                                                         */ /**
     */
    constructor(props) {
        super(props);

        /** The top level node of this ChartCardInfo */
        this._infoNode = React.createRef();
    }

    /* **************************************************************************
     * componentDidMount                                                   */ /**
     *
     * Lifecycle method of a React component.
     * This is invoked immediately after a component is mounted (inserted into the
     * tree). Initialization that requires DOM nodes should go here.
     *
     * Set the focus to this component when it is mounted (for a11y purposes).
     *
     * @see {@link https://reactjs.org/docs/react-component.html#componentdidmount|React.Component.componentDidMount}
     */
    componentDidMount() {
        this._infoNode.current.focus();
    }

    /* **************************************************************************
     * render                                                              */ /**
     *
     * Required method of a React component.
     * @see {@link https://reactjs.org/docs/react-component.html#render|React.Component.render}
     */
    render() {
        let chartInfo = <StyledInfo>{this.props.chartInfo}</StyledInfo>;

        if (this.props.longDescription) {
            chartInfo = <StyledInfoSmall>{this.props.chartInfo}</StyledInfoSmall>;
        }

        return (
            <div
                id={this.props.id}
                className='chart-info-div'
                ref={this._infoNode}
                tabIndex='-1'
            >
                <span
                    className='has-text-right chart-card-icon'
                    style={{ 'float': 'right' }}
                >
                    <button onClick={this.props.close}>
                        <CloseIcon/>
                    </button>
                </span>
                {chartInfo}
            </div>
        );
    }
}

/* **************************************************************************** *
 * Module exports                                                               *
 * **************************************************************************** */
export {
    ChartCardInfo,
};
