/* ******************************************************************************
 * index.js                                                                     *
 * *************************************************************************/ /**
 *
 * @fileoverview Hook up the Metric charts to redux state and actions
 *
 * [More detail about the file's contents]
 *
 * Created on       May 30, 2019
 * @author          Michael Jay Lippert
 *
 * @copyright (c) 2019-present Riff Learning, Inc.,
 *            MIT License (see https://opensource.org/licenses/MIT)
 *
 * ******************************************************************************/

import { connect } from 'react-redux';

import { MeetingInfo } from './MeetingInfo';
import { TurnChart } from './TurnChart';
import { TimelineChart } from './TimelineChart';
import { InfluenceChart } from './InfluenceChart';

const infoMapProps = {
    mapStateToProps: state => ({
        meeting:                state.dashboard.selectedMeeting || null,
        isLtiUser:              state.lti.loggedIn,
    }),
};

const turnMapProps = {
    mapStateToProps: state => ({
        participantId:          state.auth.user.uid,
        meeting:                state.dashboard.selectedMeeting || null,
        statsStatus:            state.dashboard.statsStatus,
        meetingStats:           state.dashboard.meetingStats,
    }),
};

const timelineMapProps = {
    mapStateToProps: state => ({
        participantId:          state.auth.user.uid,
        meeting:                state.dashboard.selectedMeeting || null,
        timelineStatus:         state.dashboard.timelineStatus,
        timelineData:           state.dashboard.timelineData,
    }),
};

const influenceMapProps = {
    mapStateToProps: state => ({
        participantId:          state.auth.user.uid,
        meeting:                state.dashboard.selectedMeeting || null,
        influenceStatus:        state.dashboard.influenceStatus,
        influenceData:          state.dashboard.influenceData,
    }),
};

const ConnectedMeetingInfo = connect(infoMapProps.mapStateToProps)(MeetingInfo);
const ConnectedTurnChart = connect(turnMapProps.mapStateToProps)(TurnChart);
const ConnectedTimelineChart = connect(timelineMapProps.mapStateToProps)(TimelineChart);
const ConnectedInfluenceChart = connect(influenceMapProps.mapStateToProps)(InfluenceChart);

/* **************************************************************************** *
 * Module exports                                                               *
 * **************************************************************************** */
export {
    ConnectedMeetingInfo as MeetingInfo,
    ConnectedTurnChart as TurnChart,
    ConnectedTimelineChart as TimelineChart,
    ConnectedInfluenceChart as InfluenceChart,
};
