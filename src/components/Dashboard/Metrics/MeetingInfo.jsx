/* ******************************************************************************
 * MeetingInfo.jsx                                                              *
 * *************************************************************************/ /**
 *
 * @fileoverview React component that displays information about a meeting
 *
 * [More detail about the file's contents]
 *
 * Created on       June 20, 2019
 * @author          Michael Jay Lippert
 *
 * @copyright (c) 2019-present Riff Learning Inc.,
 *                MIT License (see https://opensource.org/licenses/MIT)
 *
 * ******************************************************************************/

// I haven't figured out for sure if we(Riff) want to use the underscore prefix to
// signify private properties/methods or not, and I've used that convention in here
// for now, so I'm disabling no-underscore-dangle. -mjl
/* eslint
    no-underscore-dangle: off,
 */

import React from 'react';
import PropTypes from 'prop-types';
import { ScaleLoader } from 'react-spinners';

import { Colors, logger } from 'libs/utils';

const MILLISEC_PER_MINUTE = 1000 * 60;

/* ******************************************************************************
 * MeetingInfo                                                             */ /**
 *
 * React component that displays information about a meeting
 *
 ********************************************************************************/
class MeetingInfo extends React.PureComponent {
    static propTypes = {

        /** meeting whose stats will be in meetingStats */
        meeting: PropTypes.shape({
            _id: PropTypes.string.isRequired,
            room: PropTypes.string,
            startTime: PropTypes.string.isRequired,
            endTime: PropTypes.string,
            participants: PropTypes.instanceOf(Set).isRequired,
        }),

        /** did the user authenticate via LTI launch from an LMS or just a regular log in (or anonymous) */
        isLtiUser: PropTypes.bool.isRequired,
    };

    /* **************************************************************************
     * render                                                              */ /**
     *
     * Required method of a React component.
     * @see {@link https://reactjs.org/docs/react-component.html#render|React.Component.render}
     */
    render() {
        if (!this.props.meeting) {
            // devnote: the height is being set just by eyeballing how it looks
            const loadingDisplay = (
                <div className='level' style={{ height: '96pt', width: '25vw' }}>
                    <ScaleLoader color={Colors.lightRoyal}/>
                </div>
            );

            return loadingDisplay;
        }

        const roomName = this.props.isLtiUser ? this._getFriendlyLtiRoomName() : this.props.meeting.room;

        const meetingInfo = (
            <div>
                <h1 className='is-size-3 is-primary'>{'Room: '}{roomName}</h1>
                <h2 className='is-size-4 is-primary'>{`${this.props.meeting.participants.size} Attendees`}</h2>
                <h2 className='is-size-4 is-primary'>{this._getFormattedMeetingDuration()}</h2>
            </div>
        );

        return meetingInfo;
    }

    /* **************************************************************************
     * _getFriendlyLtiRoomName                                             */ /**
     *
     * The real room name of LTI user meetings in the meeting object has the
     * course id as a suffix which is ugly and not what the user saw when joining
     * the room so the friendly name has that course id suffix removed.
     *
     * @returns {string} the user friendly name of an LTI room
     */
    _getFriendlyLtiRoomName() {
        return this.props.meeting.room.split('_').slice(0, -1).join('_');
    }

    /* **************************************************************************
     * _getFormattedMeetingDuration                                        */ /**
     *
     * [Description of _getFormattedMeetingDuration]
     *
     * @returns {string} the formatted length of the meeting
     */
    _getFormattedMeetingDuration() {
        if (this.props.meeting === null) {
            return '';
        }

        // 'in progress' meetings have a null endTime, we'll calculate the duration of those
        // meetings by giving them a fake end time of now
        logger.debug('MeetingInfo: _getFormattedMeetingDuration: meeting', this.props.meeting);

        const meeting = this.props.meeting;
        const startDate = new Date(meeting.startTime);
        const endDate = meeting.endTime ? new Date(meeting.endTime) : new Date();
        const durationMins = Math.trunc((endDate.getTime() - startDate.getTime()) / MILLISEC_PER_MINUTE);

        return `${durationMins} minutes`;
    }
}

/* **************************************************************************** *
 * Module exports                                                               *
 * **************************************************************************** */
export {
    MeetingInfo,
};
