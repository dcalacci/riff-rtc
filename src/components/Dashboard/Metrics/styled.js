/* ******************************************************************************
 * styled.js                                                                    *
 * *************************************************************************/ /**
 *
 * @fileoverview React components to apply styling to metrics components
 *
 * [More detail about the file's contents]
 *
 * Created on       June 5, 2019
 * @author          Michael Jay Lippert
 *
 * @copyright (c) 2019-present Riff Learning Inc.,
 *            MIT License (see https://opensource.org/licenses/MIT)
 *
 * ******************************************************************************/

import styled from 'styled-components';

import { Colors, rgbaColor } from 'libs/utils';

const StyledCardTitle = styled.div.attrs({
    className: 'title is-5 has-text-left'
})`
    margin-left: 1rem;
    margin-right: 1rem;
    color: ${Colors.lightRoyal};
    display: flex;
    justify-content: space-between;

    button {
        background: none;
        border: none;
        color: ${rgbaColor(Colors.black, 0.54)};
        cursor: pointer;
    }
`;

const StyledChart = styled.div.attrs({
    className: 'card-image has-text-centered is-centered'
})`
    padding-bottom: 1rem;
    width: 100%;
    height: 100%;
`;

const StyledCard = styled.div.attrs({
    className: 'card has-text-centered is-centered'
})`
    width: 100%;
    height: 100%;
    margin-left: auto;
    margin-right: auto;
    margin-bottom: 2rem;
    background-color: ${Colors.purpleWhite};
    border: 2px solid ${Colors.lightRoyal};
    border-radius: 5px;
    padding-top: 0.75rem;
    box-shadow: none;
    box-sizing: border-box;
    -moz-box-sizing: border-box;
    -webkit-box-sizing: border-box;
`;

const StyledInfo = styled.p.attrs({
    className: 'has-text-weight-bold is-size-6'
})`
    padding: 2rem;
    color: ${Colors.white};
`;

const StyledInfoSmall = styled.p.attrs({
    className: 'is-size-7'
})`
    padding: 2rem 0.5rem 0.5rem 0.5rem;
    color: ${Colors.white};
`;

// DevNote: 1px padding on the bottom seems to cause something that looks like
// much more than 1px. I suspect some iteraction with an ancestor element that I
// haven't quite figured out. I've made the padding smaller, but leaving at 1px
// for now.
const StyledWave = styled.div`
    background: ${Colors.brightPlum};
    margin-top: -10px;
    padding-bottom: 1px;
`;

/** Style for text that replaces a graph when the graph has no data to display */
const EmptyGraphText = styled.p`
  display: flex;
  align-items: center;
  height: 100%;
  padding: 0 2rem;

  @media (max-width: 1070px) {
    padding: 0 .5rem;
  }
`;

/* **************************************************************************** *
 * Module exports                                                               *
 * **************************************************************************** */
export {
    StyledCard,
    StyledCardTitle,
    StyledChart,
    StyledInfo,
    StyledInfoSmall,
    StyledWave,
    EmptyGraphText,
};
