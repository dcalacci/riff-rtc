/* ******************************************************************************
 * WavyBackground.jsx                                                           *
 * *************************************************************************/ /**
 *
 * @fileoverview React component to put purple background w/ a wavy top around a child
 *
 * [More detail about the file's contents]
 *
 * Created on       June 3, 2019
 * @author          Michael Jay Lippert
 *
 * @copyright (c) 2018-present Riff Learning Inc.,
 *            MIT License (see https://opensource.org/licenses/MIT)
 *
 * ******************************************************************************/

import React from 'react';
import PropTypes from 'prop-types';

import { Colors } from 'libs/utils';

import { StyledWave } from './styled';

/* ******************************************************************************
 * WavyBackground                                                          */ /**
 *
 * React component to create a purple background w/ a wavy top.
 *
 ********************************************************************************/
class WavyBackground extends React.PureComponent {
    static propTypes = {

        /** foreground elements */
        children: PropTypes.node,
    };

    /* **************************************************************************
     * render                                                              */ /**
     *
     * Required method of a React component.
     * @see {@link https://reactjs.org/docs/react-component.html#render|React.Component.render}
     */
    render() {
        const wave = (
            <svg id='Layer_1' x='0px' y='0px' viewBox='0 0 1440 126'>
                <path
                    style={{ fill: Colors.brightPlum }}
                    d='M685.6,38.8C418.7-11.1,170.2,9.9,0,30v96h1440V30C1252.7,52.2,1010,99.4,685.6,38.8z'
                />
            </svg>
        );

        return (
            <React.Fragment>
                {wave}
                <StyledWave>
                    {this.props.children}
                </StyledWave>
            </React.Fragment>
        );
    }
}

/* **************************************************************************** *
 * Module exports                                                               *
 * **************************************************************************** */
export {
    WavyBackground,
};
