/* ******************************************************************************
 * styled.js                                                                    *
 * *************************************************************************/ /**
 *
 * @fileoverview React components to apply styling to dashboard components
 *
 * [More detail about the file's contents]
 *
 * Created on       May 29, 2019
 * @author          Michael Jay Lippert
 *
 * @copyright (c) 2019-present Riff Learning Inc.,
 *            MIT License (see https://opensource.org/licenses/MIT)
 *
 * ******************************************************************************/

import styled from 'styled-components';

import { Colors, rgbaColor } from 'libs/utils';

const MeetingTabs = styled.aside.attrs({
    className: 'timeline'
})`
    padding-left: 2.5rem;
    overflow-y: scroll;
    max-height: 100%;
    &::-webkit-scrollbar {
        width: 6px;
    }
    &::-webkit-scrollbar-thumb {
        background-color: ${Colors.mauve};
    }
    &::-webkit-scrollbar-track {
        background-color: ${rgbaColor(Colors.mauve, 0.3)};
    }
    /* If we really want the top and bottom scroll buttons on chrome see
       https://stackoverflow.com/questions/47576815/how-to-add-arrows-with-webkit-scrollbar-button?rq=1 */
`;

/* **************************************************************************** *
 * Module exports                                                               *
 * **************************************************************************** */
export {
    MeetingTabs,
};
