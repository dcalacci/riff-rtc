/* ******************************************************************************
 * MeetingListElement.jsx                                                       *
 * *************************************************************************/ /**
 *
 * @fileoverview React Riff Meeting List component
 *
 * [More detail about the file's contents]
 *
 * Created on       May 29, 2019
 * @author          Michael Jay Lippert
 *
 * @copyright (c) 2019-present Riff Learning Inc.,
 *            MIT License (see https://opensource.org/licenses/MIT)
 *
 * ******************************************************************************/

// I haven't figured out for sure if we(Riff) want to use the underscore prefix to
// signify private properties/methods or not, and I've used that convention in here
// for now, so I'm disabling no-underscore-dangle. -mjl
/* eslint
    no-underscore-dangle: off,
 */

import React from 'react';
import PropTypes from 'prop-types';
import VoiceChatIcon from '@material-ui/icons/VoiceChat';

import { Colors } from 'libs/utils';

/* ******************************************************************************
 * MeetingListElement                                                      */ /**
 *
 * React component to present a meeting in a list of meetings.
 *
 ********************************************************************************/
class MeetingListElement extends React.Component {
    static propTypes = {

        /** The meeting object to be presented */
        meeting: PropTypes.object.isRequired,

        /** whether the meeting is the currently selected meeting */
        selected: PropTypes.bool.isRequired,

        /** function to invoke when this meeting is clicked on (will select the meeting) */
        handleClick: PropTypes.func.isRequired,
    };

    /* **************************************************************************
     * render                                                              */ /**
     *
     * Required method of a React component.
     * @see {@link https://reactjs.org/docs/react-component.html#render|React.Component.render}
     */
    render() {
        const m = MeetingListElement._formatMeetingTime(new Date(this.props.meeting.startTime));
        const itemClasses = [ 'timeline-item is-clickable' ];
        let markerColor = Colors.lightAnchor; // not selected
        if (this.props.selected) {
            itemClasses.push('selected');
            // The marker color should be determined by the 'selected' class on
            // the parent 'timeline-item'
            // but the 'selected' class doesn't actually seem to be used by any
            // styling at this time -mjl 2019-05-30
            markerColor = Colors.africanLollipop;
        }

        return (
            <div
                className={itemClasses.join(' ')}
                onClick={event => this.props.handleClick(event, this.props.meeting)}
                tabIndex='0'
                role='link'
            >
                <div className='timeline-marker is-image is-32x32'>
                    <VoiceChatIcon
                        size={20}
                        style={{
                            color: markerColor,
                            marginLeft: '0.25rem',
                            marginTop: '0.25rem',
                            paddingLeft: '0.05rem',
                            paddingTop: '0.1rem',
                            fontSize: '1.3rem',
                        }}
                        aria-label='Voice Chat'
                        aria-hidden={false}
                    />
                </div>
                <div className='timeline-content'>
                    <p className='heading'>{m}</p>
                </div>
            </div>
        );
    }

    /* **************************************************************************
     * _formatMeetingTime (private static)                                 */ /**
     *
     * mimic the moment package 'ha MMM Do' formatting
     * ha - hour am/pm; MMM - 3 letter month abbrev.; Do - Day of Month 1st 2nd ... 30th 31st
     * e.g. returns a string like: '10am Feb 21st'.
     * if we need to get much fancier, such as localizing the month abbreviations, that would
     * be worth including the moment package.
     *
     * @param {Date} dt
     *
     * @returns {string} a formatted representation of the given date
     */
    static _formatMeetingTime(dt) {
        const dayOfMonth = dt.getDate();
        return `${getHoursAmPm(dt)} ${monthAbbrs[dt.getMonth()]} ${dayOfMonth}${ordinalIndicator(dayOfMonth)}`;
    }
}

/** Array of english month abbreviations */
const monthAbbrs = [ 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec' ];

/* ******************************************************************************
 * ordinalIndicator                                                        */ /**
 *
 * Get the english ordinal indicator string (st, nd, rd, th) for the given
 * whole integer number.
 *
 * @param {number} n - whole number to return the ordinal indicator for
 *
 * @returns {string} ordinal indicator
 */
function ordinalIndicator(n) {
    const specialIndicators = [ 'st', 'nd', 'rd' ];
    const defaultIndicator = 'th';
    const n100 = n % 100; // Handle numbers greater than 100
    if (n100 < 21 && n100 > 3) {
        return defaultIndicator; // th
    }

    const rem = n100 % 10;
    switch (rem) {
        case 1:
        case 2:
        case 3:
            return specialIndicators[rem - 1];
        default:
            return defaultIndicator;
    }
}

/* ******************************************************************************
 * getHoursAmPm                                                            */ /**
 *
 * Get a 12 hour with appended am/pm formatted string representing the hour
 * of the given Date.
 *
 * @param {Date} dt - Date object w/ hour to be formatted
 *
 * @returns {string} formatted am/pm hour string
 */
function getHoursAmPm(dt) {
    const hour24 = dt.getHours();
    const am = hour24 < 12;
    const hour12 = (am ? hour24 : hour24 - 12) || 12;
    return `${hour12}${am ? 'a' : 'p'}m`;
}

/* **************************************************************************** *
 * Module exports                                                               *
 * **************************************************************************** */
export {
    MeetingListElement,
};
