/* ******************************************************************************
 * index.js                                                                     *
 * *************************************************************************/ /**
 *
 * @fileoverview Hook up the Dashboard to redux state and actions
 *
 * [More detail about the file's contents]
 *
 * Created on       August 27, 2018
 * @author          Dan Calacci
 * @author          Michael Jay Lippert
 *
 * @copyright (c) 2018-present Riff Learning, Inc.,
 *            MIT License (see https://opensource.org/licenses/MIT)
 *
 * ******************************************************************************/

import { connect } from 'react-redux';

import {
    loadMeetingData,
    loadRecentMeetings,
    selectMeeting,
} from 'redux/actions/dashboard';
import { logger } from 'libs/utils';

import { DashboardView } from './DashboardView';

const dashboardMapProps = {
    mapStateToProps: state => ({
        uid:                    state.auth.user.uid,
        riffAuthToken:          state.riff.authToken,
        meetings:               state.dashboard.meetings,
        fetchMeetingsStatus:    state.dashboard.fetchMeetingsStatus,
        fetchMeetingsMessage:   state.dashboard.fetchMeetingsMessage,
        lastFetched:            state.dashboard.lastFetched,
        selectedMeeting:        state.dashboard.selectedMeeting || null,
    }),

    mapDispatchToProps: dispatch => ({
        loadRecentMeetings: (uid, selectedMeeting) => {
            dispatch(loadRecentMeetings(uid, selectedMeeting));
        },

        handleRefreshClick: (event, uid, selectedMeeting) => {
            dispatch(loadRecentMeetings(uid, selectedMeeting));
        },

        handleMeetingClick: (event, uid, meeting) => {
            event.preventDefault();
            logger.debug('selected meeting', meeting._id);
            /// TODO: Why do we want 2 distinct actions to select the meeting and to load the meeting data? -mjl 2018-09-05
            dispatch(selectMeeting(meeting));
            dispatch(loadMeetingData(uid, meeting._id));
        },
    }),
};


const ConnectedDashboard = connect(dashboardMapProps.mapStateToProps,
                                   dashboardMapProps.mapDispatchToProps)(DashboardView);

/* **************************************************************************** *
 * Module exports                                                               *
 * **************************************************************************** */
export {
    ConnectedDashboard as Dashboard,
};
