/* ******************************************************************************
 * DashboardView.jsx                                                            *
 * *************************************************************************/ /**
 *
 * @fileoverview React component container for meeting metrics (the dashboard)
 *
 * [More detail about the file's contents]
 *
 * Created on       August 27, 2018
 * @author          Dan Calacci
 *
 * @copyright (c) 2018-present Riff Learning Inc.,
 *            MIT License (see https://opensource.org/licenses/MIT)
 *
 * ******************************************************************************/

// I haven't figured out for sure if we(Riff) want to use the underscore prefix to
// signify private properties/methods or not, and I've used that convention in here
// for now, so I'm disabling no-underscore-dangle. -mjl
/* eslint
    no-underscore-dangle: off,
 */

import React from 'react';
import PropTypes from 'prop-types';
import { ScaleLoader } from 'react-spinners';
import { Helmet } from 'react-helmet';

import { Colors, logger } from 'libs/utils';

import { MeetingList } from './MeetingList';
import {
    InfluenceChart,
    MeetingInfo,
    TimelineChart,
    TurnChart,
} from './Metrics';

const HelmetComponent = () => {
    return (
        <Helmet title='My Riffs - Riff'/>
    );
};

/* ******************************************************************************
 * DashboardView                                                           */ /**
 *
 * React component to present the Riff Metrics for meetings the user attended
 *
 ********************************************************************************/
class DashboardView extends React.Component {
    static propTypes = {

        /** ID of the current user */
        uid: PropTypes.string.isRequired,

        /** The list of meeting objects to be presented */
        meetings: PropTypes.arrayOf(PropTypes.object).isRequired,

        /** The meeting object that is currently selected or null if none is selected
         *  Note: PropTypes has no native way to distinguish between null and undefined
         *        allowing null is why this is "optional"
         */
        selectedMeeting: PropTypes.object,

        /** status of the meetings */
        fetchMeetingsStatus: PropTypes.oneOf([ 'loading', 'loaded', 'error' ]).isRequired,

        /** If the fetch status is an error, this is the error message */
        fetchMeetingsMessage: PropTypes.string,

        /** riffdata authorization token */
        riffAuthToken: PropTypes.string.isRequired,

        /** function to invoke when a meeting in the meeting list is clicked on (will select the meeting) */
        handleMeetingClick: PropTypes.func.isRequired,

        /** function to load the meetings for a particular user (it had better be the current user) */
        loadRecentMeetings: PropTypes.func.isRequired,
    };

    /* **************************************************************************
     * componentDidMount                                                   */ /**
     *
     * Lifecycle method of a React component.
     * This is invoked immediately after a component is mounted (inserted into the
     * tree). Initialization that requires DOM nodes should go here.
     *
     * Load the user's list of meetings. As this may be a time consuming operation
     * we wait until this page is mounted and we know that the list is actually
     * needed.
     *
     * @see {@link https://reactjs.org/docs/react-component.html#componentdidmount|React.Component.componentDidMount}
     */
    componentDidMount() {
        if (this.props.riffAuthToken) {
            logger.debug('going to load recent meetings (1)', this.props.uid);
            this.props.loadRecentMeetings(this.props.uid, this.props.selectedMeeting);
        }
    }

    /* **************************************************************************
     * render                                                              */ /**
     *
     * Required method of a React component.
     * @see {@link https://reactjs.org/docs/react-component.html#render|React.Component.render}
     */
    render() {
        const { uid,
                meetings,
                fetchMeetingsStatus,
                fetchMeetingsMessage,
                handleMeetingClick,
                selectedMeeting } = this.props;

        if (fetchMeetingsStatus === 'loading') {
            return (
                <div className='columns is-centered has-text-centered'>
                    <HelmetComponent/>
                    <div className='column'>
                        <ScaleLoader color={Colors.lightRoyal}/>
                    </div>
                </div>
            );
        }

        if (fetchMeetingsStatus === 'error') {
            return (
                <div className='columns is-centered has-text-centered is-vcentered' style={{ height: '92vh' }}>
                    <HelmetComponent/>
                    <div className='column is-vcentered' style={{ alignItems: 'center' }}>
                        <p className='is-size-4 is-primary'>{fetchMeetingsMessage}</p>
                        <ScaleLoader color={Colors.lightRoyal}/>
                    </div>
                </div>
            );
        }

        // Style for the div used to constrain the height of the a chart in row 1
        const chartHeightStyle = { height: '40vh', minHeight: '200px' };

        return (
            <div
                className='columns has-text-centered is-centered'
                style={{ marginTop: '0px', maxWidth: '100%', height: '150vh' }}
            >
                <HelmetComponent/>
                <div className='column is-one-quarter has-text-left' style={{ height: '92vh', minHeight: '720px' }}>
                    <MeetingList
                        meetings={meetings}
                        selectedMeeting={selectedMeeting}
                        onSelectionChanged={(e, m) => handleMeetingClick(e, uid, m)}
                    />
                </div>
                {/* The minWidth set to 0 on this column div and the one below is what lets the
                    charts in the flex items correctly resize when the browser windows is resized */}
                <div
                    className='column'
                    style={{ padding: '0', minWidth: 0 }}
                >
                    <div className='columns'>
                        <div className='column has-text-left'>
                            <MeetingInfo/>
                        </div>
                    </div>
                    <div className='columns is-centered'>
                        {/* The minWidth set to 0 on this column div is what lets the charts in
                            the flex items correctly resize when the browser windows is resized */}
                        <div
                            className='column'
                            style={{ paddingBottom: '0px', minWidth: 0 }}
                        >
                            <div className='columns is-centered'>
                                <div className='column is-one-third'>
                                    <div style={chartHeightStyle}>
                                        <TurnChart/>
                                    </div>
                                </div>
                                <div className='column is-one-third'>
                                    <div style={chartHeightStyle}>
                                        <InfluenceChart
                                            influenceType={'mine'}
                                        />
                                    </div>
                                </div>
                                <div className='column is-one-third'>
                                    <div style={chartHeightStyle}>
                                        <InfluenceChart
                                            influenceType={'theirs'}
                                        />
                                    </div>
                                </div>
                            </div>

                            <div
                                className='section'
                                style={{ padding: '0px' }}
                            >
                                <TimelineChart/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

/* **************************************************************************** *
 * Module exports                                                               *
 * **************************************************************************** */
export {
    DashboardView,
};
