/* ******************************************************************************
 * styled.js                                                                    *
 * *************************************************************************/ /**
 *
 * @fileoverview React components to apply styles that are common among
 * Chat components
 *
 * Created on       October 24, 2019
 * @author          Brec Hanson
 *
 * @copyright (c) 2019-present Riff Learning Inc.,
 *            MIT License (see https://opensource.org/licenses/MIT)
 *
 * ******************************************************************************/

import styled from 'styled-components';

import { Colors } from 'libs/utils';

const Menu = styled.aside.attrs({
    className: 'menu'
})`
    max-width: 14em;
    padding-right: 10px;
    border-right: 1px solid ${Colors.brightPlum};
`;

const MenuLabel = styled.div.attrs({
    className: 'menu-label'
})`
    font-size: ${props => props.fontSize || '14px'};
    text-transform: none;
    letter-spacing: 0em;
`;

const MenuLabelCentered = styled.div.attrs({
    className: 'menu-label has-text-centered'
})`
    font-size: 1em;
    text-transform: none;
    letter-spacing: 0em;
`;

/* **************************************************************************** *
 * Module exports                                                               *
 * **************************************************************************** */
export {
    Menu,
    MenuLabel,
    MenuLabelCentered,
};
