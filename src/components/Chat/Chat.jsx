/* ******************************************************************************
 * Chat.jsx                                                                     *
 * *************************************************************************/ /**
 *
 * @fileoverview React component for the Chat page
 *
 * The Chat page initially presents a 'Lobby' where the user can verify that
 * the audio and video are working before joining (or starting) a webrtc meeting.
 *
 * After joining or starting a meeting, the 'Meeting' page is displayed,
 * where the user will be able to see & hear anyone else who joins the meeting.
 *
 * Created on       May 13, 2019
 * @author          Jordan Reedie
 *
 * @copyright (c) 2019-present Riff Learning Inc.,
 *            MIT License (see https://opensource.org/licenses/MIT)
 *
 * ******************************************************************************/
import React from 'react';
import PropTypes from 'prop-types';

import { changeRoomName, unMuteAudio } from 'redux/actions/chat';
import { store } from 'redux/store';
import addWebRtcListeners from 'redux/listeners/webrtc';
import { logger } from 'libs/utils';

import { ChatContainer } from './styled';
import { Lobby } from './Lobby';
import { Meeting } from './Meeting';
import { USER_WEBCAM_ID } from './Common/constants';

class Chat extends React.Component {

    static propTypes = {

        /** either joins an ongoing meeting or starts a new one */
        joinMeeting: PropTypes.func.isRequired,

        /** attempt to use firebase's anonymous auth to log the user in
         *  if they are not already logged in
         */
        attemptLoginAnonymous: PropTypes.func.isRequired,

        /** React's dispatch function */
        dispatch: PropTypes.func.isRequired,

        /** mute or unmute the user's mic */
        toggleMicOnOff: PropTypes.func.isRequired,

        /** true if a user is currently in a meeting, false otherwise */
        inRoom: PropTypes.bool.isRequired,

        /** if the user reached this page via LTI,
         *  this object contains parameters passed via LTI launch
         *  otherwise, all values within the object are empty / falsy
         */
        lti: PropTypes.object,

        /** this object contains a user's (firebase account) email,
         *  uid, and whether or not they verified that account
         */
        user: PropTypes.shape({
            email: PropTypes.string,
            uid: PropTypes.string,
            verified: PropTypes.bool,
        }),

        // TODO - this is contained within the user object
        // delete it and use that instead
        authId: PropTypes.string,
    };

    constructor(props) {
        super(props);
        this.state = {
            webrtc: null,
        };

        this.toggleMicOnOff = this.toggleMicOnOff.bind(this);
    }

    /**
     * FIXME - componentWillMount is deprecated by React
     * FIXME - what do we replace it with, or do we even bother right now?
     * This will break if we update to React v17 - it's replaced by
     * UNSAFE_componentWillMount
     *
     * Can we put this stuff in componentDidMount?
     *
     * -jr 6.13.19
     *
     * See https://stackoverflow.com/questions/40828004/constructor-vs-componentwillmount-what-a-componentwillmount-can-do-that-a-const
     * It seems everything done here can go in the constructor and componentDidMount.
     * -mjl 2019-07-16
     */
    UNSAFE_componentWillMount() { // eslint-disable-line camelcase
        // TODO see note about authId above
        if (!this.props.user.uid && !this.props.authId) {
            logger.debug('Chat.WillMount: No user (named or anonymous) detected, ' +
                         'signing in anonymously to preserve chat identity');

            // NOTE: this is an asynchronous action creator,
            // we may need an "isSignedIn" state to display
            // a loading view until the anonymous user is actually created.
            this.props.attemptLoginAnonymous();
        }

        // change room name to group name if
        // user is logged in through LTI.
        if (this.props.lti.loggedIn) {
            const ltiRoomName = this.props.lti.user.group;
            this.props.dispatch(changeRoomName(ltiRoomName));
        }
    }

    componentDidMount() {
        const localVideoId = USER_WEBCAM_ID;
        // we do this after the component mounts because
        // the simplewebrtc initialization needs to hook into
        // the DOM and get the video element the user's webcam
        // will get dumped into
        const webrtc = addWebRtcListeners(
            this.props.user.email,
            localVideoId,
            this.props.dispatch,
            store.getState);

        // this will cause a double render, but that's fine.
        // eslint-disable-next-line react/no-did-mount-set-state
        this.setState({ webrtc: webrtc });

        logger.debug('> webrtc connection ID:',
                     webrtc.connection.connection.id);

        // reset audio
        // FIXME should probably be a single function call here
        this.props.dispatch(unMuteAudio());
        webrtc.unmute();
    }

    render() {
        let content;

        // there are two main rendering paths - either the user is in a room,
        // or they are not
        // if they are in the room, we want them to join the meeting
        // if they are not, we take them to the lobby where they can
        // join or start a meeting
        if (this.props.inRoom) {
            content = (
                <Meeting
                    webrtc={this.state.webrtc}
                    handleMuteMicClick={this.toggleMicOnOff}
                />
            );
        }
        else {
            content = (
                <Lobby
                    webrtc={this.state.webrtc}
                    joinMeeting={this.props.joinMeeting}
                    handleMuteMicClick={this.toggleMicOnOff}
                />
            );
        }

        return (
            <ChatContainer>
                {content}
            </ChatContainer>
        );
    }

    toggleMicOnOff(muted) {
        // NOTE - currently keeping this at the top level because
        // it's used in both Lobby and Meeting
        // worth considering if we even need to be able to mute in Lobby
        // - jr 6.18.19

        // we can only mute if webrtc has been set up, if it hasn't do nothing
        if (this.state.webrtc === null) {
            return;
        }

        // This function requires a valid webrtc
        this.props.toggleMicOnOff(muted, this.state.webrtc);
    }
}

/* **************************************************************************** *
 * Module exports                                                               *
 * **************************************************************************** */
export {
    Chat,
};
