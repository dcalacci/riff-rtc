/* ******************************************************************************
 * RemoteVideos.jsx                                                             *
 * *************************************************************************/ /**
 *
 * @fileoverview React component to display incoming video streams
 *
 * Displays either:
 *  - incoming a/v streams from WebRtc peers OR
 *  - incoming shared screen from whichever peer that is current sharing
 *     note -  displays only *remote* shared screens. if the local user is
 *     sharing, display the remote peers' videos.
 *
 * Created on       August 9, 2018
 * @author          Dan Calacci
 * @author          Jordan Reedie
 *
 * @copyright (c) 2018-present Riff Learning Inc.,
 *            MIT License (see https://opensource.org/licenses/MIT)
 *
 * ******************************************************************************/
import React from 'react';
import PropTypes from 'prop-types';

import { PeerColors, WebRtcNick, logger, readablePeers } from 'libs/utils';

import { PeerVideo } from './PeerVideo';
import { SharedScreen } from './SharedScreen';

// TODO - this is probably the gnarliest bit left,
// but I'm not sure that it's worth breaking up
class RemoteVideos extends React.Component {
    static propTypes = {

        /** list of all peers to display */
        peers: PropTypes.arrayOf(PropTypes.object).isRequired,

        /** If a Peer is sharing their screen, this will
         *   have the remote shared screen element
         *  Otherwise, this will be null
         */
        remoteSharedScreen: PropTypes.object,

        /** List of the IDs of all of the webrtc peers */
        riffIds: PropTypes.arrayOf(PropTypes.string),
    };

    peerVideo(peerInfo) {
        return (
            <PeerVideo
                key={peerInfo.id}
                id={peerInfo.id}
                displayName={peerInfo.displayName}
                video={peerInfo.video}
                type='peer'
                peerColor={peerInfo.color}
                peerCount={peerInfo.otherPeerCnt}
            />
        );
    }


    collectPeerVideos() {
        // TODO riffIds is informed by the list of peers,
        // so if the peer list changes the webRtcRiffId list should as well
        // if they ever do differ, it will be an error related to having it
        // in the first place. as far as I can tell nothing else uses it so
        // I'm going to remove it in the near future and if it is absolutely
        // necessary to get the list of riffIds we have a utility function
        // to get that from the list of peers anyway
        //  -- jr 6.12.19
        const riffIds = [ ...this.props.riffIds ].sort();
        const otherPeerCnt = this.props.peers.length;
        logger.debug('rendering', otherPeerCnt, 'peers....', this.props.peers);
        logger.debug('sorted riff ids:', riffIds);

        const peerObjToVideo = (peer, i) => {
            const [ riffId, displayName ] = WebRtcNick.getIdAndDisplayName(peer.nick);
            const idx = riffIds.indexOf(riffId);
            if (idx === -1) {
                logger.error(`Peer ${i} (${displayName}) has an ID (${riffId}) ` +
                             'that is not found in the list of riff IDs');
            }
            const color = PeerColors[idx];

            const peerInfo = {
                id: peer.id,
                video: peer.videoEl,
                color,
                otherPeerCnt,
                // riffId,       // for debugging
                displayName
            };

            return this.peerVideo(peerInfo);
        };

        const peerVideos = this.props.peers.map(peerObjToVideo);

        // less than four peers and we just return them in one row
        if (otherPeerCnt < 4) {
            return peerVideos;
        }

        // 4 or more peers and we return the videos separated
        // into two rows. copied from our mattermost implementation
        const videosOnTop = Math.ceil(otherPeerCnt / 2);
        return (
            <div className='column'>
                <div className='row'>
                    <div className='columns'>
                        {peerVideos.slice(0, videosOnTop)}
                    </div>
                </div>
                <div className='row' style={{ paddingTop: '25px' }}>
                    <div className='columns'>
                        {peerVideos.slice(videosOnTop)}
                    </div>
                </div>
            </div>
        );
    }

    videos() {
        if (this.props.remoteSharedScreen) {
            return (
                <SharedScreen
                    video={this.props.remoteSharedScreen}
                    peers={this.props.peers}
                />
            );
        }

        return this.collectPeerVideos();
    }

    render() {
        return (
            <div
                className='column'
                tabIndex='0'
                aria-label={`Video chat. ${readablePeers(this.props.peers)}`}
            >
                <div className="remotes" id="remoteVideos">
                    <div className="columns is-multiline is-centered is-mobile">
                        {this.videos()}
                    </div>
                </div>
            </div>
        );
    }
}

/* **************************************************************************** *
 * Module exports                                                               *
 * **************************************************************************** */
export {
    RemoteVideos,
};
