/* ******************************************************************************
 * SharedScreen.jsx                                                             *
 * *************************************************************************/ /**
 *
 * @fileoverview React component for a remote shared screen stream
 *
 * Add a peer's shared screen to the DOM. Since we only ever display one shared
 * screen stream at a time (and no other video streams), it also adds the
 * audio streams from all of the non-sharing peers' videos to the DOM
 *
 * Created on       January 17, 2019
 * @author          Jordan Reedie
 *
 * @copyright (c) 2019-present Riff Learning Inc.,
 *            MIT License (see https://opensource.org/licenses/MIT)
 *
 * ******************************************************************************/
import React from 'react';
import PropTypes from 'prop-types';

import { PeerVideo } from './PeerVideo';
import { PeerAudio } from './PeerAudio';


class SharedScreen extends React.Component {
    static propTypes = {

        /** the list of all other peers in the meeting */
        peers: PropTypes.arrayOf(PropTypes.object).isRequired,

        /** the video element (of the shared screen) we are adding to the DOM */
        video: PropTypes.object.isRequired,
    };

    /**
     * Takes a peer and returns a PeerAudio element with the
     * audio stream from that peer's video
     */
    isolateAudioFromPeer(peer) {
        const audioTrack = peer.videoEl.srcObject.getAudioTracks()[0];
        const stream = new MediaStream();
        stream.addTrack(audioTrack);
        return <PeerAudio audio={stream} id={peer.id}/>;
    }

    peerAudioTracks() {
        // we need to add the audio for all the hidden videos back to
        // the DOM, otherwise only the person who shares screen
        // will be able to communicate
        // but don't add audio from shared screen
        // this would cause echo / reverb
        return this.props.peers
            .filter(peer => !this.props.video.id.includes(peer.id))
            .map(peer => this.isolateAudioFromPeer(peer));
    }

    render() {
        return (
            <React.Fragment>
                <PeerVideo
                    key="shared_screen"
                    id="shared_screen"
                    type="screen"
                    video={this.props.video}
                    peerCount={1}
                />
                {this.peerAudioTracks()}
            </React.Fragment>
        );
    }
}

/* **************************************************************************** *
 * Module exports                                                               *
 * **************************************************************************** */
export {
    SharedScreen,
};
