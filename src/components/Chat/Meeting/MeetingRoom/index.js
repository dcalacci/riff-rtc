/* ******************************************************************************
 * index.js                                                                     *
 * *************************************************************************/ /**
 *
 * @fileoverview React MeetingRoomComponent connected to the redux store
 *
 * Created on       June 11, 2019
 * @author          Jordan Reedie
 *
 * @copyright (c) 2019-present Riff Learning Inc.,
 *            MIT License (see https://opensource.org/licenses/MIT)
 *
 * ******************************************************************************/

import { connect } from 'react-redux';

import { MeetingRoom } from './MeetingRoom';

const mapStateToProps = state => ({
    // FIXME - what's this check for?
    webRtcPeers: state.chat.webRtcPeers[0] === null ? [] : state.chat.webRtcPeers,
    webRtcRemoteSharedScreen: state.chat.webRtcRemoteSharedScreen,
    riffIds: state.chat.webRtcRiffIds,
});

const ConnectedMeetingRoom = connect(mapStateToProps)(MeetingRoom);

export {
    ConnectedMeetingRoom as MeetingRoom,
};
