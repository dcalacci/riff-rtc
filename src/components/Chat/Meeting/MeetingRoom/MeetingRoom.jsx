/* ******************************************************************************
 * MeetingRoom.jsx                                                              *
 * *************************************************************************/ /**
 *
 * @fileoverview React component to display remote streams and textchat
 *
 * Displays remote participants' a/v streams or a single shared
 * screen stream and the TextChat
 *
 * Created on       June 5, 2019
 * @author          Jordan Reedie
 *
 * @copyright (c) 2019-present Riff Learning Inc.,
 *            MIT License (see https://opensource.org/licenses/MIT)
 *
 * ******************************************************************************/
import React from 'react';
import PropTypes from 'prop-types';

import { calculateBitrate } from 'libs/utils/webrtc_utils';

import { EmptyRoom } from './EmptyRoom';
import { TextChat } from './TextChat';
import { RemoteVideos } from './RemoteVideos';


class MeetingRoom extends React.Component {
    static propTypes = {

        /** sets the outgoing bitrate limit for video streams */
        setVideoBitrateLimit: PropTypes.func.isRequired,

        /** If a Peer is sharing their screen, this will
         *   have the remote shared screen element
         *  Otherwise, this will be null
         */
        webRtcRemoteSharedScreen: PropTypes.object,

        /** list of all webrtc peers */
        webRtcPeers: PropTypes.arrayOf(PropTypes.object),

        /** List of the IDs of all of the webrtc peers */
        riffIds: PropTypes.arrayOf(PropTypes.string),
    };

    componentDidUpdate() {
        // we want to update the bitrate limit any time the number of peers changes
        // conveniently, the component updates every time this is the case
        // the operation is idempotent and inexpensive enough that we don't
        // mind if it runs a few times unnecessarily
        const updatedBitrate = calculateBitrate(this.props.webRtcPeers.length);
        this.props.setVideoBitrateLimit(updatedBitrate);
    }

    render() {
        // if there are no other people in the meeting room,
        // we want to just return an EmptyRoom
        if (this.props.webRtcPeers.length === 0) {
            return (<EmptyRoom/>);
        }

        return (
            <React.Fragment>
                <RemoteVideos
                    ref='remote'
                    peers={this.props.webRtcPeers}
                    remoteSharedScreen={this.props.webRtcRemoteSharedScreen}
                    riffIds={this.props.riffIds}
                />
                <TextChat/>
            </React.Fragment>
        );
    }
}

/* **************************************************************************** *
 * Module exports                                                               *
 * **************************************************************************** */
export {
    MeetingRoom,
};
