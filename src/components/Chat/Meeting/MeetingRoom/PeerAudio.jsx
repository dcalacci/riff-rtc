/* ******************************************************************************
 * PeerAudio.jsx                                                                *
 * *************************************************************************/ /**
 *
 * @fileoverview React component for Peer audio streams
 *
 * This is to add the audio stream to the DOM while a user
 * is sharing their screen. Since all of the PeerVideos are
 * removed when someone shares their screen, we need a way to add
 * the audio streams back to the DOM (otherwise no one would be able to hear
 * anyone but the person who is sharing their screen)
 *
 * Created on       January 17, 2019
 * @author          Jordan Reedie
 *
 * @copyright (c) 2019-present Riff Learning Inc.,
 *            MIT License (see https://opensource.org/licenses/MIT)
 *
 * ******************************************************************************/
import React from 'react';
import PropTypes from 'prop-types';

class PeerAudio extends React.Component {
    static propTypes = {

        /** the id of the peer whose audio stream we are rendering */
        id: PropTypes.string,

        /** the audio stream to add to the DOM */
        audio: PropTypes.object,
    };

    constructor(props) {
        super(props);
        this.addTrack = this.addTrack.bind(this);
    }

    addTrack(el) {
        // this can happen if webrtc state changes while we are re-rendering
        if (el === null || el === undefined) {
            return;
        }

        el.srcObject = this.props.audio;
        el.play();
    }

    render() {
        return (
            <audio
                id={this.props.id + '_audio_only'}
                autoPlay={true}
                ref={this.addTrack}
            />
        );
    }
}

/* **************************************************************************** *
 * Module exports                                                               *
 * **************************************************************************** */
export {
    PeerAudio,
};
