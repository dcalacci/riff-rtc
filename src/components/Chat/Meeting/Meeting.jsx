/* ******************************************************************************
 * Meeting.jsx                                                                  *
 * *************************************************************************/ /**
 *
 * @fileoverview React component for the 'Meeting'
 *
 * This is displayed after a user joins a Meeting - displays the user's
 * webcam and meeting mediator in the sidebar, and the other meeting
 * participants' a/v streams in 'MeetingRoom'
 *
 * Created on       June 5, 2019
 * @author          Jordan Reedie
 *
 * @copyright (c) 2019-present Riff Learning Inc.,
 *            MIT License (see https://opensource.org/licenses/MIT)
 *
 * ******************************************************************************/
import React from 'react';
import PropTypes from 'prop-types';

import { MeetingSidebar } from './MeetingSidebar';
import { MeetingRoom } from './MeetingRoom';

class Meeting extends React.Component {
    static propTypes = {

        /** webrtc object for interacting with simplewebrtc client */
        webrtc: PropTypes.object.isRequired,

        /** mute or unmute the user's mic */
        handleMuteMicClick: PropTypes.func,
    };

    render() {
        return (
            <div className="columns">
                <MeetingSidebar
                    webrtc={this.props.webrtc}
                    handleMuteMicClick={this.props.handleMuteMicClick}
                />
                <MeetingRoom
                    setVideoBitrateLimit={bitrateLimit => this.props.webrtc.setVideoBitrateLimit(bitrateLimit)}
                />
            </div>
        );

    }
}

/* **************************************************************************** *
 * Module exports                                                               *
 * **************************************************************************** */
export {
    Meeting,
};
