/* ******************************************************************************
 * ScreenShareButton.jsx                                                        *
 * *************************************************************************/ /**
 *
 * @fileoverview React component for the screen share button
 *
 * If the button is clicked, it does the following:
 *  - if the user is currently sharing, it stops sharing
 *  - if someone else is currently sharing, it is disabled (and does nothing)
 *  - if no one is sharing, it starts the screen sharing process
 *
 * Created on       May 13, 2019
 * @author          Jordan Reedie
 *
 * @copyright (c) 2019-present Riff Learning Inc.,
 *            MIT License (see https://opensource.org/licenses/MIT)
 *
 * ******************************************************************************/
import React from 'react';
import ScreenShareIcon from '@material-ui/icons/ScreenShare';
import StopScreenShareIcon from '@material-ui/icons/StopScreenShare';
import PropTypes from 'prop-types';


class ScreenShareButton extends React.Component {
    static propTypes = {

        /** the remote shared screen to be displayed to the user */
        webRtcRemoteSharedScreen: PropTypes.object,

        /** the user's local shared screen, to be displayed
         *  in place of their webcam
         */
        webRtcLocalSharedScreen: PropTypes.object,

        /** our webrtc object */
        webrtc: PropTypes.object.isRequired,

        /** handles both starting and stopping screen sharing */
        handleScreenShareClick: PropTypes.func.isRequired,
    };

    render() {
        const classNames = 'button is-rounded side-bar-icon';
        let icon = <ScreenShareIcon/>;
        let disabled = false;
        let ariaLabel = 'Share Your Screen';
        if (this.props.webRtcRemoteSharedScreen) {
            disabled = true;
        }
        else if (this.props.webRtcLocalSharedScreen) {
            icon = <StopScreenShareIcon/>;
            ariaLabel = 'Stop Sharing Your Screen';
        }

        const onClick = event => this.props.handleScreenShareClick(
            event,
            this.props.webRtcLocalSharedScreen,
            this.props.webRtcRemoteSharedScreen,
            this.props.webrtc
        );

        return (
            <button
                className={classNames}
                onClick={onClick}
                disabled={disabled}
                aria-label={ariaLabel}
            >
                {icon}
            </button>);
    }
}

/* **************************************************************************** *
 * Module exports                                                               *
 * **************************************************************************** */
export {
    ScreenShareButton,
};
