/* ******************************************************************************
 * index.js                                                                     *
 * *************************************************************************/ /**
 *
 * @fileoverview Connects LeaveRoomButtonComponent to redux
 *
 * Created on       June 5, 2019
 * @author          Jordan Reedie
 *
 * @copyright (c) 2019-present Riff Learning Inc.,
 *            MIT License (see https://opensource.org/licenses/MIT)
 *
 * ******************************************************************************/

import { push } from 'connected-react-router';
import { connect } from 'react-redux';

import { participantLeaveRoom } from 'redux/actions/riff';
import { leaveRoom } from 'redux/actions/chat';

import { LeaveRoomButton } from './LeaveRoomButton';

const mapStateToProps = state => ({
    meetingId: state.riff.meetingId,
    uid: state.auth.user.uid,
});

const mapDispatchToProps = dispatch => ({
    leaveRoom: (webrtc, meetingId, uid) => {
        dispatch(leaveRoom());
        // this does not dispatch an action
        // this removes the participant from the meeting in riff-server
        participantLeaveRoom(meetingId, uid);
        // stop measuring speaking events
        webrtc.stopSibilant();
        // make sure we tell webrtc we left so it emits an
        // appropriate event
        webrtc.leaveRoom();
        // stop the user's webcam
        webrtc.stopLocalVideo();
        dispatch(push('/riffs'));
    },
});

const mapMergeProps = (stateProps, dispatchProps, ownProps) => ({
    // NOTE - we are intentionally not passing
    // stateProps, dispatchProps, or ownProps
    // to the component - they were only necessary for
    // the below function
    // - jr 7.26.2019
    leaveRoom: () => {
        dispatchProps.leaveRoom(ownProps.webrtc, stateProps.meetingId, stateProps.uid);
    },
});

const ConnectedLeaveRoomButton = connect(
    mapStateToProps,
    mapDispatchToProps,
    mapMergeProps)(LeaveRoomButton);

/* **************************************************************************** *
 * Module exports                                                               *
 * **************************************************************************** */
export {
    ConnectedLeaveRoomButton as LeaveRoomButton,
};
