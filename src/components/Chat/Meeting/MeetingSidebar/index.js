/* ******************************************************************************
 * index.js                                                                     *
 * *************************************************************************/ /**
 *
 * @fileoverview Connects MeetingSidebarComponent to redux
 *
 * @author          Jordan Reedie
 *
 * @copyright (c) 2019-present Riff Learning Inc.,
 *            MIT License (see https://opensource.org/licenses/MIT)
 *
 * ******************************************************************************/

import { connect } from 'react-redux';

import { logger, reconcileDisplayName } from 'libs/utils';
import {
    shareScreen,
    stopShareScreen,
} from 'redux/actions/chat';

import { MeetingSidebar } from './MeetingSidebar';

const mapStateToProps = state => ({
    displayName: reconcileDisplayName(state.chat.displayName,
                                      state.auth.user.displayName),
    roomName: state.chat.roomName,
    webRtcLocalSharedScreen: state.chat.webRtcLocalSharedScreen,
    webRtcRemoteSharedScreen: state.chat.webRtcRemoteSharedScreen,
    webRtcPeers: state.chat.webRtcPeers[0] === null ? [] : state.chat.webRtcPeers,
    isMicMuted: state.chat.audioMuted,
    // FIXME - just set this to true until we decide
    // what we want to do with it
    isMeetingMediatorEnabled: true,
});

const mapDispatchToProps = dispatch => ({
    handleScreenShareClick: (event, localSharedScreen, remoteSharedScreen, webrtc) => {
        // if the user is currently sharing, we want to stop
        if (localSharedScreen) {
            dispatch(stopShareScreen());
            webrtc.stopScreenShare();
        }
        // if someone is already sharing
        // do nothing and log it
        else if (remoteSharedScreen) {
            logger.debug('someone else is already sharing!');
        }
        else {
            logger.debug('Sharing screen!');
            dispatch(shareScreen());
            webrtc.shareScreen();
        }
    },

});

const ConnectedMeetingSidebar = connect(
    mapStateToProps,
    mapDispatchToProps,
)(MeetingSidebar);


/* **************************************************************************** *
 * Module exports                                                               *
 * **************************************************************************** */
export {
    ConnectedMeetingSidebar as MeetingSidebar,
};
