/* ******************************************************************************
 * MeetingSidebar.jsx                                                           *
 * *************************************************************************/ /**
 *
 * @fileoverview React component for the Meeting Sidebar
 *
 * Displays the user's webcam, the display & room names,
 * the microphone mute button & screen share button,
 * and the Meeting Mediator
 *
 * Created on       May 13, 2019
 * @author          Jordan Reedie
 *
 * @copyright (c) 2019-present Riff Learning Inc.,
 *            MIT License (see https://opensource.org/licenses/MIT)
 *
 * ******************************************************************************/
import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';

import { UserVideo } from 'components/Chat/Common/UserVideo';
import { MicMuteButton } from 'components/Chat/Common/MicMuteButton';
import { Menu, MenuLabel, MenuLabelCentered } from 'components/Chat/Common/styled';

import { LeaveRoomButton } from './LeaveRoomButton';
import { MeetingMediator } from './MeetingMediator';
import { ScreenShareButton } from './ScreenShareButton';

class MeetingSidebar extends React.Component {
    static propTypes = {
        /** our webrtc object */
        webrtc: PropTypes.object.isRequired,

        /** the user's display name */
        displayName: PropTypes.string.isRequired,

        /** the name of the room the user is in */
        roomName: PropTypes.string.isRequired,

        /** handles both starting and stopping screen sharing */
        handleScreenShareClick: PropTypes.func.isRequired,

        /** if true, display the meeting mediator, otherwise don't */
        isMeetingMediatorEnabled: PropTypes.bool.isRequired,

        /** the remote shared screen to be displayed to the user */
        webRtcRemoteSharedScreen: PropTypes.object,

        /** the user's local shared screen, to be displayed
         *  in place of their webcam
         */
        webRtcLocalSharedScreen: PropTypes.object,

        /** true if the user's mic is currently muted, false otherwise */
        isMicMuted: PropTypes.bool.isRequired,

        /** handles muting and unmuting of the user's mic */
        handleMuteMicClick: PropTypes.func.isRequired,

        /** an array of all the Peer objects currently in the meeting */
        webRtcPeers: PropTypes.array.isRequired,
    };

    render() {
        return (
            <React.Fragment>
                <Helmet title={`${this.props.roomName} - Riff`}/>
                <Menu>
                    <MenuLabelCentered>
                        <LeaveRoomButton
                            webrtc={this.props.webrtc}
                        />
                    </MenuLabelCentered>
                    <MenuLabel>
                        {'Name: '}
                        <span style={{ fontWeight: 'bold' }}>
                            {this.props.displayName}
                        </span>
                    </MenuLabel>
                    <MenuLabel>
                        {'Room: '}
                        <span style={{ fontWeight: 'bold' }}>
                            {this.props.roomName}
                        </span>
                    </MenuLabel>

                    <UserVideo
                        sharedScreen={this.props.webRtcLocalSharedScreen}
                        webrtc={this.props.webrtc}
                    />

                    <div className="has-text-centered">
                        <div className="control">
                            <div className="columns">
                                <div className="column">
                                    <MicMuteButton
                                        isMicMuted={this.props.isMicMuted}
                                        handleMuteMicClick={this.props.handleMuteMicClick}
                                    />
                                </div>
                                <div className="column has-text-right">
                                    <ScreenShareButton
                                        webRtcRemoteSharedScreen={this.props.webRtcRemoteSharedScreen}
                                        webRtcLocalSharedScreen={this.props.webRtcLocalSharedScreen}
                                        webrtc={this.props.webrtc}
                                        handleScreenShareClick={this.props.handleScreenShareClick}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                    <MeetingMediator
                        isEnabled={this.props.isMeetingMediatorEnabled}
                        displayName={this.props.displayName}
                        webRtcPeers={this.props.webRtcPeers}
                    />
                </Menu>
            </React.Fragment>
        );
    }
}

/* **************************************************************************** *
 * Module exports                                                               *
 * **************************************************************************** */
export {
    MeetingSidebar,
};
