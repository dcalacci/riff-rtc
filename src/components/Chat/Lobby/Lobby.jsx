/* ******************************************************************************
 * Lobby.jsx                                                                    *
 * *************************************************************************/ /**
 *
 * @fileoverview React component for the Chat Lobby
 *
 * Presents the user with a page in which they can verify their
 * microphone & camera are working and join (or start) a meeting.
 *
 * Created on       May 13, 2019
 * @author          Jordan Reedie
 *
 * @copyright (c) 2019-present Riff Learning Inc.,
 *            MIT License (see https://opensource.org/licenses/MIT)
 *
 * ******************************************************************************/

import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';

import {
    FieldInput,
    FieldLabel,
    FieldSubLabel,
    FormField,
    JoinRoomBtn,
    LobbyBody,
    LobbyContainer,
    LobbyForm,
    LobbyHeading,
    LobbySubHeading
} from './styled';

import { JoinRoomErrorNotification } from './JoinRoomErrorNotification';
import { LobbySidebar } from './LobbySidebar';

const MEDIA_ERROR_WARNING = 'Make sure your camera and microphone are ready.';

class Lobby extends React.Component {
    static propTypes = {

        /** webrtc object for interacting with simplewebrtc client */
        webrtc: PropTypes.object,

        /** the user's display name
         *  - if a user has an account or is joining via LTI,
         *    the value will be pre-populated
         *  - if not, it will be empty
         */
        displayName: PropTypes.string,

        /** persist the changes made to displayName in redux */
        saveDisplayName: PropTypes.func.isRequired,

        /** the room name of the meeting the user is attempting to join
         *  - if the user navigated to the page from MakeMeetingCard
         *    or is joining via LTI, it will be pre-populated
         *  - if not, it will be empty
         */
        roomName: PropTypes.string,

        /** persist the changes made to roomName in redux */
        saveRoomName: PropTypes.func.isRequired,

        /** true if the user should not be able to modify their
         *  display name / room name
         *  false otherwise
         */
        isInputReadOnly: PropTypes.bool,

        /** true if there is an error requesting access to  the user's
         *  webcam or microphone
         *  false otherwise
         */
        mediaError: PropTypes.bool,

        /** true if the user's mic is muted, false otherwise */
        isMicMuted: PropTypes.bool,

        /** current volume of the user's microphone */
        volume: PropTypes.number,

        /** this object contains a user's (firebase account) email,
         *  uid, and whether or not they verified their account
         */
        user: PropTypes.object.isRequired,

        /** mute or unmute the user's mic */
        handleMuteMicClick: PropTypes.func,

        /** either joins an ongoing meeting or starts a new one */
        joinMeeting: PropTypes.func.isRequired,
    };


    constructor(props) {
        super(props);

        // Defining state from props is frequently considered an anti-pattern
        // @see {@link https://medium.com/@justintulk/react-anti-patterns-props-in-initial-state-28687846cc2e
        //            |React Anti-Patterns: Props in Initial State}
        // but we are doing this here deliberately, although it may warrant further thought.
        this.state = {
            displayName: props.displayName,
            roomName: props.roomName,
            meetingType: '',
            joinError: null,
        };

        this.roomNameInput = React.createRef();
        this.displayNameInput = React.createRef();
    }

    componentDidMount() {
        // use timeout to set focus because, on route change,
        // focus is set to body, in App.jsx
        setTimeout(() => {
            if (this.props.roomName) {
                this.displayNameInput.current.focus();
            }
            else {
                this.roomNameInput.current.focus();
            }
        }, 100);
    }

    handleFieldChange = (field, event) => {
        this.setState({ [field]: event.target.value });
    }

    handleFormSubmit = (e) => {
        e.preventDefault();

        const { displayName, roomName, meetingType } = this.state;

        if (!displayName || !roomName) {
            return;
        }

        // there is an error with the user's webcam or mic,
        // let them know and don't bother trying to join the room
        if (this.props.mediaError) {
            this.setState({ joinError: MEDIA_ERROR_WARNING });
            return;
        }

        // persist the display name and room name to redux
        // for use down the line
        this.props.saveDisplayName(displayName);
        this.props.saveRoomName(roomName);

        this.props.joinMeeting(displayName, roomName, meetingType);
    }

    handleClearJoinError = () => {
        this.setState({ joinError: null });
    }

    render() {
        const { displayName, roomName, meetingType, joinError } = this.state;
        // join button is disabled unless both fields have values
        const isJoinButtonDisabled = !displayName || !roomName;

        // TODO: Consider moving to external file and handling form fields
        // more gracefully - Brec 10-29-2019
        const fields = {
            ROOM_NAME: 'room-name',
            DISPLAY_NAME: 'display-name',
            MEETING_TYPE: 'meeting-type',
        };

        return (
            <LobbyContainer>
                <Helmet title={'Chat - Riff'}/>
                <LobbySidebar
                    mediaError={this.props.mediaError}
                    webrtc={this.props.webrtc}
                    isMicMuted={this.props.isMicMuted}
                    volume={this.props.volume}
                    handleMuteMicClick={this.props.handleMuteMicClick}
                    user={this.props.user}
                />
                <LobbyBody>
                    <LobbyHeading>
                        {'Join a Riff Call'}
                    </LobbyHeading>
                    <LobbySubHeading>
                        {'Enter a room name and your name to join a call.'} <br/>
                        {'Add what type of meeting you\'re having.'} <br/>
                        {'Your metrics will automatically display when the call is over.'}
                    </LobbySubHeading>
                    <LobbyForm onSubmit={this.handleFormSubmit}>
                        <JoinRoomErrorNotification
                            joinRoomErrorMessage={joinError}
                            clearJoinRoomError={this.handleClearJoinError}
                        />
                        <FormField>
                            <FieldLabel htmlFor={fields.ROOM_NAME}>
                                {'Room Name (required)'}
                                <FieldSubLabel>
                                    {'Enter the room name for this call.'}
                                </FieldSubLabel>
                            </FieldLabel>
                            <FieldInput
                                id={fields.ROOM_NAME}
                                type='text'
                                name='room'
                                value={roomName}
                                readOnly={this.props.isInputReadOnly}
                                onChange={e => this.handleFieldChange('roomName', e)}
                                required={true}
                                ref={this.roomNameInput}
                            />
                        </FormField>
                        <FormField>
                            <FieldLabel htmlFor={fields.DISPLAY_NAME}>
                                {'Your Name (required)'}
                                <FieldSubLabel>
                                    {'Type your name.'}
                                </FieldSubLabel>
                            </FieldLabel>
                            <FieldInput
                                id={fields.DISPLAY_NAME}
                                type='text'
                                name='name'
                                value={displayName}
                                readOnly={this.props.isInputReadOnly}
                                onChange={e => this.handleFieldChange('displayName', e)}
                                required={true}
                                ref={this.displayNameInput}
                            />
                        </FormField>
                        <FormField>
                            <FieldLabel htmlFor={fields.MEETING_TYPE}>
                                {'Meeting Type'}
                                <FieldSubLabel>
                                    {'Enter what your meeting is about,'} <br/>
                                    {'e.g. design review.'}
                                </FieldSubLabel>
                            </FieldLabel>
                            <FieldInput
                                id={fields.MEETING_TYPE}
                                type='text'
                                name='meetingType'
                                value={meetingType}
                                readOnly={this.props.isInputReadOnly}
                                onChange={e => this.handleFieldChange('meetingType', e)}
                                required={false}
                            />
                        </FormField>
                        <JoinRoomBtn
                            disabled={isJoinButtonDisabled}
                            type='submit'
                        >
                            {'Join Room'}
                        </JoinRoomBtn>
                    </LobbyForm>
                </LobbyBody>
            </LobbyContainer>
        );
    }
}

/* **************************************************************************** *
 * Module exports                                                               *
 * **************************************************************************** */
export {
    Lobby,
};
