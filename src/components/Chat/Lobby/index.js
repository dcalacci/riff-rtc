/* ******************************************************************************
 * index.js                                                                     *
 * *************************************************************************/ /**
 *
 * @fileoverview Connects Lobby to redux
 *
 * Created on       June 8, 2019
 * @author          Jordan Reedie
 *
 * @copyright (c) 2019-present Riff Learning Inc.,
 *            MIT License (see https://opensource.org/licenses/MIT)
 *
 * ******************************************************************************/

import { connect } from 'react-redux';

import {
    changeDisplayName,
    changeRoomName,
} from 'redux/actions/chat';
import { reconcileDisplayName } from 'libs/utils';

import { Lobby } from './Lobby';

const mapStateToProps = (state, ownProps) => ({
    mediaError: state.chat.getMediaError,
    displayName: reconcileDisplayName(state.chat.displayName,
                                      state.auth.user.displayName),
    roomName: state.chat.roomName,
    volume: state.chat.volume,
    isMicMuted: state.chat.audioMuted,
    user: state.auth.user,
    lti: state.lti,
    riff: state.riff,
    // if the user is logged in to lti, we don't want them
    // to be able to modify their display/room names
    isInputReadOnly: state.lti.loggedIn,
    joinMeeting: (displayName, roomName, meetingType) => {
        ownProps.joinMeeting(
            displayName,
            roomName,
            meetingType,
            // it's impossible to have webrtc peers before we
            // join a meeting, so pass an empty array for now
            // TODO - look into why joinMeeting takes a list of
            // peers in the first place
            // -- jr 6.12.19
            [],
            state.auth.user,
            state.lti,
            state.riff,
            ownProps.webrtc
        );
    },
});

const mapDispatchToProps = dispatch => ({
    saveRoomName: (roomName) => {
        dispatch(changeRoomName(roomName));
    },
    saveDisplayName: (displayName) => {
        dispatch(changeDisplayName(displayName));
    },
});

const ConnectedLobby = connect(
    mapStateToProps,
    mapDispatchToProps
)(Lobby);

/* **************************************************************************** *
 * Module exports                                                               *
 * **************************************************************************** */
export {
    ConnectedLobby as Lobby,
};
