/* ******************************************************************************
 * LobbySidebar.jsx                                                             *
 * *************************************************************************/ /**
 *
 * @fileoverview React component for the Lobby Sidebar
 *
 * Displays the user's webcam & microphone activity to help them get
 * ready to join a meeting
 *
 * Created on       May 13, 2019
 * @author          Jordan Reedie
 *
 * @copyright (c) 2019-present Riff Learning Inc.,
 *            MIT License (see https://opensource.org/licenses/MIT)
 *
 * ******************************************************************************/
import React from 'react';
import PropTypes from 'prop-types';

import { UserVideo } from 'components/Chat/Common/UserVideo';
import { Menu, MenuLabel } from 'components/Chat/Common/styled';

import { MicActivityBar } from './MicActivityBar';
import { ScreenShareWarning } from './ScreenShareWarning';
import { VideoPlaceholder } from './VideoPlaceholder';

class LobbySidebar extends React.Component {
    static propTypes = {

        /** true if there is an error requesting access to  the user's
         *  webcam or microphone
         *  false otherwise
         */
        mediaError: PropTypes.bool,

        /** webrtc object for interacting with simplewebrtc client */
        webrtc: PropTypes.object,

        /** true if the user's mic is muted, false otherwise */
        isMicMuted: PropTypes.bool.isRequired,

        /** current volume of the user's microphone */
        volume: PropTypes.number.isRequired,

        /** mute or unmute the user's mic */
        handleMuteMicClick: PropTypes.func.isRequired,

        /** this object contains a user's (firebase account) email,
         *  uid, and whether or not they verified their account
         */
        user: PropTypes.object.isRequired,
    };

    render() {
        let video;
        if (this.props.mediaError) {
            video = <VideoPlaceholder/>;
        }
        else {
            video = <UserVideo webrtc={this.props.webrtc}/>;
        }

        // if there is no email, the user is not signed in, so display nothing
        // TODO: alternatively display "Anonymous User, sign up and claim your metrics!" ? -mjl 2019-10-24
        let email = null;
        if (this.props.user.email) {
            email = (
                <MenuLabel fontSize={'1em'}>
                    <span style={{ fontWeight: 'bold' }}>{'Signed in as:'}</span><br/>
                    {this.props.user.email}
                </MenuLabel>
            );
        }

        return (
            <Menu>
                {video}
                {email}
                <div style={{ marginTop: '1em' }}>
                    <MicActivityBar
                        handleMuteMicClick={this.props.handleMuteMicClick}
                        isMicMuted={this.props.isMicMuted}
                        volume={this.props.volume}
                    />
                    <MenuLabel fontSize={'1em'}>
                        <p>
                            {'Make sure Riff has access to your webcam and microphone.'}
                            {' You can\'t join a video call without them.'}
                        </p>
                    </MenuLabel>
                    <ScreenShareWarning/>
                </div>
            </Menu>
        );
    }
}

/* **************************************************************************** *
 * Module exports                                                               *
 * **************************************************************************** */
export {
    LobbySidebar,
};
