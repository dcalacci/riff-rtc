/* ******************************************************************************
 * styled.js                                                                    *
 * *************************************************************************/ /**
 *
 * @fileoverview React components to apply styling to lobby components
 *
 * Created on       October 22, 2019
 * @author          Brec Hanson
 *
 * @copyright (c) 2019-present Riff Learning Inc.,
 *            MIT License (see https://opensource.org/licenses/MIT)
 *
 * ******************************************************************************/

import styled from 'styled-components';

import { Colors } from 'libs/utils';

const FieldInput = styled.input`
    background: #f6f0fb;
    border: none;
    box-shadow: none;
    border-bottom: 2px solid ${Colors.brightPlum};
    text-align: left;
    color: ${Colors.brightPlum};
    font-size: inherit;
    line-height: inherit;
    outline-width: 0;
    box-shadow: none;
    border-bottom: 2px solid ${Colors.brightPlum};

    &:-webkit-autofill,
    &:-internal-autofill-selected,
    &:-webkit-autofill:hover,
    &:-webkit-autofill:focus {
        -webkit-text-fill-color: ${Colors.brightPlum};
        /* The box shadow here removes the blue background on text inputs when
         * you have selected a value using Google's autocomplete function. The
         * following article from 2017 outlines this, although it seems to be
         * still a relevant tactic today:
         * https://webagility.com/posts/the-ultimate-list-of-hacks-for-chromes-forced-yellow-background-on-autocompleted-inputs
         *
         * ******************************************************************************/
        -webkit-box-shadow: 0 0 0 1.5em #f6f0fb inset;
    }

    .textarea {
        color: ${Colors.brightPlum};
    }

    ::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
        color: ${Colors.brightPlum};;
        opacity: 0.5; /* Firefox */
    }

    :-ms-input-placeholder {
        color: ${Colors.brightPlum};
        opacity: 0.5; /* Firefox */
    }

    ::-ms-input-placeholder { /* Microsoft Edge */
        color: ${Colors.brightPlum};
        opacity: 0.5;
    }
`;

const FieldLabel = styled.label`
    font-size: inherit;
    line-height: inherit;
    margin-right: 2em;
    line-height: 1.2em;
`;

const FieldSubLabel = styled.div`
  font-size: .7em;
  line-height: 1.5em;
`;

const FormField = styled.div`
    margin-bottom: 1em;
    font-size: 1.3em;
    line-height: 1em;
    display: flex;
    justify-content: space-between;
    align-items: center;
`;

const JoinRoomBtn = styled.button.attrs({
    className: 'button is-outlined is-primary'
})`
    margin-top: 1em;
    float: right;

    &:enabled {
        /* Use important to override bulma styles.
        This was in lieu of removing bulma classes and manually styling */
        color: #fff!important;
        background-color: ${Colors.brightPlum}!important;
    }
`;

const LobbyContainer = styled.div`
    display: flex;
`;

const LobbyBody = styled.main`
    padding: 0 2em;
`;

const LobbyHeading = styled.div`
    line-height: 1em;
    font-size: 2.5em;
    margin-bottom: .5em;
`;

const LobbySubHeading = styled.div`
    line-height: 1.4em;
    font-size: 1.5em;
`;

const LobbyForm = styled.form`
    margin-top: 3em;
    display: inline-block;
`;

const ErrorNotification = styled.div.attrs({
    className: 'notification is-warning has-text-centered'
})`
    margin-top: 10px;
`;

const MicActivityBarContainer = styled.div`
    margin-bottom: 5px;
    display: flex;
    justify-content: space-between;
    align-items: center;
`;

const Placeholder = styled.div.attrs({
    className: 'has-text-centered'
})`
    background: linear-gradient(30deg, ${Colors.lightRoyal} 12%, ${Colors.brightPlum} 87%);
    height: 144px;
    width: 200px;
    border-radius: 5px;
    display: flex;
    align-items: center;
    color: #fff;
    padding: 5px;
`;

/* **************************************************************************** *
 * Module exports                                                               *
 * **************************************************************************** */
export {
    ErrorNotification,
    FieldInput,
    FieldLabel,
    FieldSubLabel,
    FormField,
    JoinRoomBtn,
    LobbyBody,
    LobbyContainer,
    LobbyForm,
    LobbyHeading,
    LobbySubHeading,
    MicActivityBarContainer,
    Placeholder,
};
