/* ******************************************************************************
 * index.js                                                                     *
 * *************************************************************************/ /**
 *
 * @fileoverview React Chat component attached to the router and redux store
 *
 * Created on       August 5, 2018
 * @author          Dan Calacci
 * @author          Brec Hanson
 * @author          Jordan Reedie
 *
 * @copyright (c) 2018-present Riff Learning Inc.,
 *            MIT License (see https://opensource.org/licenses/MIT)
 *
 * ******************************************************************************/

import { connect } from 'react-redux';

import {
    joinRoom,
    joinedRoom,
    muteAudio,
    unMuteAudio,
} from 'redux/actions/chat';
import { attemptLoginAnonymous } from 'redux/actions/auth';
import {
    riffAddUserToMeeting,
} from 'redux/actions/riff';
import {
    WebRtcNick,
    addA11yBrowserAlert,
    logger,
    reconcileDisplayName,
} from 'libs/utils';

import { Chat } from './Chat';


const mapStateToProps = state => ({
    user: state.auth.user,
    inRoom: state.chat.inRoom,
    authId: state.auth.uid,
    lti: state.lti
});

const mapDispatchToProps = dispatch => ({
    joinMeeting: (displayName, roomName, meetingType, webRtcPeers, user, lti, riff, webrtc) => {
        // FIXME - there's a lot of logic in here.
        // FIXME - where should it go?
        const userDisplayName = reconcileDisplayName(displayName, user.displayName);
        let webRtcRoom = roomName;
        if (lti.loggedIn) {
            webRtcRoom = `${webRtcRoom}_${lti.context.id}`;
            logger.debug(`Chat.joinMeeting: lti user's webrtc room name set to: ${webRtcRoom}`);
        }
        logger.debug(`Chat.joinMeeting: calling riffAddUserToMeeting w/ room: "${webRtcRoom}"`);

        riffAddUserToMeeting(
            user.uid,
            user.email || '',
            webRtcRoom,
            userDisplayName,
            meetingType,
            webRtcRoom,
            webRtcPeers,
            riff.authToken
        );
        dispatch(joinRoom(webRtcRoom));
        webrtc.stopVolumeCollection();
        webrtc.joinRoom(webRtcRoom, function (err, rd) {
            logger.debug('Chat.joinMeeting: webrtc.joinRoom cb:', { err, rd });
            dispatch(joinedRoom(webRtcRoom));
        });
        // use nick property to share riff IDs with all users
        webrtc.changeNick(WebRtcNick.create(user.uid, userDisplayName));
    },
    attemptLoginAnonymous: () => {
        return dispatch(attemptLoginAnonymous());
    },
    toggleMicOnOff: (muted, webrtc) => {
        // NOTE - currently keeping this at the top level because
        // it's used in both Lobby and Meeting
        // worth considering if we even need to be able to mute in Lobby
        // - jr 6.18.19
        logger.debug(muted);
        if (muted) {
            dispatch(unMuteAudio());
            // TODO side-effects
            webrtc.unmute();
        }
        else {
            dispatch(muteAudio());
            webrtc.mute();
        }
        addA11yBrowserAlert(`Microphone is ${muted ? 'on' : 'off'}.`, 'polite');
    },
    dispatch: dispatch,
});

const ConnectedChat = connect(mapStateToProps,
                              mapDispatchToProps)(Chat);

/* **************************************************************************** *
 * Module exports                                                               *
 * **************************************************************************** */
export {
    ConnectedChat as Chat,
};
