import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import {
  attemptUserCreate,
  clearAuthError,
  changePasswordState,
  changeEmailState,
  changeDisplayNameState,
} from 'redux/actions/auth';

import SignUpView from './SignUpView';


const mapStateToProps = state => ({
  error: state.auth.error,
  isInvalid: (
    state.auth.input.displayName === ''
    || state.auth.input.email === ''
    || state.auth.input.password === ''
  ),
});

const mapDispatchToProps = dispatch => ({
  handleSignUp: event => {
    event.preventDefault();
    const {displayName, email, password} = event.target.elements;
    dispatch(attemptUserCreate(displayName.value, email.value, password.value));
  },

  handlePassword: pass => {
    dispatch(changePasswordState(pass));
  },

  handleDisplayName: displayName => {
    dispatch(changeDisplayNameState(displayName));
  },


  handleEmail: email => {
    dispatch(changeEmailState(email));
  },

  clearError: event => {
    event.preventDefault();
    dispatch(clearAuthError());
  }
});

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(SignUpView));

//export default withRouter(SignUpContainer);
