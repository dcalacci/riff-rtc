import React, { Component } from "react";
import { withRouter } from 'react-router-dom';
import styled from 'styled-components';
import {Helmet} from "react-helmet";


const GradientCol = styled.div.attrs({
  className: 'column'
})`
  height: 100%;
`;

const FixedCard = styled.div.attrs({
  className: 'card-content'
})`
  border-radius: 5px;
  background: linear-gradient(30deg, rgba(138,106,148,1) 12%, rgba(171,69,171,1) 87%);
  @media (min-width: 625px) {
    position: fixed;
    width: 40%;
  }
  p {
    color: #fff;
    margin-bottom: 0.5em !important;
  }
  a {
    color: #fff;
    text-decoration: underline;
  }
  a:hover {
    color: #afafaf;
  }
  label {
    color: #fff;
  }
`;

const SignInContent = styled.div.attrs({
  className: 'content'
})`
  max-height: 100%;
`;

const SignUpForm = ({
  labelName,
  inputName,
  inputType,
  placeHolder,
  handleFunction,
  error
}) => {
  let style = undefined;
  let describedby = undefined;
  let autofocus = false;

  if (error && error.type === inputName) {
    style = { backgroundColor: "#ff8982" };
    describedby = "signup-error-notification";
    autofocus = true;
  }

  return (
    <div className="field">
      <label className="label">
        {labelName} <span>(required)</span>
      </label>
      <div className="control">
        <input
          className="input"
          type={inputType}
          name={inputName}
          required
          placeholder={placeHolder}
          onChange={handleFunction}
          style={style}
          autoFocus={autofocus}
          aria-describedby={describedby}
        />
      </div>
    </div>
  );
};

const handleOnChange = (handler) => {
  // converts a function that takes an input
  // to a function that handles an onChange event
  return (event) => handler(event.target.value);
}

const SignUpView = ({
  handleSignUp,
  handleEmail,
  handleDisplayName,
  handlePassword,
  clearError,
  error,
  email,
  password,
  isInvalid
}) => (
  <div className="section">
    <Helmet title='Sign Up - Riff' />
    <div className="columns">
      <div className="column">
        <SignInContent>
          <h1> Sign up to see data about your conversations. </h1>
          <p>
            Creating a profile helps you get the most out of Riff. A profile gives
            you access to all your historical video chat data, and all new insights,
            as we add them to the product.
          </p>
        </SignInContent>
      </div>
      <GradientCol>
        <FixedCard>
          <p className="title">
            Sign Up
          </p>
          <p>Already have an account? <a href="/login">Log in!</a></p>

          <form onSubmit={handleSignUp}>
            {error && (
              <div id="signup-error-notification" className="notification is-warning">
                <button
                  className="delete"
                  onClick={clearError}
                  aria-label="Close form error message"
                />
                {error.message}
              </div>
            )}
            <SignUpForm
              labelName="Full Name"
              inputName="displayName"
              inputType="text"
              placeHolder="Casey Smith"
              handleFunction={handleOnChange(handleDisplayName)}
              error={error}
            />
            <SignUpForm
              labelName="Email"
              inputName="email"
              inputType="text"
              placeHolder="csmith@rifflearning.com"
              handleFunction={handleOnChange(handleEmail)}
              error={error}
            />
            <SignUpForm
              labelName="Password"
              inputName="password"
              inputType="password"
              placeHolder="something unique and long"
              handleFunction={handleOnChange(handlePassword)}
              error={error}
            />
            <p>
              By proceeding to create your account and use Riff, you agree to our&nbsp;
              <a target="_blank" href="https://www.rifflearning.com/terms-of-service">
                Terms of Service
              </a>
              &nbsp;and&nbsp;
              <a target="_blank" href="https://www.rifflearning.com/privacy-policy">
                Privacy Policy
              </a>.
            </p>
            <div className="field">
              <div className="control">
                <button className="button" type="submit" disabled={isInvalid}>Submit</button>
              </div>
            </div>
          </form>
        </FixedCard>
      </GradientCol>
    </div>
  </div>
)

export default SignUpView;
