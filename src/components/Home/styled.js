/* ******************************************************************************
 * styled.js                                                                    *
 * *************************************************************************/ /**
 *
 * @fileoverview React components to apply styling to home components
 *
 * Created on       OCtober 12, 2019
 * @author          Brec Hanson
 *
 * @copyright (c) 2019-present Riff Learning Inc.,
 *            MIT License (see https://opensource.org/licenses/MIT)
 *
 * ******************************************************************************/

import styled from 'styled-components';

import { Colors } from 'libs/utils';

const LargeHeading = styled.h1`
  /* Use !important to override bulma styles */
  font-size: 2.5rem!important;
  line-height: 2.5rem!important;
  margin-bottom: ${props => props.marginBottom ? props.marginBottom : '1rem!important'};
`;

const MediumHeading = styled.h2`
  /* Use !important to override bulma styles */
  font-size: 2rem!important;
  line-height: 2rem!important;
  margin-bottom: ${props => props.marginBottom ? props.marginBottom : '1rem!important'};
`;

const HomeSection = styled.section.attrs({
  className: 'section'
})`
  background-color: ${Colors.purpleWhite};
  max-width: 1200px;
  margin: 0 auto;
`;

const MakeMeetingCard = styled.div.attrs({
  className: 'card'
})`
background: ${Colors.lightRoyal};
border-radius: 5px;
.card-content p {
  color: ${Colors.white};
}
.title {
  white-space: pre-line;
  font-size: 1.8rem;
}
`;

const TimelineImage = styled.div`
  width: 95%;
  margin: auto;
  margin-top: 2rem;

  img {
    width: 100%;
    display: block;
  }
`;

const Flex = styled.div`
  display: flex;
  align-items: ${props => props.alignItems || 'unset'};
`;

const FlexChild = styled.div`
  flex: ${props => props.flex || 1};
`;

const PostMeetingTopContainer = styled(Flex)`
  width: 95%;
  margin: auto;
  margin-top: 2rem;
`;

const StaggeredGraphs = styled(FlexChild)`
  position: relative;
  height: 350px;

  img {
    height: 70%;
    width: auto;
    position: absolute;
  }

  img:nth-of-type(1) {
    top: 0;
    left: 0;
    z-index: 1;
  }
  img:nth-of-type(2) {
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    z-index: 2;
  }
  img:nth-of-type(3) {
    right: 0;
    bottom: 0;
    z-index: 3;
  }
`;

const PromoSection = styled.div`
  padding: 0 3rem;
`;

const UnorderedList = styled.ul`
  margin-top: 1rem;
  margin-left: 3rem;
  font-size: 1.25rem;
  list-style: unset;

  li {
    line-height: 1.5rem;
    margin-bottom: 1.5rem;
  }
`;

const MediatorPromoContainer = styled.div`
  background: ${Colors.lightRoyal};
  color: ${Colors.white};
  margin: 0 -1.5rem;
  padding: 2rem 5%;
`;

const MediatorPromoHeader = styled.div`
  margin-bottom: 2rem;
`;

const PromoImage = styled.img`
  width: 80%;
  display: block;
  margin: 0 auto;
`;

const FeedbackPromoContainer = styled(Flex)`
  margin-bottom: 2rem;
`;

const HomeParagraph = styled.p.attrs({
  className: 'is-size-5'
})`
  line-height: ${props => props.lineHeight ? props.lineHeight : 'unset'};
`;

/* **************************************************************************** *
 * Module exports                                                               *
 * **************************************************************************** */
export {
  HomeSection,
  MakeMeetingCard,
  TimelineImage,
  PostMeetingTopContainer,
  StaggeredGraphs,
  PromoSection,
  Flex,
  FlexChild,
  UnorderedList,
  MediatorPromoContainer,
  MediatorPromoHeader,
  PromoImage,
  FeedbackPromoContainer,
  LargeHeading,
  MediumHeading,
  HomeParagraph,
};
