/* ******************************************************************************
 * PostMeetingMetrics.jsx                                                       *
 * *************************************************************************/ /**
 *
 * @fileoverview React component to visualise and describe post meeting metrics
 *
 * [More detail about the file's contents]
 *
 * Created on       October 12, 2019
 * @author          Brec Hanson
 *
 * @copyright (c) 2019-present Riff Learning Inc.,
 *            MIT License (see https://opensource.org/licenses/MIT)
 *
 * ******************************************************************************/

import React from 'react';

import influencedImage from '../../../assets/post-meeting-metrics/influenced.png';
import influencedByImage from '../../../assets/post-meeting-metrics/influenced-by.png';
import speakingTimeImage from '../../../assets/post-meeting-metrics/donut-tt.png';
import timelineImage from '../../../assets/post-meeting-metrics/timeline.png';

import {
  FlexChild,
  HomeParagraph,
  MediumHeading,
  PostMeetingTopContainer,
  PromoSection,
  StaggeredGraphs,
  TimelineImage,
  UnorderedList } from './styled';

/* ******************************************************************************
 * PostMeetingMetrics                                                      */ /**
 *
 * React component to visualise and describe post meeting metrics
 *
 ********************************************************************************/
class PostMeetingMetrics extends React.PureComponent {
  /* **************************************************************************
   * render                                                              */ /**
   *
   * Required method of a React component.
   * @see {@link https://reactjs.org/docs/react-component.html#render|React.Component.render}
   */
  render() {
    return (
      <div>
        <PostMeetingTopContainer>
          <StaggeredGraphs>
            <img src={influencedImage}/>
            <img src={influencedByImage}/>
            <img src={speakingTimeImage}/>
          </StaggeredGraphs>
          <FlexChild>
            <PromoSection>
              <MediumHeading>{'Post-meeting Metrics'}</MediumHeading>
              <HomeParagraph lineHeight={'1.5rem'}>
                {`After each Riff video call, Riff will display a set of metrics
                  about the conversation. Use this data to see:`}
              </HomeParagraph>
              <UnorderedList>
                <li>{`How balanced your conversations are, and how they change over time.`}</li>
                <li>{`If you or other team members dominate conversations, or need to speak up more.`}</li>
                <li>{`Who you influence and influences you.`}</li>
              </UnorderedList>
            </PromoSection>
          </FlexChild>
        </PostMeetingTopContainer>
        <TimelineImage>
          <img src={timelineImage}/>
        </TimelineImage>
      </div>
    );
  }
}

/* **************************************************************************** *
 * Module exports                                                               *
 * **************************************************************************** */
export {
  PostMeetingMetrics,
};
