/* ******************************************************************************
 * Home.jsx                                                                     *
 * *************************************************************************/ /**
 *
 * @fileoverview React component for the Home page
 *
 * [More detail about the file's contents]
 *
 * Created on       June 22, 2018
 * @author          Dan Calacci
 * @author          Michael Jay Lippert
 *
 * @copyright (c) 2018-present Riff Learning Inc.,
 *            MIT License (see https://opensource.org/licenses/MIT)
 *
 * ******************************************************************************/

import React from 'react';
import { Helmet } from 'react-helmet';

import { Colors } from 'libs/utils';

import cameraImage from '../../../assets/camera.png';
import inviteImage from '../../../assets/invite.png';
import dataImage from '../../../assets/data.png';

import { MakeMeetingCard } from './MakeMeetingCard';
import { PostMeetingMetrics } from './PostMeetingMetrics';
import { FeedbackPromo } from './FeedbackPromo';
import { MediatorPromo } from './MediatorPromo';
import { HomeParagraph, HomeSection, LargeHeading } from './styled';

/* ******************************************************************************
 * Home                                                                    */ /**
 *
 * React component to present the Riff Platform home page
 *
 ********************************************************************************/
class Home extends React.Component {
  render() {
    return (
      <HomeSection>
        <Helmet title='Riff'/>
        <div className='columns'>
          <div className='column'>
            <div className='content'>
              <LargeHeading>{'Better Communication by Design'}</LargeHeading>
              <HomeParagraph>
                {`How we interact with our peers has a big impact on our success. Riff helps you
                  see the patterns of successful collaboration and gives you metrics to create better outcomes.`}
              </HomeParagraph>
            </div>
          </div>
          <div className='column is-one-third'>
            <MakeMeetingCard/>
          </div>
        </div>
        <div
          className='section has-text-centered is-centered'
          style={{
            marginLeft: '-1.5rem',
            marginRight: '-1.5rem',
            marginTop: '2rem',
            marginBottom: '2rem',
            paddingTop: '2rem',
            paddingBottom: '2rem',
            background: Colors.lightRoyal,
          }}
        >
          <div className='columns has-text-centered is-centered'>
            <div className='column is-one-quarter'>
              <div className='card' style={{ maxHeight: '20rem', objectFit: 'contain' }}>
                <figure className='image is-square'>
                  <img
                    style={{ maxHeight: '20rem', objectFit: 'contain' }}
                    src={cameraImage}
                    alt='Click Riff Video. Make sure the browser has access to your camera and mic.'
                  />
                </figure>
              </div>
            </div>
            <div className='column is-one-quarter'>
              <div className='card' style={{ maxHeight: '20rem', objectFit: 'contain' }}>
                <figure className='image is-square'>
                  <img
                    style={{ maxHeight: '20rem', objectFit: 'contain' }}
                    src={inviteImage}
                    alt='Type in a ROOM NAME. Invite other people to join your room and have a video chat.'
                  />
                </figure>
              </div>
            </div>
            <div className='column is-one-quarter'>
              <div className='card' style={{ maxHeight: '20rem', objectFit: 'contain' }}>
                <figure className='image is-square'>
                  <img
                    style={{ maxHeight: '20rem', objectFit: 'contain' }}
                    src={dataImage}
                    alt='Click Riff Metrics. See how you interacted with others during your video chat.'
                  />
                </figure>
              </div>
            </div>
          </div>
          <div className='columns is-centered has-text-left' style={{ color: 'white' }}>
            <div className='column is-three-quarters'>
              <LargeHeading>{'Measuring What Happens'}</LargeHeading>
              <HomeParagraph>
                {`Using research from the MIT Media Lab about how teams interact, Riff
                  gives you the data to choose the most effective ways to engage with your
                  peers. Riff never looks at what you say, just the conversational “signatures”,
                  such as turn-taking and time spoken. Using AI, Riff builds and refines the
                  interaction models of high-functioning teams.`}
              </HomeParagraph>
            </div>
          </div>
        </div>
        <FeedbackPromo/>
        <MediatorPromo/>
        <PostMeetingMetrics/>
      </HomeSection>
    );
  }
}

/* **************************************************************************** *
 * Module exports                                                               *
 * **************************************************************************** */
export {
  Home,
};
