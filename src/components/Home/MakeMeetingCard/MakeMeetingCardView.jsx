/* ******************************************************************************
 * MakeMeetingCardView.jsx                                                      *
 * *************************************************************************/ /**
 *
 * @fileoverview React component to enter a meeting room name and start a meeting
 *
 * [More detail about the file's contents]
 *
 * Created on       August 4, 2018
 * @author          Dan Calacci
 * @author          Michael Jay Lippert
 *
 * @copyright (c) 2018-present Riff Learning Inc.,
 *            MIT License (see https://opensource.org/licenses/MIT)
 *
 * ******************************************************************************/

import React from 'react';
import PropTypes from 'prop-types';

import { MakeMeetingCard } from '../styled';

// TODO: fill in args

/* ******************************************************************************
 * MakeMeetingCardView                                                     */ /**
 *
 * React component to enter a meeting room name and start a meeting
 *
 ********************************************************************************/
class MakeMeetingCardView extends React.PureComponent {
  static propTypes = {

    /** did the user authenticate via LTI launch from an LMS or just a regular log in (or anonymous) */
    isLti: PropTypes.bool.isRequired,

    /** The room name to enter in the room name input field */
    roomName: PropTypes.string.isRequired,

    /** is the room name invalid */
    isInvalid: PropTypes.bool.isRequired,

    /** function to  */
    handleKeyPress: PropTypes.func.isRequired,

    /** function to  */
    handleRoomNameChange: PropTypes.func.isRequired,

    /** function to  */
    joinChatRoom: PropTypes.func.isRequired,
  };

  /* **************************************************************************
   * render                                                              */ /**
   *
   * Required method of a React component.
   * @see {@link https://reactjs.org/docs/react-component.html#render|React.Component.render}
   */
  render() {
    return (
      <MakeMeetingCard>
        <div className='card-content'>
          <p className='title'>
            {`Try it now.
              Enter a room name to start a better meeting.`}
          </p>
          <div className='field has-addons'>
            <div className='control'>
              <input
                className='input'
                type='text'
                readOnly={this.props.isLti}
                value={this.props.roomName}
                placeholder='your-next-great-meeting'
                onChange={event => this.props.handleRoomNameChange(event.target.value)}
                onKeyPress={this.props.handleKeyPress}
              />
            </div>
            <div className='control'>
              <a
                className='button is-primary'
                disabled={this.props.isInvalid}
                onClick={() => this.props.joinChatRoom(this.props.roomName)}
              >
                {'Go'}
              </a>
            </div>
          </div>
        </div>
      </MakeMeetingCard>
    );
  }
}

/* **************************************************************************** *
 * Module exports                                                               *
 * **************************************************************************** */
export {
  MakeMeetingCardView,
};
