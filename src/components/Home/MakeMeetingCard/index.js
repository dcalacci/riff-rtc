/* ******************************************************************************
 * index.js                                                                     *
 * *************************************************************************/ /**
 *
 * @fileoverview Hook up the MakeMeetingCardView to redux state and actions
 *
 * [More detail about the file's contents]
 *
 * Created on       August 4, 2018
 * @author          Dan Calacci
 * @author          Michael Jay Lippert
 *
 * @copyright (c) 2018-present Riff Learning Inc.,
 *            MIT License (see https://opensource.org/licenses/MIT)
 *
 * ******************************************************************************/

import { connect } from 'react-redux';
import { push } from 'connected-react-router';

import { changeRoomNameState } from 'redux/actions/makeMeeting';
import { changeRoomName } from 'redux/actions/chat';

import { MakeMeetingCardView } from './MakeMeetingCardView';

const mapStateToProps = state => ({
  roomName: state.makeMeeting.roomName,
  isInvalid: state.makeMeeting.roomName === '',
  isLti: state.lti.loggedIn,
});

const mapDispatchToProps = dispatch => ({
  handleRoomNameChange: (roomName) => {
    dispatch(changeRoomNameState(roomName));
  },

  handleKeyPress: (event) => {
    if (event.key === 'Enter') {
      dispatch(changeRoomName(event.target.value));
      dispatch(push('/room'));
    }
  },

  joinChatRoom: (roomName) => {
    // logger.debug('event:', event);
    dispatch(changeRoomName(roomName));
    dispatch(push('/room'));
  },
});

const ConnectedMakeMeetingCard = connect(mapStateToProps, mapDispatchToProps)(MakeMeetingCardView);

/* **************************************************************************** *
 * Module exports                                                               *
 * **************************************************************************** */
export {
  ConnectedMakeMeetingCard as MakeMeetingCard,
};
