/* ******************************************************************************
 * MediatorPromo.jsx                                                            *
 * *************************************************************************/ /**
 *
 * @fileoverview React component to render promotional info
 *
 * The promotional information in this component includes a visual example of
 * Riff's meeting mediator and a description of how it works and why it's useful
 *
 * [More detail about the file's contents]
 *
 * Created on       October 12, 2019
 * @author          Brec Hanson
 *
 * @copyright (c) 2019-present Riff Learning Inc.,
 *            MIT License (see https://opensource.org/licenses/MIT)
 *
 * ******************************************************************************/

import React from 'react';

import meetingMediatorImage from '../../../assets/meeting-mediator.png';

import {
  Flex,
  FlexChild,
  HomeParagraph,
  LargeHeading,
  MediatorPromoContainer,
  MediatorPromoHeader,
  MediumHeading,
  PromoImage,
  PromoSection,
  UnorderedList,
} from './styled';

/* ******************************************************************************
 * MediatorPromo                                                           */ /**
 *
 * React component to render promotional info
 *
 ********************************************************************************/
class MediatorPromo extends React.PureComponent {
  /* **************************************************************************
   * render                                                              */ /**
   *
   * Required method of a React component.
   * @see {@link https://reactjs.org/docs/react-component.html#render|React.Component.render}
   */
  render() {
    return (
      <MediatorPromoContainer>
        <Flex alignItems={'center'}>
          <FlexChild flex={2}>
            <MediatorPromoHeader>
              <LargeHeading marginBottom={'0'}>{'Riff Data'}</LargeHeading>
              <p className='is-size-4'>{'Metrics to help you during and after your meetings.'}</p>
            </MediatorPromoHeader>
            <PromoSection>
              <MediumHeading marginBottom={'.5rem'}>{'Real-time Intervention'}</MediumHeading>
              <HomeParagraph lineHeight={'1.5rem'}>
                {`The Meeting Mediator measures your conversations in real-time and gives
                  passive feedback to the group.`}
              </HomeParagraph>
              <UnorderedList>
                <li>{'How the conversation changes as it\'s happening.'}</li>
                <li>{'If people are over- or under-contributing.'}</li>
                <li>{'How engaged participants are.'}</li>
              </UnorderedList>
            </PromoSection>
          </FlexChild>
          <FlexChild flex={1}>
            <PromoImage src={meetingMediatorImage}/>
          </FlexChild>
        </Flex>
      </MediatorPromoContainer>
    );
  }
}

/* **************************************************************************** *
 * Module exports                                                               *
 * **************************************************************************** */
export {
  MediatorPromo,
};
