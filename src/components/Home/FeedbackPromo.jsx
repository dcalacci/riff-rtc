/* ******************************************************************************
 * FeedbackPromo.jsx                                                            *
 * *************************************************************************/ /**
 *
 * @fileoverview React component to render promotional info about Riff's products
 *
 * [More detail about the file's contents]
 *
 * Created on       October 12, 2019
 * @author          Brec Hanson
 *
 * @copyright (c) 2019-present Riff Learning Inc.,
 *            MIT License (see https://opensource.org/licenses/MIT)
 *
 * ******************************************************************************/

import React from 'react';

import feedbackPromoImage from '../../../assets/feedback-promo.png';

import {
  FeedbackPromoContainer,
  FlexChild,
  HomeParagraph,
  LargeHeading,
  PromoImage,
} from './styled';

/* ******************************************************************************
 * FeedbackPromo                                                           */ /**
 *
 * React component to render promotional info about Riff's products
 *
 ********************************************************************************/
class FeedbackPromo extends React.PureComponent {
  /* **************************************************************************
   * render                                                              */ /**
   *
   * Required method of a React component.
   * @see {@link https://reactjs.org/docs/react-component.html#render|React.Component.render}
   */
  render() {
    return (
      <FeedbackPromoContainer alignItems={'center'}>
        <FlexChild>
          <PromoImage src={feedbackPromoImage}/>
        </FlexChild>
        <FlexChild>
          <LargeHeading marginBottom={'.5rem'}>{'Small Changes, Big Results'}</LargeHeading>
          <HomeParagraph>
            {`Even a small amount of feedback can make a big difference:
              learners in online courses who used Riff video twice a week had `}
            <span className='text-bold'>
              {`30% higher grades, and were twice as likely to complete the course`}
            </span>{`.`}
          </HomeParagraph>
        </FlexChild>
      </FeedbackPromoContainer>
    );
  }
}

/* **************************************************************************** *
 * Module exports                                                               *
 * **************************************************************************** */
export {
  FeedbackPromo,
};
