/* ******************************************************************************
 * riffdata-client.js                                                           *
 * *************************************************************************/ /**
 *
 * @fileoverview Connection to the riffdata server (currently feathers socket)
 *
 * [More detail about the file's contents]
 *
 * Created on       August 15, 2018
 * @author          Dan Calacci
 *
 * @copyright (c) 2018-present Riff Learning, Inc.,
 *            MIT License (see https://opensource.org/licenses/MIT)
 *
 * ******************************************************************************/

import feathers from '@feathersjs/feathers';
import socketio from '@feathersjs/socketio-client';
import auth from '@feathersjs/authentication-client';
import io from 'socket.io-client';

// access to api

let dataserverPath = window.client_config.dataServer.path || '';
dataserverPath += '/socket.io';

const socket = io(window.client_config.dataServer.url, {
  timeout: 20000,
  path: dataserverPath,
  transports: [
    'websocket',
    'flashsocket',
    'htmlfile',
    'xhr-polling',
    'jsonp-polling'
  ]
});


const app = feathers()
  .configure(socketio(socket))
  .configure(auth({ jwt: {}, local: {} }));

/* **************************************************************************** *
 * Module exports                                                               *
 * **************************************************************************** */
export {
  app,
  socket,
};
