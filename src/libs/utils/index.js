/* ******************************************************************************
 * utils.js                                                                     *
 * *************************************************************************/ /**
 *
 * @fileoverview general utility functions
 *
 * Created on       August , 2017
 * @author          Jordan Reedie
 * @author          Michael Jay Lippert
 * @author          Brec Hanson
 *
 * @copyright (c) 2018-present Riff Learning Inc.,
 *            MIT License (see https://opensource.org/licenses/MIT)
 *
 * ******************************************************************************/

import { logger } from './logger';
import {
  cmpObjectProp,
  countByPropertyValue,
  groupByPropertyValue,
  mapObject,
  reverseCmp,
} from './collection_utils';
import {
  Colors,
  PeerColors,
  getColorForOther,
  getColorForSelf,
  getColorMap,
  getCountOtherColors,
  rgbaColor,
} from './colors';
import {
  WebRtcNick,
  readablePeers,
} from './webrtc_utils';


/**
 * Returns the time difference in seconds between the given
 * start and end times.
 */
function getDurationInSeconds(startTime, endTime) {
  const start = new Date(startTime);
  const end = new Date(endTime);
  const durationSecs = (end.getTime() - start.getTime()) / 1000;
  return durationSecs;
}

function isScreenShareSourceAvailable() {
  // currently we only support chrome v70+ (w/ experimental features enabled, if necessary)
  // and firefox
  return !!(navigator.getDisplayMedia ||
            navigator.mediaDevices.getDisplayMedia ||
            navigator.mediaDevices.getSupportedConstraints().mediaSource);
}

/**
 * Determine if CSS animations are supported
 *
 * This function was found at:
 * @see <https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Animations/Detecting_CSS_animation_support>
 *
 * Here's the explanation of how this works (from the above page):
 *  For starters we define a few variables. We assume that animation is not
 *  supported by setting animation to false. We set the animationstring to
 *  animation which is the property we want to set later on. We create an array
 *  of browser prefixes to loop over and we set pfx to an empty string.
 *
 *  Then we check if the CSS animation-name property on the style collection for
 *  the element specified by the variable elem is set. This means the browser
 *  supports CSS animation without any prefix, which, to date, none of them do.
 *
 *  If the browser does not support non-prefixed animation and animation is still
 *  false, we iterate over all the possible prefixes, since all the major
 *  browsers are currently prefixing this property and changing its name to
 *  AnimationName instead.
 *
 *  Once this code is finished running, the value of animation will be false if
 *  CSS animation support isn't available, or it will be true. If it is true then
 *  both the animation property name and the keyframe prefix will be the right
 *  ones. So if you use a new Firefox, the property will be MozAnimation and the
 *  keyframe prefix -moz- and with Chrome it'll be WebkitAnimation and -webkit-.
 *  Notice browsers don't make it easy with the switching between camelCase and
 *  hyphen-ation.
 */
function isCSSAnimationsSupported() {
  let animation = false;
  let animationstring = 'animation';  // eslint-disable-line no-unused-vars
  let keyframeprefix = '';            // eslint-disable-line no-unused-vars
  const domPrefixes = [ 'Webkit', 'Moz', 'O', 'ms', 'Khtml' ];
  let pfx = '';
  const elem = document.createElement('div');

  if (elem.style.animationName !== undefined) {
    animation = true;
  }

  if (!animation) {
    for (let i = 0; i < domPrefixes.length; i++) {
      if (elem.style[domPrefixes[i] + 'AnimationName'] !== undefined) {
        pfx = domPrefixes[i];
        animationstring = pfx + 'Animation';
        keyframeprefix = '-' + pfx.toLowerCase() + '-';
        animation = true;
        break;
      }
    }
  }

  return animation;
}

/**
 * Temporarily adds a div to the DOM
 * (for one second)
 *
 * Contents of div will be read by screen reader
 * When passing a priority, the options are:
 *    1. 'assertive' - will interupt the current speech
 *    2. 'polite'(default) - will be read when current speech completes
 *    *Note: these are standards, but exact functionality can vary between screen readers
 */
function addA11yBrowserAlert(text, alertPriority) {
  const newAlert = document.createElement('div');
  const id = 'speak-' + Date.now();

  newAlert.setAttribute('id', id);
  newAlert.setAttribute('role', 'alert');
  newAlert.classList.add('a11y-browser-alert');
  newAlert.setAttribute('aria-live', alertPriority ? alertPriority : 'polite');
  newAlert.setAttribute('aria-atomic', 'true');

  document.body.appendChild(newAlert);

  window.setTimeout(function () {
    document.getElementById(id).innerHTML = text;
  }, 100);

  window.setTimeout(function () {
    document.body.removeChild(document.getElementById(id));
  }, 1000);
}

/**
 * Determine if an email is valid
 *
 * This function was found at:
 * http://emailregex.com/
 *
 * As stated on the website, there is no 100% gauranteed way to verify
 * that an email is valid (with regex or on the front-end).
 * But it is advertised as working 99.99% of the time
 * and that should be sufficient for front-end validation.
 */
function validateEmail(email) {
  // eslint-disable-next-line max-len
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}

/**
 * Determine what to display in the Meeting for the user's display name
 * There are two places we can get a display name from - the user's profile,
 * or the input box in the Lobby. If the user has entered anything in
 * the Lobby, we want to make sure we use that. If the user has not entered
 * anything yet, we want to default to the display name in their
 * profile (from their firebase acct)
 *
 * If they have not entered anything in the Lobby and they do not have
 * a profile, we default to the empty string
 *
 */
function reconcileDisplayName(inputDisplayName, profileDisplayName) {
  return inputDisplayName || profileDisplayName || '';
}

/* **************************************************************************** *
 * Module exports                                                               *
 * **************************************************************************** */
export {
  logger,
  countByPropertyValue,
  groupByPropertyValue,
  mapObject,
  cmpObjectProp,
  reverseCmp,
  Colors,
  PeerColors,
  getColorForOther,
  getColorForSelf,
  getColorMap,
  getCountOtherColors,
  rgbaColor,
  WebRtcNick,
  readablePeers,
  getDurationInSeconds,
  isScreenShareSourceAvailable,
  isCSSAnimationsSupported,
  addA11yBrowserAlert,
  validateEmail,
  reconcileDisplayName,
};
