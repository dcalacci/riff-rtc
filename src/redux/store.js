import { Provider } from 'react-redux';
import { applyMiddleware, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { routerMiddleware } from 'connected-react-router';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage'; // defaults to localStorage for web and AsyncStorage for react-native
import thunk from 'redux-thunk';
import browserHistory from "../history";
import createRootReducer from './reducers';
import addRiffListener from './listeners/riff';
import { logger } from '../libs/utils';


const composeEnhancers = composeWithDevTools({
  // Specify custom devTools options
});

// Apply the middleware to the store
let store = createStore(
  createRootReducer(browserHistory),
  composeEnhancers(
    applyMiddleware(thunk),
    applyMiddleware(routerMiddleware(browserHistory))
  ));

logger.debug('Start the RiffListener');
addRiffListener(store.dispatch, store.getState);

let persistor = persistStore(store);
export {store, persistor};

