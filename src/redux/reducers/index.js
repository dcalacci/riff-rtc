import { combineReducers } from 'redux';
import storage from 'redux-persist/lib/storage'; // defaults to localStorage for web and AsyncStorage for react-native
import { connectRouter } from 'connected-react-router';
import { persistReducer } from 'redux-persist';

import auth from './auth';
import chat from './chat';
import dashboard from './dashboard';
import lti from './lti';
import makeMeeting from './makeMeeting';
import menu from './menu';
import profile from './profile';
import riff from './riff';


const rootPersistConfig = {
  key: 'root',
  storage,
  blacklist: ['router', 'auth', 'chat', 'profile', 'riff', 'dashboard']
};

// we want our webRTC peers to be populated by our server,
// not saved state.
const chatPersistConfig = {
  key: 'chat',
  storage: storage,
  blacklist: ['webRtcPeers', 'webRtcRiffIds', 'volume', 'roomName',
              'inRoom', 'joiningRoom', 'getMediaError', 'audioMuted',
              'webRtcLocalSharedScreen', 'webRtcRemoteSharedScreen',
              'userSharing', 'displayName', 'savedDisplayName']
};

const authPersistConfig = {
  key: 'auth',
  storage: storage,
  blacklist: ['email', 'password', 'isForgotPassword', 'error',
              'input', 'isInvalid', 'resetLinkSent']
};

const dashPersistConfig = {
  key: 'dashboard',
  storage: storage,
  blacklist: ['fetchMeetingsStatus', 'statsStatus']
};

const profilePersistConfig = {
  key: 'profile',
  storage: storage,
  blacklist: ['changeDisplayNameStatus', 'changeDisplayNameMessage',
              'displayNameInput', 'resetPassRequested']
};

// default export is createRootReducer()
export default (history) => persistReducer(
  rootPersistConfig,
  combineReducers({
    auth: persistReducer(authPersistConfig, auth),
    lti: lti,
    riff: riff,
    menu: menu,
    dashboard: dashboard,
    chat: persistReducer(chatPersistConfig, chat),
    profile: persistReducer(profilePersistConfig, profile),
    makeMeeting: makeMeeting,
    router: connectRouter(history),
  }));
