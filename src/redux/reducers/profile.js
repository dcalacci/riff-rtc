import {
  CHANGE_DISPLAY_NAME,
  CHANGE_DISPLAY_NAME_INPUT,
  RESET_PASSWORD,
  DISMISS_RESET_PASSWORD,
  RESEND_VERIFICATION,
  DISMISS_RESEND_VERIFICATION
} from '../constants/ActionTypes';

const initialState = {
  resetPassRequested: false,
  resendVerificationRequested: false,
  changeDisplayNameMessage: '',
  changeDisplayNameStatus: 'waiting',
  displayNameInput: null
};

const profile = (state=initialState, action) => {
  switch(action.type) {
  case(CHANGE_DISPLAY_NAME_INPUT):
    return {...state, displayNameInput: action.displayName};
  case(CHANGE_DISPLAY_NAME):
    return {
      ...state,
      changeDisplayNameStatus: action.status,
      changeDisplayNameMessage: (action.message ? action.message : state.changeDisplayNameMessage),
      displayNameInput: (action.status !== 'success' ? state.displayNameInput : action.displayName)
    };
  case(RESET_PASSWORD):
    return {
      ...state,
      resetPassRequested: true
    };
  case(DISMISS_RESET_PASSWORD):
    return {
      ...state,
      resetPassRequested: false
    };
  case(RESEND_VERIFICATION):
    return {
      ...state,
      resendVerificationRequested: true
    };
  case(DISMISS_RESEND_VERIFICATION):
    return {
      ...state,
      resendVerificationRequested: false
    };
  default:
    return state;
  }
};

export default profile;
