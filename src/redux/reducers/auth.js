import {
  CREATE_USER_SUCCESS,
  CREATE_USER_FAIL,
  FORGOT_PASS_FAIL,
  CLEAR_ERROR,
  LOG_OUT,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_FAIL,
  LOGIN_ANONYMOUS,
  INPUT_STATE_CHANGE,
  CHANGE_EMAIL,
  FORGOT_PASSWORD,
  CANCEL_FORGOT_PASSWORD,
  RESET_PASSWORD,
  DISMISS_RESET_PASSWORD
} from '../constants/ActionTypes';

const initialState = {
  loggedIn: false,
  anonymous: true,
  uid: null,
  user: {
    email: null,
    uid: null,
    verified: false
  },
  error: null,
  input: {
    email: '',
    password: '',
    displayName: ''
  },
  isForgotPassword: false,
  resetLinkSent: false
}


const auth = (state = initialState, action) => {
  switch(action.type) {
  case(LOGIN_USER_SUCCESS):
    return {
      ...state,
      loggedIn: true,
      anonymous: false,
      user: action.user,
      uid: null,
      error: null
    };
  case(LOGIN_USER_FAIL):
    return { ...state, loggedIn: false, error: action.error };
  case(LOG_OUT):
    console.log("USER LOGGED OUT");
    return initialState;
  case(CREATE_USER_SUCCESS):
    return { ...state, loggedIn: true, anonymous: false, user: action.user };
  case(CREATE_USER_FAIL):
    return { ...state, loggedIn: false, error: action.error };
  case(FORGOT_PASS_FAIL):
    return { ...state, loggedIn: false, error: action.error };
  case(CLEAR_ERROR):
    return { ...state, error: null };
  case(LOGIN_ANONYMOUS):
    return {
      ...state,
      loggedIn: true,
      anonymous: true,
      user: { ...state.user, uid: action.uid },
      uid: action.uid, error: null
    };
  case(INPUT_STATE_CHANGE):
    return {
      ...state,
      input: {
        displayName: action.displayName !== null ? action.displayName : state.input.displayName,
        email: action.email !== null ? action.email : state.input.email,
        password: action.password !== null ? action.password : state.input.password
      }
    };
  case(CHANGE_EMAIL):
    return {
      ...state,
      user: {
        ...state.user,
        email: action.status === "success" ? action.email : state.user.email
      }
    };
  case(FORGOT_PASSWORD):
    return {
      ...state,
      isForgotPassword: true
    };
  case(CANCEL_FORGOT_PASSWORD):
    return {
      ...state,
      isForgotPassword: false,
      resetLinkSent: false,
      error: null
    };
  case(RESET_PASSWORD):
    return {
      ...state,
      resetLinkSent: true,
      error: null
    };
  case(DISMISS_RESET_PASSWORD):
    return {
      ...state,
      resetLinkSent: false
    };
  default:
    return state;
  }
}

export default auth;
