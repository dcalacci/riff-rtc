/* ******************************************************************************
 * dashboard.js                                                                 *
 * *************************************************************************/ /**
 *
 * @fileoverview Dashboard redux reducer function
 *
 * Handler for redux actions which modify the dashboard redux state.
 *
 * Created on       August 27, 2018
 * @author          Dan Calacci
 *
 * @copyright (c) 2018-present Riff Learning Inc.,
 *            MIT License (see https://opensource.org/licenses/MIT)
 *
 * ******************************************************************************/

import {
  DASHBOARD_FETCH_MEETINGS,
  DASHBOARD_FETCH_MEETING_INFLUENCE,
  DASHBOARD_FETCH_MEETING_STATS,
  DASHBOARD_FETCH_MEETING_TIMELINE,
  DASHBOARD_SELECT_MEETING,
  LOG_OUT
} from 'redux/constants/ActionTypes';
import { logger } from 'libs/utils';

/** Status enum of valid dashboard statuses */
const EStatus = {
  /** The "item" has been requested to be loaded, and loading is in process.
   * This is also used as the initial state before loading has started
   */
  LOADING:  'loading',

  /** The "item" has been successfully loaded and is available from the state */
  LOADED:   'loaded',

  /** The "item" failed to load and is not available from the state */
  ERROR:    'error',
};

const initialState = {
  fetchMeetingsStatus: EStatus.LOADING,
  fetchMeetingsMessage: 'Initial state, no meetings loaded',
  lastFetched: new Date('January 1, 2000 00:01:00'),
  meetings: [],
  selectedMeeting: null,
  statsStatus: EStatus.LOADING,

  /**
   * An array of general stats about the participants in the selected meeting
   * @type {Array<{ participantId: string,
   *                name: string,
   *                lengthUtterances: number,
   *                numUtterances: number,
   *                meanLengthUtterances: number }>}
   */
  meetingStats: null,

  influenceStatus: EStatus.LOADING,
  influenceData: null,
  timelineStatus: EStatus.LOADING,
  timelineData: null,
};

const dashboard = (state = initialState, action) => {
  switch (action.type) {
    case LOG_OUT:
      logger.debug('dashboard reducer: log out in dashboard');
      return initialState;

    case DASHBOARD_FETCH_MEETINGS: {
      const now = new Date();
      switch (action.status) {
        case EStatus.LOADING:
          // TODO: Figure out if fetching meetings always gets all the user's meetings
          //       and replaces the current set of meetings, or if it should get "more"
          //       meetings, and only be updating what is already loaded. I think that
          //       should actually be a different action or maybe just status. -mjl 2019-5-21
          return {
            ...state,
            fetchMeetingsStatus: action.status,
            fetchMeetingsMessage: action.message,
            meetings: [],
          };

        case EStatus.LOADED:
          return {
            ...state,
            fetchMeetingsStatus: action.status,
            fetchMeetingsMessage: action.message || 'Meetings successfully loaded',
            meetings: action.meetings,
            lastFetched: now,
          };

        case EStatus.ERROR:
          return {
            ...state,
            fetchMeetingsStatus: action.status,
            fetchMeetingsMessage: action.message,
          };

        default:
          // If the status is invalid, do nothing
          logger.error(`dashboard reducer: DASHBOARD_FETCH_MEETINGS invalid status: ${action.status}`, action);
          return state;
      }
    }

    case DASHBOARD_SELECT_MEETING:
      return { ...state, selectedMeeting: action.meeting };

    case DASHBOARD_FETCH_MEETING_STATS: {
      switch (action.status) {
        case EStatus.LOADING:
          // TODO: Figure out how selecting a meeting, and fetching the various metric
          //       data should relate to one another and what should happen if some fail
          //       and some succeed. -mjl 2019-5-21
          return {
            ...state,
            statsStatus: action.status,
            meetingStats: null,
          };

        case EStatus.LOADED:
          return {
            ...state,
            statsStatus: action.status,
            meetingStats: action.meetingStats,
          };

        case EStatus.ERROR:
          return {
            ...state,
            statsStatus: action.status,
          };

        default:
          // If the status is invalid, do nothing
          logger.error(`dashboard reducer: DASHBOARD_FETCH_MEETING_STATS invalid status: ${action.status}`, action);
          return state;
      }
    }

    case DASHBOARD_FETCH_MEETING_INFLUENCE: {
      switch (action.status) {
        case EStatus.LOADING:
          // TODO: Figure out how selecting a meeting, and fetching the various metric
          //       data should relate to one another and what should happen if some fail
          //       and some succeed. -mjl 2019-5-21
          return {
            ...state,
            influenceStatus: action.status,
            influenceData: null,
          };

        case EStatus.LOADED:
          return {
            ...state,
            influenceStatus: action.status,
            influenceData: action.influenceData,
          };

        case EStatus.ERROR:
          return {
            ...state,
            influenceStatus: action.status,
          };

        default:
          // If the status is invalid, do nothing
          logger.error(`dashboard reducer: DASHBOARD_FETCH_MEETING_INFLUENCE invalid status: ${action.status}`, action);
          return state;
      }
    }

    case DASHBOARD_FETCH_MEETING_TIMELINE: {
      switch (action.status) {
        case EStatus.LOADING:
          // TODO: Figure out how selecting a meeting, and fetching the various metric
          //       data should relate to one another and what should happen if some fail
          //       and some succeed. -mjl 2019-5-21
          return {
            ...state,
            timelineStatus: action.status,
            timelineData: null,
          };

        case EStatus.LOADED:
          return {
            ...state,
            timelineStatus: action.status,
            timelineData: action.timelineData,
          };

        case EStatus.ERROR:
          return {
            ...state,
            timelineStatus: action.status,
          };

        default:
          // If the status is invalid, do nothing
          logger.error(`dashboard reducer: DASHBOARD_FETCH_MEETING_TIMELINE invalid status: ${action.status}`, action);
          return state;
      }
    }

    default:
      return state;
  }
};

export default dashboard;
