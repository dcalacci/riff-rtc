import SimpleWebRTC from 'simplewebrtc';
import sibilant from 'sibilant-webaudio';

import { app } from 'libs/riffdata-client';
import { logger } from 'libs/utils';
import {
  addLocalSharedScreen,
  addPeer,
  addSharedScreen,
  getDisplayError,
  getMediaError,
  readyToCall,
  removeLocalSharedScreen,
  removePeer,
  removeSharedScreen,
  saveLocalWebrtcId,
  volumeChanged,
} from 'redux/actions/chat';
import {
  participantLeaveRoom,
  updateRiffMeetingId,
} from 'redux/actions/riff';


export default function (nick, localVideoId, dispatch, getState) {
  let signalmasterPath = window.client_config.signalMaster.path || '';
  signalmasterPath += '/socket.io';
  const webRtcConfig = {
    localVideoEl: localVideoId,
    remoteVideosEl: '', // handled by our component
    autoRequestMedia: true,
    url: window.client_config.signalMaster.url,
    nick: nick,
    socketio: {
      path: signalmasterPath,
      forceNew: true
    },
    media: {
      audio: true,
      video: {
        // TODO - the resolution here is rather low
        // this is good for cpu limited users,
        // but in the future we would like to implement variable resolution
        // to improve visual quality for those who can afford it
        width: { ideal: 320 },
        height: { ideal: 240 },
        // firefox doesn't support requesting a framerate other than
        // that which the user's webcam can natively provide
        // chrome does not have this limitation
        frameRate: { ideal: 12, max: 30 }
      }
    },
    debug: !!window.client_config.webrtc_debug,
  };

  const webrtc = new SimpleWebRTC(webRtcConfig);

  logger.debug('Listener.WebRtc: Creating webrtc constant...', webrtc);
  // logger.debug("Local Session ID:", webrtc.connection.socket.sessionId)

  const bindSibilantToStream = function (stream) {
    const sib = new sibilant(stream);

    if (sib) {
      logger.debug('Listener.WebRtc: Registering speaking detection for stream', stream);
      // FIXME - uh, is something supposed to be happening here?
      // can we just delete this? -jr
      webrtc.stopVolumeCollection = function () {
        // sib.unbind('volumeChange');
      };

      webrtc.startVolumeCollection = function () {
        sib.bind('volumeChange', function (data) {
          const state = getState();
          if (!state.chat.inRoom) {
            dispatch(volumeChanged(data));
          }
        }.bind(getState));
      };

      webrtc.stopSibilant = function () {
        sib.unbind('stoppedSpeaking');
      };

      // use this to show user volume to confirm audio/video working
      webrtc.startVolumeCollection();

      logger.debug(`Listener.WebRtc: binding to sib stoppedSpeaking w/ room? "${getState().chat.webRtcRoom}"`);
      sib.bind('stoppedSpeaking', (data) => {
        if (getState().chat.inRoom) {
          logger.debug('Listener.WebRtc.sib.stoppedSpeaking: create utterance for user: ' +
                       `${getState().auth.user.uid} in room: "${getState().chat.webRtcRoom}"`);
          app.service('utterances')
            .create({
              participant: getState().auth.user.uid,
              room: getState().chat.webRtcRoom,
              startTime: data.start.toISOString(),
              endTime: data.end.toISOString(),
              token: getState().riff.authToken,
            })
            .then(function (res) {
              logger.debug('Listener.WebRtc.sib.stoppedSpeaking: speaking event recorded:', res);
              dispatch(updateRiffMeetingId(res.meeting));
            })
            .catch(function (err) {
              logger.error('Listener.WebRtc: ERROR', err);
            });
        }
      });
    }
  };

  webrtc.on('videoAdded', function (video, peer) {
    logger.debug('Listener.WebRtc.videoAdded: added video', peer, video, 'nick:', peer.nick);
    dispatch(addPeer({ peer: peer, videoEl: video }));
  });

  webrtc.on('videoRemoved', function (video, peer) {
    const state = getState();
    dispatch(removePeer({ peer: peer, videoEl: video }));
    if (state.chat.inRoom) {
      logger.debug('Listener.WebRtc.videoRemoved: removing participant ' +
                   `${peer.nick} from meeting ${state.riff.meetingId}`);
      const [ riffId ] = peer.nick.split('|');
      participantLeaveRoom(state.riff.meetingId, riffId);
    }
  });

  webrtc.on('screenAdded', function (video, peer) {
    logger.debug('Listener.WebRtc.screenAdded: adding shared screen!', video, 'from', peer);

    dispatch(addSharedScreen({ videoEl: video, peer: peer }));
  });

  webrtc.on('screenRemoved', function (video, peer) {
    logger.debug('Listener.WebRtc.screenRemoved: removing shared screen!', { video, peer });
    dispatch(removeSharedScreen());
  });

  webrtc.on('localScreenAdded', function (video) {
    dispatch(addLocalSharedScreen(video));
  });

  webrtc.on('localScreenRemoved', function (video) {
    dispatch(removeLocalSharedScreen(video));
  });

  // this happens if the user ends via the chrome button
  // instead of our button
  webrtc.on('localScreenStopped', function (video) {
    dispatch(removeLocalSharedScreen(video));
  });

  webrtc.on('localScreenRequestFailed', function () {
    dispatch(getDisplayError());
  });

  webrtc.on('localStreamRequestFailed', function () {
    dispatch(getMediaError(true));
  });

  webrtc.on('localStream', function (stream) {
    if (stream.active) {
      dispatch(getMediaError(false));
    }
    bindSibilantToStream(stream);
  });

  webrtc.changeNick = function (nick) {
    this.config.nick = nick;
    this.webrtc.config.nick = nick;
  };

  webrtc.on('readyToCall', function (connectionId) {
    dispatch(getMediaError(false));
    logger.debug('Listener.WebRtc.readyToCall: local webrtc connection id:', connectionId);
    dispatch(saveLocalWebrtcId(connectionId));
    dispatch(readyToCall());
  });

  return webrtc;
}
