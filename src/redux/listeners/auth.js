import app from 'libs/firebase-app';
import { logger } from 'libs/utils';
import { loginAnonymously, loginUserSuccess } from 'redux/actions/auth';


export default function (dispatch /*, authState*/) {

  app.auth().onAuthStateChanged((user) => {
    logger.debug('Listener.Auth: Auth state changed.', user);
    if (user === null) {
      logger.info('Listener.Auth: No signed in user');
    }
    else if (user.isAnonymous) {
      logger.debug('Listener.Auth: An anonymous user signed in!', user.uid);
      dispatch(loginAnonymously(user.uid));
    }
    else {
      logger.debug('Listener.Auth: A named user signed in', { displayName: user.displayName,
                                                              email: user.email,
                                                              verified: user.emailVerified,
                                                              uid: user.uid });
      dispatch(loginUserSuccess({ user }));
    }
  });
}

