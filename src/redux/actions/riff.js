import { app, socket } from 'libs/riffdata-client';
import { logger } from 'libs/utils';

import {
  RIFF_AUTHENTICATE_FAIL,
  RIFF_AUTHENTICATE_SUCCESS,
  RIFF_MEETING_ID_UPDATE,
  RIFF_PARTICIPANTS_CHANGED,
  RIFF_TURN_UPDATE,
} from 'redux/constants/ActionTypes';


export const riffAuthSuccess = (token) => {
  return { type: RIFF_AUTHENTICATE_SUCCESS,
           token: token };
};

export const riffAuthFail = (err) => {
  return { type: RIFF_AUTHENTICATE_FAIL,
           error: err };
};

export const updateTurnData = (transitions, turns) => {
  logger.debug('Action.Riff: updating turn data:', transitions, turns);
  return { type: RIFF_TURN_UPDATE,
           transitions: transitions,
           turns: turns };
};

export const updateMeetingParticipants = (participants) => {
  logger.debug('Action.Riff: updating riff meeting participants', participants);
  return { type: RIFF_PARTICIPANTS_CHANGED,
           participants: participants };
};

export const updateRiffMeetingId = (meetingId) => {
  return { type: RIFF_MEETING_ID_UPDATE,
           meetingId: meetingId };
};


// TODO - this doesn't return an action???
export const participantLeaveRoom = (meetingId, participantId) => {
  return app.service('meetings').patch(meetingId, {
    remove_participants: [ participantId ]
  })
  .then((res) => {
    logger.debug(`Action.Riff: removed participant: ${participantId} from meeting ${meetingId}`, res);
    return true;
  })
  .catch(function (err) {
    logger.error('Action.Riff: shit, caught an error leaving the room:', err);
    return false;
  });
};

export const attemptRiffAuthenticate = () => (dispatch) => {
  app.authenticate({
    strategy: 'local',
    email: window.client_config.dataServer.email,
    password: window.client_config.dataServer.password,
  })
  .then(function (result) {
    logger.debug('Action.Riff: data-server auth result!: ', result);
    dispatch(riffAuthSuccess(result.accessToken));
  }.bind(this))
  .catch(function (err) {
    logger.error('Action.Riff: data-server auth ERROR:', err);
    dispatch(riffAuthFail(err));
    logger.info('Action.Riff: trying to authenticate again...');
    dispatch(attemptRiffAuthenticate());
  });
};

export const riffAddUserToMeeting = (uid, email, roomName, nickName, meetingType,
                                     url, currentUsers, token) => {
  logger.debug('Action.Riff: [riff] adding users to meeting ');

  const parts = currentUsers.map(
    (user) => { return { participant: user.id }; });

  return socket.emit('meetingJoined', {
    participant: uid,
    email: email,
    name: nickName,
    participants: parts,
    room: roomName,
    description: meetingType,
    meetingUrl: url,
    consent: true,
    consentDate: new Date().toISOString(),
    token: token
  });
};
