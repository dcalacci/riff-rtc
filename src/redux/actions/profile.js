import app from 'libs/firebase-app';
import { app as riffServer } from 'libs/riffdata-client';
import {
  CHANGE_DISPLAY_NAME,
  CHANGE_DISPLAY_NAME_INPUT,
  DISMISS_RESEND_VERIFICATION,
  DISMISS_RESET_PASSWORD,
  RESEND_VERIFICATION,
  RESET_PASSWORD,
} from 'redux/constants/ActionTypes';

import { resetPassword } from './auth';

export const changeDisplayName = (displayName) => {
  return { type: CHANGE_DISPLAY_NAME, displayName: displayName, status: 'success' };
};

export const changeDisplayNameError = (errorMessage) => {
  return { type: CHANGE_DISPLAY_NAME, status: 'error', message: errorMessage };
};

export const changeDisplayNameLoading = () => {
  return { type: CHANGE_DISPLAY_NAME, status: 'loading' };
};

export const clearDisplayNameError = () => {
  return { type: CHANGE_DISPLAY_NAME, status: 'waiting' };
};

export const handleChangeDisplayName = displayName => (dispatch) => {
  dispatch(changeDisplayNameLoading());
  const user = app.auth().currentUser;
  user.updateProfile({ displayName: displayName }).then(() => {
    dispatch(changeDisplayName(displayName));
    // update the participant's name in the server
    // so the dashboard stats have the correct labels
    // patch returns a promise, but we don't care about
    // the results so we ignore it
    riffServer.service('participants').patch(user.uid, { name: displayName });
  }).catch((err) => {
    dispatch(changeDisplayNameError(err.message));
  });

};

export const handleDisplayNameInput = (displayName) => {
  return { type: CHANGE_DISPLAY_NAME_INPUT, displayName };
};

export const handleResetPassword = () => {
  const auth = app.auth();
  const user = auth.currentUser;
  resetPassword(user.email);
  return { type: RESET_PASSWORD };
};

export const dismissResetPassword = () => {
  return { type: DISMISS_RESET_PASSWORD };
};

export const resendVerification = () => {
  // TODO - this may not be the right spot for this
  const user = app.auth().currentUser;
  user.sendEmailVerification();
  return { type: RESEND_VERIFICATION };
};

export const dismissResendVerification = () => {
  return { type: DISMISS_RESEND_VERIFICATION };
};

