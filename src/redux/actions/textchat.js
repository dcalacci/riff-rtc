import { app } from 'libs/riffdata-client';
import { logger } from 'libs/utils';

import {
  TEXT_CHAT_MSG_UPDATE,
  TEXT_CHAT_SET_BADGE,
} from 'redux/constants/ActionTypes';

const updateTextChat = (message, meeting, participant, time) => {
  const messageObj = { message, meeting, participant, time };
  return {
    type: TEXT_CHAT_MSG_UPDATE,
    messageObj,
  };
};

const setTextChatBadge = (badgeValue) => {
  return {
    type: TEXT_CHAT_SET_BADGE,
    badgeValue,
  };
};

const sendTextChatMsg = (message, participant, meeting) => (dispatch) => {
  return app.service('messages').create({
    msg: message,
    participant: participant,
    meeting: meeting
  })
  .then(function (result) {
    logger.debug('Action.TextChat: created a message!', result);
    dispatch(updateTextChat(result.msg,
                            result.meeting,
                            result.participant,
                            result.time));
  })
  .catch(function (err) {
    logger.error('Action.TextChat: send msg errored out', err);
  });
};

/* **************************************************************************** *
 * Module exports                                                               *
 * **************************************************************************** */
export {
  sendTextChatMsg,
  setTextChatBadge,
  updateTextChat,
};
