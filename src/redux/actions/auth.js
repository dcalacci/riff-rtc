/* ******************************************************************************
 * auth.js                                                                      *
 * *************************************************************************/ /**
 *
 * @fileoverview Authentication redux actions
 *
 * [More detail about the file's contents]
 *
 * Created on       August 1, 2018
 * @author          Dan Calacci
 *
 * @copyright (c) 2018-present Riff Learning Inc.,
 *            MIT License (see https://opensource.org/licenses/MIT)
 *
 * ******************************************************************************/

import { push } from 'connected-react-router';

import firebase from 'firebase/app';
import 'firebase/auth';

import {
  CANCEL_FORGOT_PASSWORD,
  CLEAR_ERROR,
  CREATE_USER_FAIL,
  CREATE_USER_SUCCESS,
  DISMISS_RESET_PASSWORD,
  FORGOT_PASSWORD,
  FORGOT_PASS_FAIL,
  INPUT_STATE_CHANGE,
  LOGIN_ANONYMOUS,
  LOGIN_USER_FAIL,
  LOGIN_USER_SUCCESS,
  LOG_OUT,
  RESET_PASSWORD,
} from 'redux/constants/ActionTypes';
import app from 'libs/firebase-app';
import { app as riffServer } from 'libs/riffdata-client';
import { logger, validateEmail } from 'libs/utils';

export const createUserSuccess = (user) => {
  const userInfo = {
    displayName: user.displayName,
    email: user.email,
    uid: user.uid,
    verified: user.emailVerified
  };
  return {
    type: CREATE_USER_SUCCESS,
    user: userInfo,
  };
};

const updateErrorMessage = (error) => {
  const updatedError = Object.assign({}, error);
  switch (updatedError.code) {
    case 'auth/email-already-in-use':
      updatedError.message = 'This email address is already in use.';
      updatedError.type = 'email';
      break;

    case 'auth/invalid-email':
      updatedError.message = 'Please provide a valid email address.';
      updatedError.type = 'email';
      break;

    case 'auth/weak-password':
      updatedError.message = 'Password must be at least six characters long.';
      updatedError.type = 'password';
      break;

    // display same message for these cases
    case 'auth/too-many-requests':
    case 'auth/wrong-password':
    case 'auth/user-not-found':
      updatedError.message = 'Email / password combination not found!';
      updatedError.type = 'login';
      break;

    default:
      logger.warn('unexpected error message: ', error);
      // leave error at default error state
  }

  return updatedError;
};

export const createUserFail = (error) => {
  const updatedError = updateErrorMessage(error);
  return {
    type: CREATE_USER_FAIL,
    error: updatedError
  };
};

export const forgotPassFail = (error) => {
  const updatedError = updateErrorMessage(error);
  return {
    type: FORGOT_PASS_FAIL,
    error: updatedError
  };
};

export const loginUserSuccess = (resp) => {
  const userInfo = {
    displayName: resp.user.displayName,
    email: resp.user.email,
    uid: resp.user.uid,
    verified: resp.user.emailVerified
  };
  return {
    type: LOGIN_USER_SUCCESS,
    user: userInfo,
  };
};

export const loginUserFail = (error) => {
  const updatedError = updateErrorMessage(error);
  return {
    type: LOGIN_USER_FAIL,
    error: updatedError
  };
};


export const clearAuthError = () => {
  return {
    type: CLEAR_ERROR
  };
};

export const logOutUserNoRedirect = () => (dispatch) => {
  return app.auth().signOut()
    .then((res) => {
      logger.debug('action/auth: signed out?', res);
      dispatch({
        type: LOG_OUT
      });
      return res;
    });
};

// logout and redirect to the login page
export const logOutUser = () => (dispatch) => {
  return dispatch(logOutUserNoRedirect())
    .then((res) => {
      dispatch(push('/login'));
    });
};

export const changeInputState = (displayName, email, password) => {
  return {
    type: INPUT_STATE_CHANGE,
    displayName: displayName,
    email: email,
    password: password
  };
};

export const changeDisplayNameState = (displayName) => {
  return changeInputState(displayName, null, null);
};

export const changeEmailState = (email) => {
  return changeInputState(null, email, null);
};

export const changePasswordState = (pass) => {
  return changeInputState(null, null, pass);
};

export const loginAnonymously = (uid) => {
  logger.debug(`action/auth: Logged in anonymously! UID: ${uid}`);
  return {
    type: LOGIN_ANONYMOUS,
    uid: uid
  };
};

export const attemptLoginAnonymous = () => (dispatch) => {
  return app.auth().signInAnonymously()
    .then((resp) => {
      return dispatch(loginAnonymously(resp.user.uid));
    })
    .catch((error) => {
      // Handle Errors here.
      let errorCode = error.code;
      let errorMessage = error.message;
      logger.error('failed to sign in anonymously...');
      if (errorCode === 'auth/operation-not-allowed') {
        alert('You must enable Anonymous auth in the Firebase Console.');
      }
      else {
        logger.error(error);
      }
    });
};

export const attemptUserCreate = (displayName, email, pass) => (dispatch) => {
  // firebase password requirements are not configurable,
  // and default minimum password length is 6
  // so we have to handle that here
  // TODO we have set to 6 until we implement
  // custom password reset forms
  if (pass.length < 6) {
    const error = {
      code: 'auth/weak-password',
    };
    dispatch(createUserFail(error));
    return;
  }

  let createUser;
  if (app.auth().currentUser) {
    // there is no EmailAuthProvider under app.auth or app.auth()
    // so we use the static method from firebase.auth
    const credential = firebase.auth.EmailAuthProvider.credential(email, pass);
    createUser = () => app.auth().currentUser.linkWithCredential(credential);
  }
  else {
    createUser = () => app.auth().createUserWithEmailAndPassword(email, pass);
  }

  return createUser()
    .then((usercred) => {
      // update the riff-server particpant db so
      // our labels in the dashboard will display properly
      // patch returns a promise, but we don't care about
      // the results so we ignore it
      riffServer.service('participants').patch(usercred.user.uid, { name: displayName });
      return usercred.user.updateProfile({ displayName: displayName });
    })
    .then(() => {
      const user = app.auth().currentUser;
      dispatch(createUserSuccess(user));
      return user.sendEmailVerification();
    })
    .then(() => {
      // email sent
      dispatch(push('/profile'));
    })
    .catch(error => dispatch(createUserFail(error)));
};

export const attemptUserSignIn = (email, pass) => (dispatch) => {
  return app.auth().signInWithEmailAndPassword(email, pass)
    .then((resp) => {
      dispatch(loginUserSuccess(resp));
      dispatch(push('/riffs'));
    })
    .catch((error) => dispatch(loginUserFail(error)));
};

export const forgotPassword = () => {
  return { type: FORGOT_PASSWORD };
};

export const cancelForgotPassword = () => {
  return { type: CANCEL_FORGOT_PASSWORD };
};

export const resetPassword = email => (dispatch) => {
  if (validateEmail(email)) {
    return app.auth().sendPasswordResetEmail(email)
    .then((res) => {
      dispatch({
        type: RESET_PASSWORD
      });
      return res;
    });
  }
  else {
    const error = {
      code: 'auth/invalid-email',
    };
    dispatch(forgotPassFail(error));
    return;
  }
};

export const dismissResetPassword = () => {
  return { type: DISMISS_RESET_PASSWORD };
};

//
// module.exports = {
//
//   // called at app start
//   startListeningToAuth: function(){
//     return function(dispatch,getState){
//       fireRef.onAuth(function(authData){
//         if (authData){
//           dispatch({
//             type: C.LOGIN_USER,
//             uid: authData.uid,
//             username: authData.github.displayName || authData.github.username
//           });
//         } else {
//           if (getState().auth.currently !== C.ANONYMOUS){ // log out if not already logged out
//             dispatch({type:C.LOGOUT});
//           }
//         }
//       });
//     }
//   },
//   attemptLogin: function(){
//     return function(dispatch,getState){
//       dispatch({type:C.ATTEMPTING_LOGIN});
//       fireRef.authWithOAuthPopup("github", function(error, authData) {
//         if (error) {
//           dispatch({type:C.DISPLAY_ERROR,error:"Login failed! "+error});
//           dispatch({type:C.LOGOUT});
//         } else {
//           // no need to do anything here, startListeningToAuth have already made sure that we update on changes
//         }
//       });
//     };
//   },
//   logoutUser: function(){
//     return function(dispatch,getState){
//       dispatch({type:C.LOGOUT}); // don't really need to do this, but nice to get immediate feedback
//       fireRef.unauth();
//     };
//   }
// };
