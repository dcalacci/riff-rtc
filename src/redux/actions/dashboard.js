/* ******************************************************************************
 * dashboard.js                                                                 *
 * *************************************************************************/ /**
 *
 * @fileoverview Dashboard redux action creators
 *
 * [More detail about the file's contents]
 *
 * Created on       August 27, 2018
 * @author          Dan Calacci
 *
 * @copyright (c) 2018-present Riff Learning Inc.,
 *            MIT License (see https://opensource.org/licenses/MIT)
 *
 * ******************************************************************************/

import {
  DASHBOARD_FETCH_MEETINGS,
  DASHBOARD_FETCH_MEETING_INFLUENCE,
  DASHBOARD_FETCH_MEETING_STATS,
  DASHBOARD_FETCH_MEETING_TIMELINE,
  DASHBOARD_SELECT_MEETING,
} from 'redux/constants/ActionTypes';

import {
  cmpObjectProp,
  countByPropertyValue,
  getDurationInSeconds,
  groupByPropertyValue,
  logger,
  reverseCmp,
} from 'libs/utils';
import { app } from 'libs/riffdata-client';


// Error messages thrown (and checked in catch)
const NO_USEFUL_MEETINGS_FOUND = 'no useful meetings';
const NO_MEETINGS_WITH_OTHERS_FOUND = 'no meetings with other participants';

/* ******************************************************************************
 * updateMeetingList (sync action)                                         */ /**
 *
 * Update the list of meetings that the current user attended.
 */
const updateMeetingList = (meetings) => {
  return {
    type: DASHBOARD_FETCH_MEETINGS,
    status: 'loaded',
    meetings: meetings,
  };
};

/* ******************************************************************************
 * selectMeeting (sync action)                                             */ /**
 *
 * Select the specified meeting in the dashboard's list of meetings.
 */
const selectMeeting = (meeting) => {
  return {
    type: DASHBOARD_SELECT_MEETING,
    meeting: meeting,
  };
};

/* ******************************************************************************
 * loadMeetingData (async action)                                          */ /**
 *
 * Returns a thunk that will asynchronously load the stats for the specified
 * meeting, along with the metrics for the specified participant in the meeting.
 *
 * @param {string} uid
 * @param {string} meetingId
 *
 * @returns {ReduxThunk} which returns a promise which resolves when ALL the
 *    meeting stats and participant metrics have been loaded.
 */
function loadMeetingData(uid, meetingId) {
  async function thunk(dispatch /*, getState*/) {
    // Synchronously change the meeting stats, influence and timeline statuses to 'loading'
    dispatch({ type: DASHBOARD_FETCH_MEETING_STATS, status: 'loading' });
    dispatch({ type: DASHBOARD_FETCH_MEETING_TIMELINE, status: 'loading' });
    dispatch({ type: DASHBOARD_FETCH_MEETING_INFLUENCE, status: 'loading' });

    // TODO: we should also synchronously change the influence and timeline fetching status to 'loading'

    const rawUtterances = await app.service('utterances')
      .find({
        query: {
          meeting: meetingId,
          $limit: 10000,
          stitch: true,
          // $sort: { startTime: 1 } // requesting stitched utterances to be returned sorted does not work!
        }
      });

    const sortedUtterances = rawUtterances.slice().sort(cmpObjectProp('startTime'));
    const participantUtterances = groupByPropertyValue(sortedUtterances, 'participant');

    logger.debug('Action.dashboard.loadMeetingData: utterances:',
                 { raw: rawUtterances, sorted: sortedUtterances, grouped: participantUtterances });

    const speakingParticipantIds = Object.keys(participantUtterances);
    const participantsQResponse = await app.service('participants')
      .find({ query: { _id: { $in: speakingParticipantIds }, $limit: 100 } });
    if (participantsQResponse.total > participantsQResponse.limit) {
      logger.error('Action.dashboard.loadMeetingData: Error: Too many participants ' +
                   `(${participantsQResponse.total}) for query. Raise limit of ${participantsQResponse.limit}`);
    }
    const speakingParticipants = participantsQResponse.data
      .reduce((partMap, p) => partMap.set(p._id, { id: p._id, name: p.name, email: p.email }), new Map());

    logger.debug('Action.dashboard.loadMeetingData: speakingParticipants:',
                 { participantsQResponse, speakingParticipants });

    // Asynchronously get the meeting stats, influence data and timeline data.
    // They will dispatch actions updating their fetch status to loaded when they complete.
    return Promise.all([
      getMeetingStats(participantUtterances, speakingParticipants, meetingId, dispatch),
      getInfluenceData(sortedUtterances, speakingParticipants, meetingId, uid, dispatch),
      getTimelineData(sortedUtterances, speakingParticipants, meetingId, uid, dispatch),
    ]);
  }

  return thunk;
}

/* ******************************************************************************
 * loadRecentMeetings (async action)                                       */ /**
 *
 * Returns a thunk that will asynchronously load all the meetings that the given
 * user (uid) participated in.
 *
 * @param {string} uid
 *
 * @returns {ReduxThunk} which returns a promise which resolves when ALL the
 *    meetings have been loaded.
 */
function loadRecentMeetings(uid) {
  async function thunk(dispatch/*, getState*/) {
    // Synchronously change the fetch meetings status to 'loading'
    dispatch({ type: DASHBOARD_FETCH_MEETINGS, status: 'loading' });

    let participant = null;
    try {
      participant = await app.service('participants').get(uid);
      logger.debug(`Action.dashboard.loadRecentMeeting: get participant ${uid}`, { participant });

      const allParticipantsMeetings = await app.service('meetings')
        .find({
          query: {
            _id: { $in: participant.meetings },
          },
        });
      logger.debug(`Action.dashboard.loadRecentMeeting: meetings for participant ${uid}`, { allParticipantsMeetings });

      const usefulMeetings = allParticipantsMeetings.filter(isUsefulMeeting);

      if (usefulMeetings.length === 0) {
        throw new Error(NO_USEFUL_MEETINGS_FOUND);
      }

      const promiseAddParticipantsToMeetings = usefulMeetings.map(m => addAttendingParticipantsToMeeting(m));
      await Promise.all(promiseAddParticipantsToMeetings);

      // now that we have the participants in each meeting, also exclude meetings with
      // only 1 participant.

      // TODO: this will include meetings where someone joins but does not speak.
      // because we use the utterance data to inform our shit, the # of attendees will also be wrong.
      // right thing to do here is to try and create a service on the server that will reliably give
      // us # of attendees
      logger.debug('Action.dashboard.loadRecentMeeting: usefulMeetings w/ participants', usefulMeetings);
      const meetingsWithOthers = usefulMeetings.filter(m => m.participants.size > 1);

      logger.debug('Action.dashboard.loadRecentMeeting: useful meetings (pass1 and pass2):',
                   { usefulMeetings, meetingsWithOthers });

      if (meetingsWithOthers.length === 0) {
        throw new Error(NO_MEETINGS_WITH_OTHERS_FOUND);
      }

      // sort the meetings by descending time
      meetingsWithOthers.sort(reverseCmp(cmpObjectProp('startTime')));

      dispatch(updateMeetingList(meetingsWithOthers));

      if (meetingsWithOthers.length > 0) {
        const newSelectedMeeting = meetingsWithOthers[0];
        dispatch(selectMeeting(newSelectedMeeting));
        dispatch(loadMeetingData(uid, newSelectedMeeting._id));
      }

      // We're done at this point, return undefined to indicate that.
      return undefined;
    }
    catch (e) {
      if (participant === null) {
        logger.info(`Action.dashboard.loadRecentMeeting: Error getting participant ${uid}: ${e}`);
        return dispatch({
          type: DASHBOARD_FETCH_MEETINGS,
          status: 'error',
          message: 'No meetings found. Meetings that last for over two minutes will show up here.'
        });
      }

      if (e.message === NO_USEFUL_MEETINGS_FOUND) {
        return dispatch({
          type: DASHBOARD_FETCH_MEETINGS,
          status: 'error',
          message: "We'll only show meetings that lasted for over two minutes. Go have a riff!"
        });
      }

      if (e.message === NO_MEETINGS_WITH_OTHERS_FOUND) {
        return dispatch({
          type: DASHBOARD_FETCH_MEETINGS,
          status: 'error',
          message: 'Only had meetings by yourself? ' +
                   'Come back after some meetings with others to explore some insights.' });
      }

      logger.error(`Action.dashboard.loadRecentMeeting: Error: couldn't retrieve meetings: ${e}`, { e });

      // infinite loop? Keep trying (every 4 seconds)
      const retryHandle = setTimeout(() => dispatch(loadRecentMeetings(uid)), 4000);

      return { err: e, retryHandle };
    }
  }

  return thunk;
}

/* ******************************************************************************
 * getMeetingStats (async)                                                 */ /**
 *
 * Use the given list of raw utterance objects to calculate some simple stats for
 * the meeting the utterances belong to.
 * The stats are returned as an array of objects, with each object containing
 * stats for a participant in the meeting.
 * The object contains the participant id and name as well as the following
 * stats:
 *  - numUtterances - number of utterances made by the participant
 *  - lengthUtterances - sum of the duration of the participant's utterances in seconds
 *  - meanLengthUtterances - average duration of the participant's utterances in seconds
 *
 * It is expected (but not verified in any way) that the list of utterances is all
 * of the utterance objects for the specified meeting id.
 *
 * An Utterance is defined by this mongoose schema:
 *   const utteranceSchema = new Schema({
 *     participant: { type: String, ref: 'Participant' },
 *     meeting: { type: String, ref: 'Meeting' },
 *     startTime: Date,
 *     endTime: Date,
 *     volumes: [{
 *       timestamp: String,
 *       vol: Number
 *     }]
 *   })
 *
 * @param {Object<ParticipantId, Array<Utterance>>} participantUtterances
 * @param {Map<ParticipantId, { id: ParticipantId, name: string, email: string }>} speakingParticipants
 * @param {string} meetingId
 * @param {Function} dispatch
 *
 * @returns {Array<{ participantId: string,
 *                   name: string,
 *                   lengthUtterances: number,
 *                   numUtterances: number,
 *                   meanLengthUtterances: number }>}
 */
async function getMeetingStats(participantUtterances, speakingParticipants, meetingId, dispatch) {
  try {
    const participantStats = [];
    for (const participant of speakingParticipants.values()) {
      const utterances = participantUtterances[participant.id];
      const numUtterances = utterances.length;
      const totalSecsUtterances =
        utterances.reduce((uSecs, u) => uSecs + getDurationInSeconds(u.startTime, u.endTime), 0);
      const meanSecsUtterances = numUtterances ? totalSecsUtterances / numUtterances : 0;

      participantStats.push({
        name: participant.name,
        participantId: participant.id,
        numUtterances,
        lengthUtterances: totalSecsUtterances,
        meanLengthUtterances: meanSecsUtterances,
      });
    }

    logger.debug('Action.dashboard.getMeetingStats: success', { participantStats });

    // We've successfully loaded the meeting stats
    dispatch({
      type: DASHBOARD_FETCH_MEETING_STATS,
      status: 'loaded',
      meetingStats: participantStats,
    });

    return participantStats;
  }
  catch (e) {
    logger.error('Action.dashboard.getMeetingStats: ERROR encountered', e);

    dispatch({
      type: DASHBOARD_FETCH_MEETING_STATS,
      status: 'error',
      error: e,
    });

    throw e;
  }
}

/* ******************************************************************************
 * getInfluenceData (async)                                                */ /**
 *
 * goal is to process and create a network of who follows whom
 * each node is a participant
 * each directed edge A->B indicates probability that B follows A.
 *
 * @param {Array<Utterance>} sortedUtterances - utterances sorted ascending by startTime
 * @param {Map<ParticipantId, { id: ParticipantId, name: string, email: string }>} speakingParticipants
 *    Map of the meeting's speaking participants by id
 * @param {string} meetingId - unused
 * @param {string} uid - participant Id of the current user
 * @param {Function} dispatch
 *
 * @returns {Array<{ id: string,            - unique id for this element of the array
 *                   size: number,
 *                   source: string,        - participant Id
 *                   sourceName: string,    - participant Name
 *                   target: string,        - participant Id
 *                   targetName: string }>} - participant Name
 */
async function getInfluenceData(sortedUtterances, speakingParticipants, meetingId, uid, dispatch) {
  let recentUttCounts = sortedUtterances.map((ut, idx) => {
    // get list of utterances within 2 seconds that are not by the speaker.
    const recentUtterances = sortedUtterances.slice(0, idx).filter((recentUt) => {
      const timeDiff = getDurationInSeconds(recentUt.endTime, ut.startTime);
      const recent = timeDiff < 3 && timeDiff > 0;
      const sameParticipant = ut.participant === recentUt.participant;
      return recent && !sameParticipant;
    });
    if (recentUtterances.length > 0) {
      return { participant: ut.participant,
               counts: countByPropertyValue(recentUtterances, 'participant') };
    }
    return false;
  });

  recentUttCounts = recentUttCounts.filter(cnts => Boolean(cnts));
  logger.debug('Action.dashboard.getInfluenceData: recent utt counts:', recentUttCounts);

  // create object with the following format:
  // {participantId: {participantId: Count, participantId: Count, ...}}
  const aggregatedCounts = recentUttCounts.reduce((memo, ut) => {
    if (!(ut.participant in memo)) {
      memo[ut.participant] = {};
    }

    // update count object that's stored in memo, adding new
    // keys as we need to.
    // obj here should be an object of {participantId: nUtterances}
    const obj = memo[ut.participant];
    for (const [ k, v ] of Object.entries(ut.counts)) {
      if (k in obj) {
        obj[k] += v;
      }
      else {
        obj[k] = v;
      }
    }

    return memo;
  }, {});

  // limit to only the current user
  // aggregatedCounts = aggregatedCounts[uid];

  const finalEdges = [];
  for (const [ mainParticipant, counts ] of Object.entries(aggregatedCounts)) {
    for (const [ participant2, cnt ] of Object.entries(counts)) {
      const id = `e${finalEdges.length}`;
      const sParticipant = speakingParticipants.get(mainParticipant);
      const tParticipant = speakingParticipants.get(participant2);
      const edge = {
        id,
        source: mainParticipant,
        sourceName: sParticipant ? sParticipant.name : 'no name found',
        target: participant2,
        targetName: tParticipant ? tParticipant.name : 'no name found',
        size: cnt,
      };
      finalEdges.push(edge);
    }
  }

  logger.debug('Action.dashboard.getInfluenceData: success', { finalEdges });

  dispatch({
    type: DASHBOARD_FETCH_MEETING_INFLUENCE,
    status: 'loaded',
    influenceData: finalEdges,
  });

  return finalEdges;
}

/* ******************************************************************************
 * getTimelineData (async)                                                 */ /**
 *
 * Get all the data needed to display the timeline of participants talking
 * for a particular meeting.
 *
 * @param {Array<Utterance>} sortedUtterances - utterances sorted ascending by startTime
 * @param {Map<ParticipantId, { id: ParticipantId, name: string, email: string }>} speakingParticipants
 *    Map of the meeting's speaking participants by id
 * @param {string} meetingId - unused
 * @param {string} uid - participant Id of the current user
 * @param {Function} dispatch
 *
 * @returns {{ sortedParticipants: Array,
 *             utts: Array,
 *             startTime: string, - Date string ex. 2019-03-07T19:13:07.345Z
 *             endTime: string }}
 */
async function getTimelineData(sortedUtterances, speakingParticipants, meetingId, uid, dispatch) {
  // sortedUtterances are sorted by startTime, so the minStartTime is the startTime of the 1st one
  const minStartTime = sortedUtterances[0].startTime;
  const maxEndTime = sortedUtterances.reduce((maxTime, curUtt) => {
    return curUtt.endTime > maxTime ? curUtt.endTime : maxTime;
  }, '2000-01-01T00:00:00.000Z');

  // extract the utterance properties we care about (convert time strings to Date objects)
  const utts = sortedUtterances.map((u) => {
    return {
      participant: u.participant,
      startDate: new Date(u.startTime),
      endDate: new Date(u.endTime),
    };
  });

  // sort the participants into the order they should be displayed in the chart
  // putting current user first
  // Note: we are referencing the objects stored in speakingParticipants, except for
  //       the current user's, because we need to modify that one.
  //       No one else should modify these participant objects!
  const sortedParticipants = [];
  for (const p of speakingParticipants.values()) {
    if (p.id !== uid) {
      sortedParticipants.push(p);
    }
  }

  sortedParticipants.sort(cmpObjectProp('id'));
  sortedParticipants.unshift({ ...speakingParticipants.get(uid), name: 'You' });

  const timelineData = {
    utts,
    sortedParticipants,
    startTime: minStartTime,
    endTime: maxEndTime,
  };

  logger.debug('Action.dashboard.getTimelineData: success', { timelineData });

  dispatch({
    type: DASHBOARD_FETCH_MEETING_TIMELINE,
    status: 'loaded',
    timelineData,
  });

  return timelineData;
}

/* ******************************************************************************
 * isUsefulMeeting                                                         */ /**
 *
 * Return true if the meeting was a "useful" meeting.
 */
function isUsefulMeeting(meeting) {
  //  any meeting "in progress" is useful
  if (!meeting.endTime) {
    return true;
  }

  // meetings longer than 2 minutes are useful
  const durationSecs = getDurationInSeconds(meeting.startTime, meeting.endTime);
  return durationSecs > 2 * 60;
}

/* ******************************************************************************
 * addAttendingParticipantsToMeeting                                       */ /**
 *
 * [Description of addAttendingParticipantsToMeeting]
 *
 * @param {string} meeting
 *      [Description of the meeting parameter]
 *
 * @returns {string}
 */
async function addAttendingParticipantsToMeeting(meeting) {
  const participantEventsQResponse = await app.service('participantEvents')
    .find({ query: { meeting: meeting._id, $limit: 500 } });

  // use the participant events to find all participants in
  // that meeting, and add that set of participants to the meeting object
  const participantEvents = participantEventsQResponse.data;
  const participantIdsInEvents = participantEvents.flatMap(pe => pe.participants);
  meeting.participants = new Set(participantIdsInEvents);   // eslint-disable-line require-atomic-updates

  return meeting;
}

/* **************************************************************************** *
 * Module exports                                                               *
 * **************************************************************************** */
export {
  // Dashboard action creators
  updateMeetingList,
  selectMeeting,
  loadMeetingData,
  loadRecentMeetings,
};
