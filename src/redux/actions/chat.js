/* ******************************************************************************
 * chat.js                                                                      *
 * *************************************************************************/ /**
 *
 * @fileoverview Redux Chat action creators
 *
 * [More detail about the file's contents]
 *
 * Created on       August 5, 2018
 * @author          Dan Calacci
 *
 * @copyright (c) 2018-present Riff Learning Inc.,
 *            MIT License (see https://opensource.org/licenses/MIT)
 *
 * ******************************************************************************/

import {
  ADD_LOCAL_SHARED_SCREEN,
  ADD_PEER,
  ADD_SHARED_SCREEN,
  CHAT_CHANGE_DISPLAY_NAME,
  CHAT_CHANGE_ROOM_NAME,
  CHAT_DISPLAY_NAME_CHANGE,
  CHAT_GET_DISPLAY_ERROR,
  CHAT_GET_MEDIA_ERROR,
  CHAT_LEAVE_ROOM,
  CHAT_READY_TO_CALL,
  CHAT_SHARE_STREAM,
  CHAT_VOLUME_CHANGED,
  CHAT_WEBRTC_ID_CHANGE,
  JOINED_ROOM,
  JOIN_ROOM,
  MUTE_AUDIO,
  REMOVE_LOCAL_SHARED_SCREEN,
  REMOVE_PEER,
  REMOVE_SHARED_SCREEN,
  SHARE_SCREEN,
  STOP_SHARE_SCREEN,
  UNMUTE_AUDIO,
} from 'redux/constants/ActionTypes';
import app from 'libs/firebase-app';
import { addA11yBrowserAlert, logger } from 'libs/utils';

export const addPeer = (peer) => {
  addA11yBrowserAlert(`${peer.peer.nick.split('|')[1]} joined the room.`, 'polite');
  return {
    type: ADD_PEER,
    peer: peer,
  };
};

export const removePeer = (peer) => {
  addA11yBrowserAlert(`${peer.peer.nick.split('|')[1]} left the room.`, 'polite');
  return {
    type: REMOVE_PEER,
    peer: peer,
  };
};

export const addSharedScreen = (peer) => {
  return {
    type: ADD_SHARED_SCREEN,
    peer: peer,
  };
};

export const removeSharedScreen = () => {
  return {
    type: REMOVE_SHARED_SCREEN,
  };
};

export const addLocalSharedScreen = (screen) => {
  return {
    type: ADD_LOCAL_SHARED_SCREEN,
    screen: screen,
  };
};

export const removeLocalSharedScreen = (screen) => {
  return {
    type: REMOVE_LOCAL_SHARED_SCREEN,
    screen: screen,
  };
};

export const readyToCall = (roomName) => {
  return {
    type: CHAT_READY_TO_CALL,
    roomName: roomName,
  };
};

export const shareStream = (stream) => {
  return {
    type: CHAT_SHARE_STREAM,
    stream: stream,
  };
};

export const shareScreen = () => {
  return {
    type: SHARE_SCREEN,
  };
};

export const stopShareScreen = () => {
  return {
    type: STOP_SHARE_SCREEN,
  };
};

export const volumeChanged = (vol) => {
  return {
    type: CHAT_VOLUME_CHANGED,
    volume: vol,
  };
};

/**
 * getMediaError is invoked when there
 * is an error requesting a user's webcam
 * or microphone
 */
export const getMediaError = (error) => {
  return {
    type: CHAT_GET_MEDIA_ERROR,
    error: error,
  };
};

/**
 * getDisplayError is invoked when there
 * is an error requesting a user's screen
 * for screen sharing
 */
export const getDisplayError = (error) => {
  return {
    type: CHAT_GET_DISPLAY_ERROR,
    error: error,
  };
};

export const changeRoomName = (roomName) => {
  return {
    type: CHAT_CHANGE_ROOM_NAME,
    roomName: roomName,
  };
};

export const changeDisplayName = (displayName) => {
  return {
    type: CHAT_CHANGE_DISPLAY_NAME,
    displayName: displayName,
  };
};

export const joinRoom = (roomName) => {
  return {
    type: JOIN_ROOM,
    roomName: roomName,
  };
};

export const joinedRoom = (name) => {
  return {
    type: JOINED_ROOM,
    name: name,
  };
};

export const saveLocalWebrtcId = (webRtcId) => {
  logger.debug('saving local webrtc id:', webRtcId);
  return {
    type: CHAT_WEBRTC_ID_CHANGE,
    webRtcId: webRtcId,
  };
};

export const muteAudio = () => {
  logger.info('action/chat: muting audio');
  return { type: MUTE_AUDIO };
};

export const unMuteAudio = () => {
  return { type: UNMUTE_AUDIO };
};

export const leaveRoom = () => {
  return { type: CHAT_LEAVE_ROOM };
};
