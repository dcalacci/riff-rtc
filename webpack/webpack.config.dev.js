const path = require('path');
const webpack = require('webpack');
const nodeExternals = require('webpack-node-externals');
const DashboardPlugin = require('webpack-dashboard/plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const ExtractTextPlugin = require('extract-text-webpack-plugin');
//const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
  devtool: 'eval',
  entry: './src/index.jsx',
  target: 'web',
  mode: 'development',
  node: {
    fs: 'empty'
  },
  output: {
    path: path.resolve('build'),
    filename: 'bundle.js'
  },
  resolve: {
    modules: [
        'node_modules',
        path.resolve(`${__dirname}/../src`),
    ],
    extensions: [ '*', '.js', '.jsx', '.json', '.css' ]
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)?$/,
        include: path.resolve('src'),
        use: {
          loader: 'babel-loader',
          options: { babelrc: true }
        }
      },
      {
        test: /\.coffee$/,
        use: ['coffee-loader']
      },
      {
        test: /\.json$/,
        enforce: 'pre',
        exclude:'/node_modules/clmtrackr/',
        use: [
          'cache-loader',
          require.resolve('./strip-json-loader'),
        ],
        type: 'javascript/auto'
      },
      {
        test: /\.scss$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            'css-loader',
            'sass-loader'
          ]
        })
      },
      {
        test: /\.css$/,
        include: /node_modules/,
        use: [ 'style-loader', 'css-loader' ],
      },
      {
        test: /\.css$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'style-loader'
          }, {
            loader: 'css-loader',
            options: {
              modules: {
                localIdentName: '[name]__[local]___[hash:base64:5]'
              }
            }
          }
        ]
      },
      {
        test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 10000,
              mimetype: 'application/font-woff'
            }
          }
        ]
      },
      {
        test: /\.(jpe?g|gif|png|svg|ttf|wav|mp3)$/,
        use: [ 'file-loader' ]
      },
    ]
  },
  plugins: [
    new BundleAnalyzerPlugin({
      analyzerMode: 'static',
      openAnalyzer: false,
      reportFilename: 'WebpackBundleReport.html'
    }),
    new HtmlWebpackPlugin({
      inject: 'body',
      template: 'public/index.html'
    }),
    new webpack.HotModuleReplacementPlugin(),
    new DashboardPlugin(),
    new ExtractTextPlugin('css/styles.css')
    // new MiniCssExtractPlugin({
    //   filename: 'styles.css'
    // })
  ],
  watchOptions: {
    poll: 1000,
    ignored: [
      /node_modules/,
      'logs/*',
      'server/**/*',
    ],
  },
  devServer: {
    hot: true,
    quiet: true,
    inline: true,
    stats: false,
    watchOptions: { poll: 1000, ignored: /node_modules/ }
  }
};
